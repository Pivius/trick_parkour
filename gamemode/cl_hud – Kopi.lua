
-- http://facepunch.com/showthread.php?t=1209676
hud = hud or {}

hud.SIZE = 0.05/2
hud.DISTANCE = 25/2
hud.SUPER_ANGLE = 20


hud.Color = Color( 255, 255, 255, 255 )

        hud.ScreenWidth = 0
        hud.ScreenHeight = 0;
        hud.ScreenFOV = 0

Topspeed = 0

local keyechoes = CreateClientConVar("pkr_keys", "0", true)
local hudtoggle = CreateClientConVar("pkr_hud", "1", true)
function GM:HUDPaint()



    cam.Start3D( EyePos(), EyeAngles() )

        --hud.DrawHUD()
    cam.End3D()
end

function GM:HUDShouldDraw(name)
    local draw = true

    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo") then
    draw = false;
    end
        
return draw;
end




function hud.CheckDimensions()
        local width = ScrW();
        local height = ScrH();
        local fov = LocalPlayer():GetFOV();
        
        if(hud.ScreenWidth ~= width or hud.ScreenHeight ~= height or (fov >= 75 and hud.ScreenFOV ~= fov)) then
                hud.ChangeRatios(width, height, fov);
        end

        return;
end

function hud.ChangeRatios(width, height, fov)
        local fovDiff = fov - 75;

        if(width / height == 16 / 9) then
                hud.SIZE = 0.05 / (2.4 - 0.4 * (fovDiff / 15));
                
        elseif(width / height == 16 / 10) then
                hud.SIZE = 0.05 / (2.6 - 0.45 * (fovDiff / 15));
                
        else
                hud.SIZE = 0.05 / (3.15 - 0.55 * (fovDiff / 15));
                
        end

        hud.ScreenWidth = width;
        hud.ScreenHeight = height;
        hud.ScreenFOV = fov;

        return;
end


function hud.Health( ply, pos, ang )
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local            ply = LocalPlayer()
    local             HP = ply:Health()

    cam.Start3D2D( pos, ang, hud.SIZE/1.6 )

        draw.RoundedBox( 2, -636 * X_MULTIPLIER,  234 * Y_MULTIPLIER , 211, 60, Color( 255, 255, 255, 2 ));
        draw.RoundedBox( 2, -630 * X_MULTIPLIER,  242 * Y_MULTIPLIER , 39, 25, Color( 10, 10, 10, 255 ));
        draw.DrawText( HP, "CloseCaption_Normal", -628 * X_MULTIPLIER,  239.5 * Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

        draw.RoundedBox( 2, -631 * X_MULTIPLIER,  277 * Y_MULTIPLIER, 207, 5, Color( 1, 1, 1, 255));
        draw.RoundedBox( 1, -631 * X_MULTIPLIER,  277 * Y_MULTIPLIER, math.Clamp( HP, 0, 200 )*2.065, 5, Color( 200, 50, 50, 255));
        draw.RoundedBox( 1, -631 * X_MULTIPLIER,  277 * Y_MULTIPLIER, math.Clamp( HP, 0, 200 )*2.065, 1, Color( 255, 255, 255, 10));

    cam.End3D2D()
end

function hud.speeds()
local fast = LocalPlayer():GetVelocity():Length() / 10
local speed = math.Round(LocalPlayer():GetVelocity():Length() / 10, - 1  )

--draw.DrawText( speed, "Trebuchet18", ScrW() * 0.505, ScrH() * 0.497 )
draw.SimpleTextOutlined(speed, "Trebuchet18", ScrW() * 0.505, ScrH() * 0.497, Color(255,255,255), 0, 0, 1, Color(0,0,0))

end
hook.Add( "HUDPaint", "HelloThere", speeds)



function hud.TopSpeedDispText( ply, pos, ang, deltaTime ) 
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local   ply = LocalPlayer()

    local       playerspeed  = ply:GetVelocity()
    local speed2 = Vector(0,0,0)
        local speed3 = Vector(playerspeed.x, playerspeed.y, 0)
    local          Speed = playerspeed:Length()

    speed2.y = playerspeed.y
    speed2.x = playerspeed.x
    speed2.z = 0

    if playerspeed.z > speed3:Length() or playerspeed.z < -speed3:Length() then
        speed2.z = 0
    elseif playerspeed.z < speed3:Length() or playerspeed.z > -speed3:Length() then
        speed2.z = playerspeed.z
    end
    local           fast = math.Round( speed2:Length()/10 )
        if fast > Topspeed and fast > 50 and ply:Alive() then Topspeed = fast end 
        cam.Start3D2D( pos, ang, hud.SIZE/1.6 )
        draw.DrawText( Topspeed, "CloseCaption_Normal", 595 * X_MULTIPLIER,  135.5 * Y_MULTIPLIER, hud.SUPER_COLOR, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
        cam.End3D2D()
        end


function hud.SpeedDisp( ply, pos, ang )
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local            ply = LocalPlayer()
    local       playerspeed  = ply:GetVelocity()
    local speed2 = Vector(0,0,0)
    local          Speed = playerspeed:Length()

    speed2.y = playerspeed.y
    speed2.x = playerspeed.x
    speed2.z = 0



    cam.Start3D2D( pos, ang, hud.SIZE/1.6 )

       draw.RoundedBox(2, -636 * X_MULTIPLIER,  230 * Y_MULTIPLIER, 215, 60, Color( 255, 255, 255, 2));
       draw.RoundedBox(2, -630 * X_MULTIPLIER,  238 * Y_MULTIPLIER, 39, 25, Color( 10, 10, 10, 255));

        draw.RoundedBox(2, -631 * X_MULTIPLIER,  273 * Y_MULTIPLIER, 207, 5, Color( 1, 1, 1, 255));
        draw.RoundedBox(1, -631 * X_MULTIPLIER,  273 * Y_MULTIPLIER, math.Clamp( speed2:Length()+20, 0, 5000)/22.5, 5, Color( 105, 175, 175, 255));
        draw.RoundedBox(1, -631 * X_MULTIPLIER,  273 * Y_MULTIPLIER, math.Clamp( speed2:Length()+20, 0, 5000)/22.5-2, 1, Color( 255, 255, 255, 40));

    cam.End3D2D()


end



function hud.Contextmenu( ply, pos, ang )

    -- Let the gamemode decide whether we should open or not..
    if ( !hook.Call( "ContextMenuOpen", GAMEMODE ) ) then return end
        
    if Contextmenuisopen == true then

        local       SCREEN_W = ScrW();
        local       SCREEN_H = ScrH();
        local   X_MULTIPLIER = ScrW() / SCREEN_W;
        local   Y_MULTIPLIER = ScrH() / SCREEN_H;
        local            ply = LocalPlayer()
        
        cam.Start3D2D( pos, ang, hud.SIZE/1.6 )
            draw.RoundedBox(2, -636 * X_MULTIPLIER, -120 * Y_MULTIPLIER, 215, 295, Color( 25, 25, 25, 155));
            draw.DrawText( "Work in progress", "CloseCaption_Normal", -606*X_MULTIPLIER, -60*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

        cam.End3D2D()

    end
    
end


function hud.SpeedDispText( ply, pos, ang ) 
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;

    local           fast = math.Round( ply:GetVelocity():Length()/10 )
    local       playerspeed  = ply:GetVelocity()
    local speed2 = Vector(0,0,0)
    local          Speed = playerspeed:Length()
    local speed3 = Vector(playerspeed.x, playerspeed.y, 0)

    speed2.y = playerspeed.y
    speed2.x = playerspeed.x
    speed2.z = 0

    if playerspeed.z > speed3:Length() or playerspeed.z < -speed3:Length() then
        speed2.z = 0
    elseif playerspeed.z < speed3:Length() or playerspeed.z > -speed3:Length() then
        speed2.z = playerspeed.z
    end

    cam.Start3D2D( pos, ang, hud.SIZE/1.6 )

       draw.DrawText( math.Round(speed2:Length()/10), "CloseCaption_Normal", 595 * X_MULTIPLIER,  235.5 * Y_MULTIPLIER, hud.SUPER_COLOR, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);

    cam.End3D2D()
end





function hud.KeyEcho(ply, pos, ang)
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local            ply = LocalPlayer()

    cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
        --Forward/W
        WBox = draw.RoundedBox(2, 100*X_MULTIPLIER, 86*Y_MULTIPLIER, 95, 85, Color( 10, 10, 10, 80));
        draw.DrawText( "W", "CloseCaption_Normal", 137.6*X_MULTIPLIER, 115.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
        --Left/A
        ABox = draw.RoundedBox(2, X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 85, Color( 10, 10, 10, 80));
        draw.DrawText( "A", "CloseCaption_Normal", 41.92*X_MULTIPLIER, 205.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
        --Back/S
        SBox = draw.RoundedBox(2, 100*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 85, Color( 10, 10, 10, 80));
        draw.DrawText( "S", "CloseCaption_Normal", 139.2*X_MULTIPLIER, 205.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
        --Right/D
        DBox = draw.RoundedBox(2, 200*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 85, Color( 10, 10, 10, 80));
        draw.DrawText( "D", "CloseCaption_Normal", 239.2*X_MULTIPLIER, 205.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
        --Jump/Space
        SPBox = draw.RoundedBox(2, X_MULTIPLIER, 270*Y_MULTIPLIER, 295.4, 85, Color( 10, 10, 10, 80));               
        draw.DrawText( "Jump", "CloseCaption_Normal", 119.2*X_MULTIPLIER, 295.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

       --CTRLBox = draw.RoundedBox(2, -200*X_MULTIPLIER, 269.6*Y_MULTIPLIER, 95, 85, Color( 10, 10, 10, 125));
       --draw.DrawText( "Duck", "CloseCaption_Normal", -180*X_MULTIPLIER, 296.2*Y_MULTIPLIER, hud.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
    cam.End3D2D()

    if ( ply:KeyDown( IN_FORWARD ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
                draw.RoundedBox(2, 100*X_MULTIPLIER, 86*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 100*X_MULTIPLIER, 86*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 100*X_MULTIPLIER, 167*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 191*X_MULTIPLIER, 86*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
            cam.End3D2D()
    end

    if ( ply:KeyDown( IN_MOVELEFT ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )

                draw.RoundedBox(2, 0*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 0*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 0*X_MULTIPLIER, 258.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 92*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));

            cam.End3D2D()
    end

    if ( ply:KeyDown( IN_BACK ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
                draw.RoundedBox(2, 100*X_MULTIPLIER, 178*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 100*X_MULTIPLIER, 178*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 100*X_MULTIPLIER, 259*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 191*X_MULTIPLIER, 178*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
            cam.End3D2D()
    end
        
    if ( ply:KeyDown( IN_MOVERIGHT ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
                draw.RoundedBox(2, 200*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 200*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 200*X_MULTIPLIER, 258.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 291*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));

            cam.End3D2D()        
    end

    if ( ply:KeyDown( IN_JUMP ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
                draw.RoundedBox(2, 0*X_MULTIPLIER, 269.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 0*X_MULTIPLIER, 269.5*Y_MULTIPLIER, 295, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 292*X_MULTIPLIER, 269.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, 0*X_MULTIPLIER, 350.5*Y_MULTIPLIER, 295, 4, Color( 100, 90, 213, 255));

            cam.End3D2D()
    end

    --[[if ( ply:KeyDown( IN_DUCK ) ) then
            cam.Start3D2D( pos, ang, hud.SIZE/1.7 )
                draw.RoundedBox(2, -200*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, -200*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2, -200*X_MULTIPLIER, 258.5*Y_MULTIPLIER, 95, 4, Color( 100, 90, 213, 255));
                draw.RoundedBox(2,  -109*X_MULTIPLIER, 177.5*Y_MULTIPLIER, 4, 85, Color( 100, 90, 213, 255));

            cam.End3D2D()
    end]]--

end





function hud.DrawHUD()
    local        ply = LocalPlayer()
    local     plyAng = EyeAngles()
    local     plyPos = EyePos()
    local        pos = plyPos
    local  centerAng = Angle( plyAng.p, plyAng.y, 0 ) 
    local  hudangle2 = 0.1
    local   hudangle = 0


    pos = pos + ( centerAng:Forward() * hud.DISTANCE )

    centerAng:RotateAroundAxis( centerAng:Right(), 90 )
    centerAng:RotateAroundAxis( centerAng:Up(), -90 )
    //centerAng:RotateAroundAxis( centerAng:Right(), hudangle )

    local  leftAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 
    local rightAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 

    leftAng:RotateAroundAxis( leftAng:Right(), hud.SUPER_ANGLE*-1 )
    rightAng:RotateAroundAxis( rightAng:Right(), hud.SUPER_ANGLE )

    local speedAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 

    speedAng:RotateAroundAxis( speedAng:Right(), hud.SUPER_ANGLE*10)

    local centerAngUp = Angle( plyAng.p, plyAng.y, 0 ) 

    centerAngUp:RotateAroundAxis( centerAngUp:Right(), 98 )
    centerAngUp:RotateAroundAxis( centerAngUp:Up(), -90 )

    local  leftAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r ) 
    local rightAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r ) 

    leftAngUp:RotateAroundAxis( leftAngUp:Right(), hud.SUPER_ANGLE*-1)
    rightAngUp:RotateAroundAxis( rightAngUp:Right(), hud.SUPER_ANGLE )

        cam.Start3D( plyPos, plyAng)

            if hudtoggle:GetBool() then
                hud.Health(ply, pos, leftAng)
                hud.SpeedDisp(ply, pos, speedAng)
                hud.SpeedDispText(ply, pos, rightAng)
                hud.TopSpeedDispText(ply, pos, rightAng)

                if keyechoes:GetBool() then
                    hud.KeyEcho(ply, pos, centerAng)
                end
                hud.Contextmenu(ply, pos, leftAngUp)
            end


        cam.End3D()
end

hook.Add( "RenderScreenspaceEffects", "hudM8", hud.DrawHUD )








--[[

-------------------------------------
-- Fonts.
-------------------------------------
surface.CreateFont("HUD Health", {font = "Acens", size = 35})

-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}


hud.size = 0.05/2
hud.distance = 25/2

-------------------------------------
-- The main UI color.
-------------------------------------
hud.Color = Color( 255, 255, 255, 255 )

-------------------------------------
-- Topspeed
-- Saves the topspeed of the player for the current session.
-------------------------------------
Topspeed = 0


-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local keyechoes = CreateClientConVar("pkr_keys", "0", true)
local hudtoggle = CreateClientConVar("pkr_hud", "1", true)

-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDShouldDraw(name)
    local draw = true
    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo") then
    draw = false;
    end
return draw;
end


-------------------------------------
-- UI Health.
-------------------------------------
function hud.Health( ply, pos, ang )
    
    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local            ply = LocalPlayer()
    local             HP = ply:Health()
    local         endpos = 0
    -------------------------------------
    -- Icon.
    -------------------------------------


    healthicon = Material("gamemodes/Parkour2/content/materials/Parkour/hud/FullHealth.png", "noclamp")

    healthicon2 = Material("gamemodes/Parkour2/content/materials/Parkour/hud/LowHealth.png", "noclamp")
 

    cam.Start3D2D( pos, ang, hud.size/1.6 )
        -------------------------------------
        -- UI Background.
        -------------------------------------
        surface.SetDrawColor(Color(155, 155, 155, 5))
        
        -------------------------------------
        -- UI fit numbers
        -- 100-90
        -------------------------------------
        if HP >= 100 then
            endpos = 80
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP < 100 and HP > 94 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 95 and HP > 93 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 93 and HP > 91 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 91 and HP > 90 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 90-80
        -------------------------------------
        elseif HP <= 90 and HP > 84 then
            endpos = 73
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 85 and HP > 83 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 83 and HP > 81 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 81 and HP > 80 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 80-70
        -------------------------------------
        elseif HP <= 80 and HP > 74 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 75 and HP > 73 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 73 and HP > 71 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 71 and HP > 70 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 70-60
        -------------------------------------
        elseif HP <= 70 and HP > 64 then
            endpos = 73
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 65 and HP > 63 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 63 and HP > 61 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 61 and HP > 60 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 60-50
        -------------------------------------
        elseif HP <= 60 and HP > 54 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 55 and HP > 53 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 53 and HP > 51 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 51 and HP > 50 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 50-40
        -------------------------------------
        elseif HP <= 50 and HP > 44 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 45 and HP > 43 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 43 and HP > 41 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 41 and HP > 40 then
            endpos = 61
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 40-30
        -------------------------------------
        elseif HP <= 40 and HP > 34 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 35 and HP > 33 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 33 and HP > 31 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 31 and HP > 30 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 30-20
        -------------------------------------
        elseif HP <= 30 and HP > 24 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 25 and HP > 23 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 23 and HP > 21 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 21 and HP > 20 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 20-10
        -------------------------------------
        elseif HP <= 20 and HP > 14 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 15 and HP > 13 then
            endpos = 59
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 13 and HP > 11 then
            endpos = 61
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 11 and HP > 10 then
            endpos = 52
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 10-0
        -------------------------------------
        elseif HP <= 10 and HP > 4 then
            endpos = 56
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 5 and HP > 3 then
            endpos = 54
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 3 and HP > 1 then
            endpos = 54
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 1 and HP > 0 then
            endpos = 46
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 0 then
            endpos = 55
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        end

        surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375,  lerpB, 35)
        -------------------------------------
        -- Icon UI.
        -------------------------------------
        surface.SetDrawColor(Color(255, 255, 255, 255))
        if HP >= 45 then 
        surface.SetMaterial(healthicon)
        positiony = Y_MULTIPLIER+379.8
        else
        surface.SetMaterial(healthicon2)
        positiony = Y_MULTIPLIER+377.5
        end
        //surface.DrawTexturedRect(X_MULTIPLIER-673.3, positiony, 30, 30)

        -------------------------------------
        -- Health bar background.
        -------------------------------------
        surface.SetDrawColor(Color(25, 25, 25, 235))
        surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375,  200-1.2, 3)

        -------------------------------------
        -- Health bar delayed
        -------------------------------------
        lerp = lerp and Lerp(0.05, lerp, HP) or HP

        surface.SetDrawColor(Color(255, 75, 75, 255))
        surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(lerp, 0, 100), 4)


        -------------------------------------
        -- Health bar 
        -------------------------------------
        surface.SetDrawColor(Color(255, 75, 75, 255))

        //surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375, math.Clamp(HP*2, 0, 200) , 3)
 --[[--Used if only when health lowered delayed
            surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(HP, 0, 100), 4)
]]--[[
        if HP > HP then
                lerp = lerp and Lerp(0.05, HP, lerp) or HP
            surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(lerp, 0, 100), 4)
        end 
        -------------------------------------
        -- Health counter
        -------------------------------------

            surface.SetTextColor(Color(255, 255, 255, 255))
            surface.SetFont( "HUD Health" )
            surface.SetTextPos( X_MULTIPLIER-640, Y_MULTIPLIER+377.5 )
            surface.DrawText( HP )
    cam.End3D2D()
end



        -------------------------------------
        -- IDEA SECTION
        --
        -- SPEEDUI
        -------------
        --The limit should be all time top -- BY THEO
        -------------------------------------



function hud.DrawHUD()
    local        ply = LocalPlayer()
    local     plyAng = EyeAngles()
    local     plyPos = EyePos()
    local        pos = plyPos
    local  centerAng = Angle( plyAng.p, plyAng.y, 0 ) 


    pos = pos + ( centerAng:Forward() * hud.distance )

    centerAng:RotateAroundAxis( centerAng:Right(), 90 )
    centerAng:RotateAroundAxis( centerAng:Up(), -90 )
    //centerAng:RotateAroundAxis( centerAng:Right(), hudangle )

    local  leftAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 
    local rightAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 

    leftAng:RotateAroundAxis( leftAng:Right(), 15*-1 )
    leftAng:RotateAroundAxis( leftAng:Up(), 7*-1 )
    rightAng:RotateAroundAxis( rightAng:Right(), 20 )

    local speedAng = Angle( centerAng.p, centerAng.y, centerAng.r ) 

    speedAng:RotateAroundAxis( speedAng:Right(), 20*10)

    local centerAngUp = Angle( plyAng.p, plyAng.y, 0 ) 

    centerAngUp:RotateAroundAxis( centerAngUp:Right(), 98 )
    centerAngUp:RotateAroundAxis( centerAngUp:Up(), -90 )

    local  leftAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r ) 
    local rightAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r ) 

    leftAngUp:RotateAroundAxis( leftAngUp:Right(), 20*-1)
    rightAngUp:RotateAroundAxis( rightAngUp:Right(), 20 )

        cam.Start3D( plyPos, plyAng)
            if hudtoggle:GetBool() then
                hud.Health(ply, pos, leftAng)
            end
        cam.End()
end

hook.Add( "RenderScreenspaceEffects", "hudM8", hud.DrawHUD )




]]