parkour = {}
SQLDetails = {
  Host = "127.0.0.1", User = "root",
  Pass = "", Database = "gmod_vgam",
  Port = 3306
}


local tables = {}

tables.topspeed = "vgam_topspeed"
tables.zones = "parkour_zones"
tables.tricks = "parkour_tricks"
tables.users = "vgam_users"
tables.ranks = "vgam_ranks"


SQLDetails.tables = tables
parkour.database = SQLDetails
