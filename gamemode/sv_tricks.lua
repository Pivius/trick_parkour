// Made by Pivius
// 01/15/2017


util.AddNetworkString( "Update Tricks" )
Tricks = {}
Tricks.Tricks = {}

local meta = FindMetaTable( "Player" )
--PlayerInitialSpawn
hook.Add("PlayerInitialSpawn", "LoadPlayerTricks", function(ply)
  GetSet("zoneOrder", "Player")
  GetSet("zoneSpeed", "Player")
  GetSet("lastTrick", "Player")
  GetSet("Points", "Player")
  GetSet("trickRank", "Player")
  GetSet("zoneTime", "Player")
  GetSet("prevZone", "Player")
end)

hook.Add("PlayerSpawn", "LoadPlayerTricks", function(ply)
  /*GetSet("zoneOrder", "Player")
  GetSet("zoneSpeed", "Player")
  GetSet("lastTrick", "Player")
  GetSet("Points", "Player")
  GetSet("trickRank", "Player")
  */
  ply:SetzoneOrder(nil)
  ply:SetzoneSpeed(nil)
  ply:SetlastTrick(nil)
  ply:SetPoints(0)
  ply:SettrickRank(1)
  ply:SetzoneTime(nil)
  ply:SetprevZone(nil)

end)

Tricks.ZoneOrder = {}
Tricks.ZoneSpeed = {}
Tricks.LastTricks = {}
Tricks.Ranks = {
  [1] = "SpeedyFast",
  [2] = "Novice",
  [3] = "Intermediate",
  [4] = "Ok",
  [5] = "Not good",
  [6] = "A bit good",
  [7] = "Godlike"
}

 ///////////////////////////////////////
// ████████ ██████  ██  ██████ ██   ██ //
//    ██    ██   ██ ██ ██      ██  ██  //
//    ██    ██████  ██ ██      █████   //
//    ██    ██   ██ ██ ██      ██  ██  //
//    ██    ██   ██ ██  ██████ ██   ██ //
 ///////////////////////////////////////


function meta:CreateOrder()
  if !self:GetzoneOrder() then
    self:SetzoneOrder({})
  end
  if !self:GetzoneSpeed() then
    self:SetzoneSpeed({})
  end
  if !self:GetlastTrick() then
    self:SetlastTrick(nil)
  end

  if !self:GetzoneTime() then
    self:SetzoneTime({})
  end
end

function meta:ZoneOrder(zone, status, vel)
    self:CreateOrder()
    local t = {} -- Temporary table
    local zones = self:GetzoneOrder()
    UT:MergeTableTo(t, zones)
    table.insert(t, zone)
    self:SetzoneOrder(t) -- Set Zone Order

    t = {} -- Temporary table
    local speed = self:GetzoneSpeed()
    UT:MergeTableTo(t, speed)
    table.insert(t, vel)
    self:SetzoneSpeed(t) -- Set Zone Speed

    t = {} -- Temporary table
    UT:MergeTableTo(t, self:GetzoneTime())
    table.insert(t, CurTime())
    self:SetzoneTime(t)

end

-------------------------------------
-- Update Zone to player.
-------------------------------------

function UpdateTricks()
	local trch = Tricks.Tricks

	--PrintTable(znch)
	net.Start("Update Tricks")

		net.WriteTable(trch)

	net.Send(Admin:GetPlayerFrom("owner"))

end
timer.Create( "UpdateTricks", 1, 0, UpdateTricks )

function Tricks:CreateTrick(name, sequence, ignore, pts, limit)

  local points = pts
  local sequence = sequence
  local name = name
  local ignore = ignore
  local speedlimit
  if !limit then
    speedlimit = 0
  else
    speedlimit = limit
  end

  local trick = {
    ['sequence'] = sequence,
    ['ignore'] = ignore,
    ['points'] = points,
    ['trick'] = name,
    ['speedlimit'] = speedlimit
  }
  table.insert(Tricks.Tricks, trick)
end


 /////////////////////////////////////////////////////////////////////////////////////////////
//  ██████ ██████  ███████  █████  ████████ ███████     ████████ ██████  ██  ██████ ██   ██  //
// ██      ██   ██ ██      ██   ██    ██    ██             ██    ██   ██ ██ ██      ██  ██   //
// ██      ██████  █████   ███████    ██    █████          ██    ██████  ██ ██      █████    //
// ██      ██   ██ ██      ██   ██    ██    ██             ██    ██   ██ ██ ██      ██  ██   //
//  ██████ ██   ██ ███████ ██   ██    ██    ███████        ██    ██   ██ ██  ██████ ██   ██  //
 /////////////////////////////////////////////////////////////////////////////////////////////

--                   Name         Segment    pts
Tricks:CreateTrick("Acceleration", {"Wall1", "Wall2", "Wall3"}, {}, 5, 50)
Tricks:CreateTrick("Surf roof trick", {"Surf", "Roof"}, {}, 5)
Tricks:CreateTrick("Surf roof to Surf trick", {"Surf", "Roof", "Roof", "Surf"}, {}, 5)
Tricks:CreateTrick("Cool pillar jump land on heli trick i guess", {"Pillar7 Ramp1", "Heli"}, {}, 100)

function Tricks:GetInfo(trick)
  for k, v in pairs(Tricks.Tricks) do
    local key = table.GetKeys( v )
    --print(table.HasValue(table.GetKeys( Tricks.Tricks[k], trick )))
    --print(table.KeyFromValue(Tricks.Tricks[k], name))

    if table.HasValue(key, trick) then
      local name = trick
      local sequence = table.GetFirstKey(Tricks.Tricks[k][trick])
      local pts = table.GetFirstValue(Tricks.Tricks[k][trick])
      local moves = table.Count(sequence)
      return name, sequence, pts, moves
    end
  end
end

function meta:ResetOrder()
  self:SetzoneOrder(nil)
  self:SetzoneSpeed(nil)
  self:SetlastTrick(nil)
  self:SetzoneTime(nil)
end

function meta:FinishTrick(trick, vel, time)
  self:SetlastTrick(trick)
  chat.Text(nil, Admin.Colors["VGAM"], "[Trick Man] ", Admin.Colors["normal"], self:Nick() .. " completed " .. trick )
  chat.Text(nil, Admin.Colors["normal"], "Top Velocity: ".. vel )
  chat.Text(nil, Admin.Colors["normal"], "Time: ".. time )
end

function meta:TrackTricks()
  if self:GetzoneOrder() == nil then return end

  local curzone = table.GetLastValue( self:GetzoneOrder() )
  local trackkey = table.GetLastKey( self:GetzoneOrder() )
  local tricks = Tricks.Tricks
  local trickname, seqstring
  local tbl = {}
  local trickOrder = {}
  local trickSpeed = {}
  local trickTime = {}
  self:SetprevZone(self:GetzoneOrder()[#self:GetzoneOrder()-1])

  timer.Create("Reset " .. self:SteamID(), 120, 1, function()
    if IsValid(self) then
      self:ResetOrder()
    end
  end)

  for _, trick in pairs(tricks) do

    trickOrder = {}
    trickSpeed = {}
    trickTime = {}

    --UT:MergeTableTo(trickOrder, self:GetzoneOrder())
    UT:MergeTableTo(trickSpeed, self:GetzoneSpeed())
    UT:MergeTableTo(trickTime, self:GetzoneTime())

    local t = {}
    for k, order in pairs(self:GetzoneOrder()) do
      if !UT:TblContain(trick['ignore'], order) then
        table.insert(t, order)
      end
    end
    UT:MergeTableTo(trickOrder, t)
    local keys = (#t - #trick["sequence"] )
    for i = 1, keys do
        table.remove(trickOrder, 1)
        table.remove(trickSpeed, 1)
        table.remove(trickTime, 1)
    end
    PrintTable(trickOrder)
    print("\n")
    /*
    for _, ignore in pairs(trick["ignore"]) do
      if UT:TblContain(trickOrder, ignore) then
        local ignorekey = table.KeyFromValue(trickOrder, ignore)
        if !UT:TblContain(trick['ignore'], self:GetzoneOrder()[i+ignorekey-1]) then
          print(i-ignorekey+1)

        end
      end
    end
    */
    if UT:TblCompare(trickOrder, trick["sequence"]) then
      trickname = trick["trick"]
      if trickSpeed[1] < trick["speedlimit"] or trick["speedlimit"] == 0 then
        local time = math.Round( table.GetLastValue(trickTime) - trickTime[1], 3 )
        timer.Simple(0.1, function()
          self:FinishTrick(trickname, trickSpeed[table.GetWinningKey( trickSpeed )], time)
          --self:ResetTrick()
        end)
      else
        chat.Text(self, Admin.Colors["VGAM"], "[Trick Man] ", Admin.Colors["normal"], "You went too fast!")
        chat.Text(self, Admin.Colors["normal"], "Start speedlimit is " .. trick["speedlimit"])
      end
    end
  end
end
