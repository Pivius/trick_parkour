--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

-- These files get sent to the client
AddCSLuaFile( "cl_init.lua" )

AddCSLuaFile( 'sh_stats.lua' )
AddCSLuaFile( 'cl_stats.lua' )
AddCSLuaFile("sh_walljump.lua")

AddCSLuaFile( 'getset.lua' )
AddCSLuaFile( 'UtiliT.lua' )
AddCSLuaFile( "cl_font.lua" )
AddCSLuaFile( "persistence.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_context.lua" )
AddCSLuaFile( "cl_hud.lua" )
AddCSLuaFile( "cl_lobby.lua" )
AddCSLuaFile( "sh_wallslide.lua" )
AddCSLuaFile( 'cl_trails.lua' )
AddCSLuaFile( 'sh_surfing.lua' )
AddCSLuaFile("sh_editor.lua")
AddCSLuaFile("cl_editor.lua")
AddCSLuaFile("admin/cl_commands.lua")
AddCSLuaFile('sh_teleport.lua')

include('sql/database.lua')
include('sql/core.lua')
include( 'getset.lua' )
include( 'sv_stats.lua' )
include( 'sh_stats.lua' )
include('sv_notifications.lua')
include( 'UtiliT.lua' )
include( 'sh_surfing.lua' )
include( 'admin/sv_hooks.lua')
include( 'admin/sv_admin.lua')
include( 'admin/sv_commands.lua')
include( 'shared.lua' )
include( 'player.lua' )
include("sh_walljump.lua")
include( "sh_wallslide.lua" )
include('sv_player.lua')
include('sv_trails.lua')
include("sv_tricks.lua")
include("sv_zones.lua")
include("sh_editor.lua")
include('sh_teleport.lua')
--
-- Make BaseClass available
--
DEFINE_BASECLASS( "gamemode_base" )

--


hook.Add("Initialize", "console", function()
	game.ConsoleCommand("sv_maxvelocity 9999\n")
	game.ConsoleCommand( "sv_friction 8\n" )
	game.ConsoleCommand( "sv_gravity 300\n" )
	game.ConsoleCommand("sv_sticktoground 0\n")
	game.ConsoleCommand("sv_airaccelerate 10\n")
	game.ConsoleCommand("sv_accelerate 10\n")
	game.ConsoleCommand("mp_falldamage 0\n")

end)
Admin.Commands:LoadCMD()
Admin:Init()

function GM:PlayerConnect(ply)


end

hook.Add("PlayerSpawn", "Spawnwithtrail", function(ply)
		--	ply.Trail = util.SpriteTrail(ply, 0, Color(255,255,255), false, 1, 2, 10, 1/(15+1)*0.5,'trick/test.vmt') --trail texture "white.vmt"
		ply:TrailColor(Color( 0, 255, 255, 255))
		ply:TrailMat("sprites/physcannon_bluelight2.vmt")--trick/test.vmt
		ply:TrailLife(2)
		ply:TrailSize(12, 0)
		ply:TrailRes(0.01)
		ply:TrailAdditive(true)
		ply:CreateTrail()

end)



hook.Add("PlayerDeath", "Die", function ( pl )
	pl:RemoveTrail()
end)

hook.Add("PlayerDisconnected", "PlayerDisc", function( pl )
	pl:RemoveTrail()
end)

hook.Add( "InitPostEntity", "LoadEntities", function()
	Zones:AwaitLoad(true)
end)

function GM:GetFallDamage( ply, speed )
	 return ( speed / 23.4)
end
--[[---------------------------------------------------------
   Name: gamemode:PlayerSpawn( )
   Desc: Called when a player spawns
-----------------------------------------------------------]]
function GM:PlayerSpawn( pl )


	player_manager.SetPlayerClass( pl, "player_sandbox" )

	BaseClass.PlayerSpawn( self, pl )

	pl:SetAvoidPlayers( false )
	pl:SetCollisionGroup( COLLISION_GROUP_WEAPON )

end

function GM:EntityTakeDamage( ent, info )
end

--[[---------------------------------------------------------
   Name: gamemode:PlayerShouldTakeDamage
   Return true if this player should take damage from this attacker
   Note: This is a shared function - the client will think they can
	 damage the players even though they can't. This just means the
	 prediction will show blood.
-----------------------------------------------------------]]
function GM:PlayerShouldTakeDamage( ply, attacker )

	-- The player should always take damage in single player..
	if ( game.SinglePlayer() ) then return true end

	-- No player vs player damage
	if ( attacker:IsValid() && attacker:IsPlayer() ) then
		return cvars.Bool( "sbox_playershurtplayers", true )
	end
	-- Default, let the player be hurt
	return true

end

function GM:CreateEntityRagdoll( entity, ragdoll )
end

-- Set the ServerName every 30 seconds in case it changes..
-- This is for backwards compatibility only - client can now use GetHostName()
local function HostnameThink()

	SetGlobalString( "ServerName", GetHostName() )

end

timer.Create( "HostnameThink", 30, 0, HostnameThink )

--[[---------------------------------------------------------
   Show the search when f1 is pressed
-----------------------------------------------------------]]
function GM:ShowHelp( ply )

	ply:SendLua( "hook.Run( 'StartSearch' )" );

end



--[[---------------------------------------------------------
   Called once on the player's first spawn
-----------------------------------------------------------]]
function GM:PlayerInitialSpawn( ply )

	BaseClass.PlayerInitialSpawn( self, ply )

end
