local PlayerMeta = FindMetaTable("Player")
function PlayerMeta:NearWall()
	local trace = {}
	trace.start = self:GetPos() + Vector( 0, 0, 50 )
	trace.endpos = trace.start + (self:GetAimVector() * 40)
	trace.filter = self
	local trFw = util.TraceLine(trace)

	local trace = {}
	trace.start = self:GetPos() + Vector( 0, 0, 50 )
	trace.endpos = trace.start + (self:GetAimVector():Angle():Right( ) * 40)
	trace.filter = self
	local trRi = util.TraceLine(trace)

	local trace = {}
	trace.start = self:GetPos() + Vector( 0, 0, 50 )
	trace.endpos = trace.start - (self:GetAimVector():Angle():Right( ) * -40)
	trace.filter = self
	local trLe = util.TraceLine(trace)

	local trace = {}
	trace.start = self:GetPos() + Vector( 0, 0, 50 )
	trace.endpos = trace.start + (self:GetAimVector() * -40)
	trace.filter = self
	local trBk = util.TraceLine(trace)

	local trace = {}
	trace.start = self:GetPos() + Vector( 0, 0, 50 )
	trace.endpos = trace.start + (self:GetAimVector():Angle():Up( ) * 40)
	trace.filter = self
	local trUp = util.TraceLine(trace)

	if trFw and trRi and trLe and (trFw.HitWorld or trRi.HitWorld or trLe.HitWorld or trBk.HitWorld or trUp.HitWorld) and !(trFw.HitSky or trRi.HitSky or trLe.HitSky or trBk.HitSky or trUp.HitSky) then
		return true
	else
		return false
	end
	return false
end

function WallSlide(ply, cmd)
	ply.sound = ply.sound or CreateSound(ply, "physics/body/body_medium_scrape_smooth_loop1.wav")
	local tr = 	ply:GetEyeTrace()
	local grabtime = 0
	local tracedata = {}
	tracedata.start = ply:EyePos()
	tracedata.endpos = ply:EyePos()+(ply:GetAimVector()*40)
	tracedata.filter = ply
	local trFW = util.TraceLine(tracedata)

	local tracedata = {}
	tracedata.start = ply:EyePos()
	tracedata.endpos = ply:EyePos()+(ply:GetAimVector()*-40)
	tracedata.filter = ply

	local trBK = util.TraceLine(tracedata)

	if ply:NearWall() and (trFW.Fraction < 1 or trBK.Fraction < 1) then
		ply.canGrab = true
	else
		ply.canGrab = false
	end
	if ply.canGrab and !ply:OnGround()  then
		if ply:KeyPressed(IN_ATTACK2) then
			ply.doSlide = true
			ply.vel = cmd:GetVelocity()
			ply.vel.z = 0
			ply.timer = CurTime() + 5
		end
		if ply.doSlide then
			if ply:KeyReleased(IN_ATTACK2) then
				ply.sound:FadeOut(0.1)
				ply.doSlide = false
				ply.vel = cmd:GetVelocity()
				ply.vel.z = 0
			end
			if ply:KeyDown(IN_ATTACK2) then
				ply.sound:Play()
				ply.slowdown =1
				local aimvec = ply:GetAimVector()
				aimvec.z = aimvec.z *0.1
				local aim = aimvec * 5

				if ply:GetSpeed() > 40 then
					ply.slowdown = math.Clamp(math.TimeFraction( ply.timer-3, ply.timer-5, CurTime() ), 0, 1)
					aim = aimvec*2
				elseif cmd:GetVelocity() == 400 then
					aim = aimvec*0.001
					aim.z = aim.z *2
					ply.vel = cmd:GetVelocity()
					ply.vel.z = 0
					ply.slowdown =1
				elseif ply:GetSpeed() < 40 then
					aim = aimvec*5
					aim.z = aim.z *20
					ply.vel = cmd:GetVelocity()
					ply.vel.z = 0
					ply.slowdown =1
				end

				cmd:SetVelocity(ply.slowdown * (ply.vel + aim))
			end
		end
	else
		ply.sound:FadeOut(0.1)
		ply.vel = cmd:GetVelocity()
		ply.vel.z = 0
	end
	if ply.timer and ply.timer < CurTime() then
		ply.sound:FadeOut(0.1)
		ply.doSlide = false
		ply.vel = cmd:GetVelocity()
		ply.vel.z = 0
	end
end
hook.Add("SetupMove", "WallSlide", WallSlide)
