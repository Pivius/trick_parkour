Admin.Commands:createCMD("editor", "caller", function(caller)
	chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Editor toggled.")
  caller:toggleEditor()
end)

Admin.Commands:createCMD("clip", "caller", function(caller)
	chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip toggled.")
  caller:toggleNoclip()
end)
