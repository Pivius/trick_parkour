include("my_sql.lua")
SQL = {}
SQLdata = SQLdata or {}
SQL.Use = true -- Set this to false if you don't want MySQL

local data = parkour.database

mysql:Disconnect()
if SQL.Use then
	mysql:Connect(data.Host, data.User, data.Pass, data.Database, data.Port)
end



hook.Add("DatabaseConnected", "Create Tables", function()
	local topspeed = mysql:Create(SQLDetails.tables.topspeed)
		topspeed:Create("sID", "VARCHAR(255)")
		topspeed:Create("name", "VARCHAR(255)")
		topspeed:Create("speed", "INT(255)")
		topspeed:Create("date", "VARCHAR(255)")
		topspeed:PrimaryKey("sID")
	topspeed:Execute()

	local zones = mysql:Create(SQLDetails.tables.zones)
		zones:Create("map", "VARCHAR(255)")
		zones:Create("id", "INT(255)")
		zones:Create("name", "VARCHAR(255)")
		zones:Create("p1", "VARCHAR(255)")
		zones:Create("p2", "VARCHAR(255)")
		zones:Create("mode", "VARCHAR(255)")
		zones:PrimaryKey("id")
	zones:Execute()

	local tricks = mysql:Create(SQLDetails.tables.tricks)
		tricks:Create("map", "VARCHAR(255)")
		tricks:Create("id", "INT(255)")
		tricks:Create("name", "VARCHAR(255)")
		tricks:Create("sequence", "VARCHAR(255)")
		tricks:Create("ignore", "VARCHAR(255)")
		tricks:PrimaryKey("id")
	tricks:Execute()

	local qadmins = mysql:Create(SQLDetails.tables.users)
		qadmins:Create("sID", "VARCHAR(255)")
    qadmins:Create("name", "VARCHAR(255)")
    qadmins:Create("rank", "VARCHAR(255)")
		qadmins:Create("flags", "VARCHAR(255)")
    qadmins:PrimaryKey("sID")
  qadmins:Execute()
	local qranks = mysql:Create(SQLDetails.tables.ranks)
		qranks:Create("name", "VARCHAR(255)")
		qranks:Create("inheritance", "VARCHAR(255)")
		qranks:Create("cmds", "VARCHAR(255)")
		qranks:Create("power", "TINYINT(255)")
		qranks:PrimaryKey("name")
	qranks:Execute()
	Admins:Init()
end)

timer.Create( "mysql.Timer", 1, 0, function()
    mysql:Think()
end )
