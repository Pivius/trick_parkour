PANEL = {} or PANEL

function PANEL:Create()
 self.mainpanel = vgui.Create( "vgui panel" )
 self.mainpanel:SetPos(self.X, self.Y)
 self.mainpanel:SetSize(self.width, self.height+50)
 self.mainpanel:SetCol(self.color)
 self.mainpanel:Create( )

 self.text = vgui.Create( "DLabelEditable", self.mainpanel.bg )
 self.text:SetSize( self.width, self.width )
 self.text:SetPos(0)
 self.text:SetAlpha(255)
 self.text:SetColor(Color(0,0,0))
 self.text:SetText( "" )
 self.smoothsize = self.height
 surface.SetFont(self.font)
 self.txtw, self.txth = surface.GetTextSize(self.title)

 self.text.Paint = function()
   context.button( 1, 1,
   0, 0,
   self.color,
   self.title,
   ((self.width - self.txtw)/2), 1,
   Color(255,255,255),
   self.font)
 end


 self.list = vgui.Create("DScrollPanel")
 self.list:SetSize( self.width+15, self.height  )
 self.list:SetPos(self.X, self.Y+50)

 self.list:SetVerticalScrollbarEnabled(false)

 self.list.VBar:SetAlpha(0)
 self.list.offset = 0
 self.list.OnVScroll = function(self, iOffset)
   self.offset = iOffset

 end
 self.list.SecondThink = function()end
 self.list.ThirdThink = function()end
 self.list.FourthThink = function()end
 self.list.Think = function(self)
   self.SecondThink()
   self.ThirdThink()
   self.FourthThink()
   self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.03, self.offsetsmooth , self.offset) or self.offset

   self:GetCanvas():SetPos( 0, self.offsetsmooth )

 end

 self.list.PerformLayout = function(self)
   local wide = self:GetWide()

   self:Rebuild()

   self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )

   if self.VBar.Enabled then wide = wide - self.VBar:GetWide() end

   self.pnlCanvas:SetWide(wide)

   self:Rebuild()
 end



 self.List = {}
end
function PANEL:Item(pnl)
  if IsValid(pnl.p) and IsValid(self.list) then
    self.list:AddItem( pnl.p )
    pnl.p:SetZPos( -2000 )
  end

  if IsValid(pnl.panel) and IsValid(self.list) then
    self.list:AddItem( pnl.panel )
    pnl.panel:SetZPos( -2000 )
  end
  if IsValid(self.list) then
    table.insert(self.List, pnl)
  end
end

function PANEL:RemoveItem(pnl)
  table.RemoveByValue(self.List, pnl)
end

function PANEL:Remove()
  if self.mainpanel then
    self.mainpanel:Remove()
  end
  if self.list then
    self.list:Remove()
  end
  table.Empty(self.List)
  self.Think = function() end
end

function PANEL:Funct(func)
 if !func then print("No function") self.func = (function() end) return end
 self.func = func
end

function PANEL:SetFont(font)
 if !font then print("No font") self.font = "Context button" return end
 self.font = font
end

function PANEL:SetTitle(title)
 if !title then print("No font") self.title = "" return end
 self.title = title
end

function PANEL:SetSize(w,h)
 if not w or not h then print("No scaling") return end
 self.width = w
 self.height = h
 if self.mainpanel and self.list then
   self.mainpanel:SetSize(self.width, self.height+50)
   self.list:SetSize(self.width, self.height)
 end
end

function PANEL:NewColor(vec)
 if not vec then print("No coloring") return end
 self.color = vec
end

function PANEL:SetPos(x,y)
 if not x or not y then return end
 self.X = x
 self.Y = y

 if self.mainpanel and self.list then
   self.mainpanel:SetPos(self.X, self.Y)
   self.list:SetPos(self.X, self.Y+50)
 end
end

function PANEL:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.list.ThirdThink = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function PANEL:SizeTo( frac, to )

  self.list.SecondThink = function()

    self.smoothsize = self.smoothsize and UT:Lerp(frac, self.smoothsize , to) or to

    if self.height > to then
      size = math.Clamp(self.smoothsize, to, self.height)
    elseif self.height < to then
      size = math.Clamp(self.smoothsize, self.height, to)
    end

    self:SetSize(self.width, size)

  end

end

function PANEL:Show( alpha, dur, delay, slidepos, dir )
 self:Move( 0.1, slidepos, dir )
 if alpha > 0 then
   self.mainpanel:Show( alpha, dur, delay, slidepos )
   self.list:AlphaTo(alpha, dur, delay)
   for k, v in pairs(self.List) do
     if !v then return end
     v:Show(255, 0.1, 0.1, 0)
   end
 else return end
end

function PANEL:Hide( dur, delay, slidepos, dir )
 self:Move( 0.1, slidepos, dir )
 self.mainpanel:Hide( dur, delay, slidepos )
 self.list:AlphaTo(0, dur, delay)
 for k, v in pairs(self.List) do
   if !v then return end
   v:Hide(0.1, 0.1, 0)
 end
end



function PANEL:Init()

 self:SetPos(0,0)
 self:SetSize(0,0)
 self:SetFont("Context button")
 self:SetTitle("")
 self:NewColor(Color(255,255,255,100))
 self:Funct(function() end)
end


vgui.Register( "Scroll", PANEL, "EditablePanel" )
