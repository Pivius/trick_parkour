PANEL = {} or PANEL

function PANEL:Create()
 self.editing = false

 self.panel = self.panel or {}
 self.panel = vgui.Create( "DModelPanel" )

 self.panel:SetSize( 0, 0 )
 self.panel:SetPos(0, 0)
 self.panel:SetAlpha(0)
 self.panel:SetColor(self.color)

 surface.SetFont(self.font)
 self.txtw, self.txth = surface.GetTextSize(self.Text)

 self.button = self.button or {}
 if !self.parent then
   self.button = vgui.Create( "DButton" )
 else
   self.button = vgui.Create( "DButton", self.parent )
 end

 self.button:SetSize( self.width, self.height )
 self.button:SetPos(self.X, self.Y)
 self.button:SetAlpha(0)
 self.button:SetText("")

 self.button.OnCursorEntered = function()

     self.panel:ColorTo(self.highlight, 0.15)

 end

 self.button.OnCursorExited = function()

     self.panel:ColorTo(self.color, 0.15)

 end

 self.button.Paint = function( )

  context.button( 0, 0,
    self.width,
    self.height,
    self.panel:GetColor(),
    self.Text,
    ((self.width - self.txtw)*0.5),
    ((self.height - self.txth)*0.5),
    Color(255,255,255,255),
    self.font)
 end

 self.button.DoClick = function()
   if not self.editing then
     self.editing = true
     self:SetTxt("")
     self.txt = self.txt or {}
     self.txt = vgui.Create( "DTextEntry" )

     self.txt:SetSize( self.width, self.height )
     self.txt:SetPos(self.X, self.Y)
     self.txt:SetFont(self.font)
     self.txt:SetText(self.Text)
     self.txt:SetTextColor(Color(255,255,255))
     self.txt:SetAlpha(0)
     self.txt:SetEditable(true)
     self.txt:SetDrawBackground(false)
     self.txt:SetEnterAllowed( true )

     self.txt:MakePopup()

     context.tempopen = true
   end
   self.txt.OnTextChanged = function()
     self:SetTxt(self.txt:GetValue())
     surface.SetFont(self.font)
     self.txtw, self.txth = surface.GetTextSize( self.Text )
   end

   self.txt.OnEnter = function()
     self.editing = false
     self.funct = self.func()
     self.txt:Remove()
   end
 end
end

function PANEL:Remove()
  if self.panel then
    self.panel:Remove()
  end
  --if self.parent then return end
  if self.button then
    self.button:Remove()
  end
end

function PANEL:Funct(func)
 if !func then print("No function") self.func = (function() end) return end
 self.func = func
end

function PANEL:SetTxt(text)
 if !text then print("No text") self.Text = "" return end
 self.Text = text
 if self.button then
   self.button.Paint = function( )

    context.button( 0, 0,
      self.width,
      self.height,
      self.panel:GetColor(),
      self.Text,
      ((self.width - self.txtw)*0.5),
      ((self.height - self.txth)*0.5),
      Color(255,255,255,255),
      self.font)
   end
 end
end

function PANEL:SetFont(font)
 if !font then print("No font") self.font = "vgui Text" return end
 self.font = font
end

function PANEL:SetParent(pnl)
  self.parent = pnl
end

function PANEL:SetSize(w,h)
  if not w or not h then print("No scaling") return end
  self.width = w
  self.height = h
  if self.button then
    self.button:SetSize(self.width, self.height)
  end
end

function PANEL:GetSize()
  return self.width, self.height
end

function PANEL:SetCol(vec)
  if not vec then print("No coloring") return end
  self.color = vec
end

function PANEL:NewHighlight(vec)
 if not vec then print("No highlight") return end
 self.highlight = vec
end

function PANEL:NewFlash(vec)
 if not vec then print("No flashing") return end
 self.flash = vec
end

function PANEL:SetPos(x,y)
  if not x or not y then return end
  self.X = x
  self.Y = y
  if self.panel and self.button then
    self.button:SetPos(self.X, self.Y)
  end
end

function PANEL:GetPos()
  return self.X, self.Y
end

function PANEL:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.button.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function PANEL:Show( alpha, dur, delay, slidepos, dir )
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.button:AlphaTo(alpha, dur, delay)
  else return end
end

function PANEL:Hide( dur, delay, slidepos, dir )
  self:Move( 0.1, slidepos, dir )
  self.button:AlphaTo(0, dur, delay)
end

function PANEL:Init()
 self:SetPos(0,0)
 self:SetSize(0,0)
 self:SetFont("Context button")
 self:SetCol(Color(255,255,255, 150))
 self:NewHighlight(Color(255,255,255,255))
 self:NewFlash(Color(255,255,255,0))
 self:Funct(function() end)
 self:SetTxt("Init")
end

vgui.Register( "EditText", PANEL, "EditablePanel" )
