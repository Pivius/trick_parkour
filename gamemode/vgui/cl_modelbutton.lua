PANEL = {} or PANEL

function PANEL:Create()

  if !self.parent then
    self.panel = vgui.Create( "DModelPanel" )
  else
    self.panel = vgui.Create( "DModelPanel", self.parent )
  end
 self.panel:SetAlpha(255)
 self.panel:SetColor(self.color)
 self.panel:SetSize( self.width, self.height )
 self.panel:SetPos(self.X, self.Y)
 self.panel:SetModel( "models/Health_model/health_model.mdl" )
 local mn, mx = self.panel.Entity:GetRenderBounds()
 local size = 0
 size = math.max( size, math.abs( mn.x ) + math.abs( mx.x ) )
 size = math.max( size, math.abs( mn.y ) + math.abs( mx.y ) )
 size = math.max( size, math.abs( mn.z ) + math.abs( mx.z ) )
 self.panel:SetCamPos( Vector( size, size, size ) )
 self.panel:SetLookAt( ( mn + mx ) * 0.5 )
 self.panel:SetDirectionalLight( BOX_FRONT, Color( 1, 1, 1 ) )
 self.panel:SetAmbientLight( self.modcolor )

 self.panel:DockMargin(self.mrg1, self.mrg2, self.mrg3, self.mrg4)
 self.panel:Dock(self.DOCK)
 self.panel:SetFOV(90)

 self.effect = false

 self.panel.OnCursorEntered = function()
     self.effect = true
 end

 self.panel.OnCursorExited = function()
     self.effect = false
 end

 self.panel.Think = function()
   if self.effect == true then
   end
 end

 self.panel.DoClick = function()
  self.funct = self.func()

 end
end

function PANEL:Remove()
  if self.panel then
    self.panel:Remove()
  end

end

function PANEL:SetDock(dock)
 if !dock then self.DOCK = NODOCK return end
 self.DOCK = dock
 --self.button:Dock( self.DOCK )
end

function PANEL:SetMargin(mrg1, mrg2, mrg3, mrg4)
 if !mrg1 then  return end
 self.mrg1 = mrg1
 self.mrg2 = mrg2
 self.mrg3 = mrg3
 self.mrg4 = mrg4


 --self.button:DockMargin( mrg1, mrg2, mrg3, mrg4 )
end

function PANEL:Funct(func)
 if !func then self.func = (function() end) return end
 self.func = func
end

function PANEL:SetParent(pnl)
  self.parent = pnl
end

function PANEL:SetSize(w,h)
 if not w or not h then print("No scaling") return end
 self.width = w
 self.height = h
 if self.panel then
   self.panel:SetSize(self.width, self.height)
 end
end

function PANEL:GetSize()
 return self.width, self.height
end

function PANEL:NewColor(vec)
 if not vec then print("No coloring") return end
 self.color = vec
end

function PANEL:ModelCol(vec)
 if not vec then print("No coloring") return end
 self.modcolor = vec
end

function PANEL:SetPos(x,y)
 if not x or not y then return end
 self.X = x
 self.Y = y
 if self.panel then
   self.panel:SetPos(x, y)
 end
end

function PANEL:GetPos()
 return self.X, self.Y
end

function PANEL:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.panel.SecondThink = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function PANEL:Show( alpha, dur, delay, slidepos, dir )
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.panel:AlphaTo(alpha, dur, delay)
  else return end
end

function PANEL:Hide( dur, delay, slidepos, dir )
  self:Move( 0.1, slidepos, dir )
  self.panel:AlphaTo(0, dur, delay)
end


function PANEL:Init()
 self:SetPos(0,0)
 self:SetDock(NODOCK)
 self:SetMargin(0, 0, 0, 0.1)
 -- self:SetPos(0,0)
 self:ModelCol(Color(255,255,255,255))
 self:NewColor(Color(255,255,255,150))
 self:Funct(function() end)
end


vgui.Register( "ModelButton", PANEL, "EditablePanel" )
