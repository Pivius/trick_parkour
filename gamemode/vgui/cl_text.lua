PANEL = {} or PANEL


 function PANEL:Create()
   if !self.parent then
     self.text = vgui.Create( "DLabel" )
   else
     self.text = vgui.Create( "DLabel", self.parent )
   end

   self.text:SetSize( self.width, self.height )
   self.text:SetPos(self.X, self.Y)
   self.text:SetAlpha(0)
   self.text:SetColor(self.color)
   self.text:SetText( "" )

   self.text.Paint = function()
     context.button( 0, 0,
      0,
      0,
      self.color,
      self.Text,
      0,
      0,
      Color(255,255,255,255),
      self.font)
   end

 end

function PANEL:Remove()
  if !IsValid(self.text) then return end
    self.text:Remove()
    self.Think = function() end
end

function PANEL:SetFont(font)
  if !font then print("No font")return end
  self.font = font
end

function PANEL:SetTxt(txt)
  if !txt then print("No text")return end
  self.Text = txt
  if IsValid(self.text) then
    self.text.Paint = function()
      context.button( 0, 0,
       0,
       0,
       self.color,
       self.Text,
       0,
       0,
       Color(255,255,255,255),
       self.font)
    end
  end
end

function PANEL:SetSize(w,h)
  if not w or not h then print("No scaling") return end
  self.width = w
  self.height = h
  if self.text then
    self.text:SetSize(self.width, self.height)
  end
end

function PANEL:GetSize()
  return self.width, self.height
end

function PANEL:SetCol(vec)
  if not vec then print("No coloring") return end
  self.color = vec
end

function PANEL:SetPos(x,y)
  if not x or not y then return end
  self.X = x
  self.Y = y
  if self.text then
    self.text:SetPos(self.X, self.Y)
  end
end
function PANEL:SetParent(pnl)
  self.parent = pnl
end

function PANEL:GetPos()
  return self.X, self.Y
end

function PANEL:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.text.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function PANEL:Show( alpha, dur, delay, slidepos, dir )
  if !IsValid(self.text) then return end
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.text:AlphaTo(alpha, dur, delay)
  else return end
end

function PANEL:Hide( dur, delay, slidepos, dir )
  if !IsValid(self.text) then return end
  self:Move( 0.1, slidepos, dir )
  self.text:AlphaTo(0, dur, delay)
end

function PANEL:Init()
  self:SetPos(0,0)
  self:SetSize(0,0)
  self:SetTxt("Init")
  self:SetFont("Context button")
  self:SetCol(Color(0, 0, 0, 0))
end

vgui.Register( "Text", PANEL, "EditablePanel" )
