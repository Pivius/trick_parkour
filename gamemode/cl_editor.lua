Editor.UI = {} or Editor.UI

hook.Add("OnEntityCreated", "Initialize editor", function(ent)
  if ent == LocalPlayer() then
    GetSet("panelEditing", "Player")
    LocalPlayer():SetpanelEditing(nil)
  end
end)

Zones = {}
Tricks = {}
SequenceOpen  = false
IgnoreOpen    = false
Editing       = false
Draw          = false
posMode       = "Feet"
Minimized     = false

CurrentZone = {
  Name = "Untitled",
  ID = 1,
  Point1 = Vector(0, 0, 0),
  Point2 = Vector(0, 0, 0),
  Mode = "Enter"
}

CurrentTrick = {
  Name = "Untitled",
  ID = 1,
  Sequence = {},
  Limit = 0
}

net.Receive("Update Zone", function(len, ply)
  local cache = net.ReadTable()
  if UT:TblCompare(Zones,cache) then return end
  UT:MergeTableTo(Zones,cache)

end)

net.Receive("Update Tricks", function(len, ply)
  local cache = net.ReadTable()
  if UT:TblCompare(Tricks,cache) then return end
  UT:MergeTableTo(Tricks,cache)

end)

Editor.Zones = {}
Editor.Tricks = {}
Editor.Panels = {}

function Editor.RenderBox(pos1, pos2)
  if !pos1 then pos1= Vector(0,0,0) end
  if !pos2 then pos2= Vector(0,0,0) end
	local Col, Width = Color( 0, 255, 255, 255 ), 1
  local DrawMaterial = Material( "materials/zone/zone.png" )
  if !Draw then
    pos1 = Vector(0,0,0)
    pos2 = Vector(0,0,0)
  end
  hook.Add("PreDrawTranslucentRenderables", "draws a box", function()
    render.SetMaterial( DrawMaterial )
  	render.DrawBeam( Vector(pos1.x, pos1.y, pos1.z), Vector(pos1.x, pos2.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos1.z), Vector(pos2.x, pos2.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos1.z), Vector(pos2.x, pos1.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos1.z), Vector(pos1.x, pos1.y, pos1.z), Width, 0, 1, Col )

  	render.DrawBeam( Vector(pos1.x, pos1.y, pos2.z), Vector(pos1.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos2.z), Vector(pos2.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos2.z), Vector(pos2.x, pos1.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos2.z), Vector(pos1.x, pos1.y, pos2.z), Width, 0, 1, Col )

  	render.DrawBeam( Vector(pos1.x, pos1.y, pos1.z), Vector(pos1.x, pos1.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos1.z), Vector(pos1.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos1.z), Vector(pos2.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos1.z), Vector(pos2.x, pos1.y, pos2.z), Width, 0, 1, Col )
  end)
end

function Editor.UI:Reset()
  for k ,v in pairs(vgui.GetWorldPanel():GetChildren()) do
    if string.match(tostring(v), "Editor") then
      v:Remove()
    end
  end
end
Editor.UI:Reset()

function Editor.CreateButton(name, x, y, w, h, text, font, parent, func)
  local PANEL = vgui.Create( "Button", nil, "Editor " .. name )
  table.insert(Editor.Panels, PANEL)
  PANEL:SetSize(w,h) --NORMAL (175,36)
  PANEL:NewColor(Color(0,0,0,200))
  PANEL:NewHighlight(Color(0,0,0,230))
  PANEL:NewFlash(Color(255,255,255,150))
  PANEL:SetFont(font)
  if parent != nil then
    PANEL:SetParent(parent)
  end
  PANEL:SetPos(x,y)
  PANEL:SetTxt(text)
  PANEL:Funct(function(pnl)
    pnl.panel:SetColor(pnl.flash)
    pnl.panel:ColorTo(pnl.highlight, 0.1, 0.1)
    local funct = func(pnl)
  end)
  PANEL:Create()

  return PANEL
end

function Editor.CreateText(name, x, y, text, font, parent)
  local TEXT = vgui.Create( "Text", nil, "Editor " .. name )
  table.insert(Editor.Panels, TEXT)
  TEXT:SetCol(Color(0,0,0))
  TEXT:SetFont(font)
  surface.SetFont(font)
  TEXT:SetTxt(text)
  local txtw, txth = surface.GetTextSize(TEXT.Text)
  TEXT:SetSize(txtw,ScrH()*0.55)
  TEXT:SetPos(x-(txtw/2),y)
  TEXT:SetParent(parent)
  TEXT:Create()
  TEXT:Show( 255, 0.5, 0, x-(txtw/2), "x")
  return TEXT
end

function Editor.List(panel, func, text, ident, tbl, type)
  local ADD = vgui.Create( "Button", nil, "Editor List " .. ident )
  table.insert(tbl, ident)
  table.insert(Editor.Panels, ADD)

  ADD:SetSize(175,36)
  ADD:NewColor(Color(0,0,0,200))
  ADD:NewHighlight(Color(0,0,0,230))
  ADD:NewFlash(Color(255,255,255,150))
  ADD:SetPos(0,0)
  ADD:SetParent(panel.list)
  ADD:SetDock(TOP)
  ADD:SetMargin(20, 0, 0, 1)
  ADD:SetTxt(text)
  ADD:Funct(function()
    ADD.panel:SetColor(ADD.flash)
    ADD.panel:ColorTo(ADD.highlight, 0.1, 0.1)
    local funct = func(ADD)
  end)
  ADD:Create()

  panel:Item(ADD)
end

function Editor.Draw()

  if !LocalPlayer():GetEditing() then return end
  table.Empty(Editor.Tricks)
  table.Empty(Editor.Zones)

  -- Main Panel
  local mainPanel = vgui.Create( "vgui panel", nil, "Editor mainPanel" )
  table.insert(Editor.Panels, mainPanel)
  mainPanel:SetSize(ScrW(),ScrH()*0.2)
  mainPanel:SetCol(Color(0,0,0,200))
  mainPanel:SetPos(0,0)
  mainPanel:Create()
  mainPanel:Hide( 0, 0, 0, "y")
  timer.Simple(0.12, function() mainPanel:Show( 255, 0.5, 0, 22 , "y") end)

  --Trick Editor Text
  Editor.CreateText("TRICK EDIT", mainPanel.width*0.5, 5, "TRICK EDITOR", "Editor Text", mainPanel.bg)

  Editor.CreateButton("Help", mainPanel.width*0.5 - 25/2, mainPanel.height-25, 25, 25, "?", "HUD Name", mainPanel.bg,
  function(self)
  end)
  /*
  ████████ ██████  ██  ██████ ██   ██ ███████
     ██    ██   ██ ██ ██      ██  ██  ██
     ██    ██████  ██ ██      █████   ███████
     ██    ██   ██ ██ ██      ██  ██       ██
     ██    ██   ██ ██  ██████ ██   ██ ███████
  */
  --Trick List
  local trickList = vgui.Create( "Scroll", nil, "Editor trickList" )
  table.insert(Editor.Panels, trickList)
  trickList:SetSize(ScrW()*0.17,-50)
  trickList:NewColor(Color(0,0,0,200))
  trickList:SetPos((ScrW()*0.6) - (trickList.width - 150)*0.5,ScrH()*0.218 + 10)
  trickList:SetTitle("Tricks")
  trickList:Funct(function() end)
  trickList:Create()
  trickList:Show( 255, 0.5, 0, (ScrW()*0.6) - (trickList.width - 150)*0.5) -- Get button position then get the list and button difference/2
  trickList.visible = false
  trickList.list.FourthThink = function()
    for _,tricks in pairs(Tricks) do
      if !UT:TblContain(Editor.Tricks, tricks.trick) then
        Editor.List(trickList, function()end, tricks.trick, tricks.trick, Editor.Tricks, "Trick")
      end
    end
  end
  --Trick Menu
  local trickMenu = Editor.CreateButton("Tricks", mainPanel.width*0.6, mainPanel.height*0.6, 150, 60, "Tricks", "Editor Text", mainPanel.bg,
  function(mnu)
    if Editing then return end
    if !trickList.visible then
      trickList.visible = true
      trickList:SizeTo( 0.1, ScrH()*0.35 )
    else
      trickList.visible = false
      trickList:SizeTo( 0.1, -50 )
    end
  end)
  Editor.CreateText("Tricks", mainPanel.width*0.6 + (150/2), mainPanel.height*0.2, "9", "Tricks", mainPanel.bg)

  Editor.CreateButton("Create Trick But", trickList.width-15, 0, 15, 15, "+", "HUD Name", trickList.mainpanel.bg,
  function(self)
  end)
  /*
  ███████  ██████  ███    ██ ███████ ███████
     ███  ██    ██ ████   ██ ██      ██
    ███   ██    ██ ██ ██  ██ █████   ███████
   ███    ██    ██ ██  ██ ██ ██           ██
  ███████  ██████  ██   ████ ███████ ███████
  */

  --Zone List
  local zoneList = vgui.Create( "Scroll", nil, "Editor zoneList" )

  --Zone Menu
  local zoneMenu = Editor.CreateButton("Zones", mainPanel.width*0.3, mainPanel.height*0.6, 150, 60, "Zones", "Editor Text", mainPanel.bg,
  function()
    if Editing then return end
    if !zoneList.visible then
      zoneList.visible = true
      zoneList:SizeTo( 0.1, ScrH()*0.35 )
    else
      zoneList.visible = false
      zoneList:SizeTo( 0.1, -50 )
    end
  end)

  -- Zone Edit panel
  local ZoneEdit = vgui.Create( "vgui panel", nil, "Editor ZoneEdit" )
  table.insert(Editor.Panels, ZoneEdit)
  ZoneEdit:SetSize(ScrW()*0.3,0)
  ZoneEdit:SetCol(Color(0,0,0,200))
  ZoneEdit:SetPos(ScrW()/2 - (ScrW()*0.3/2),ScrH()*0.2181 + 10)
  ZoneEdit:Create()
  ZoneEdit:Show( 255, 0.5, 0, ScrH()*0.2181, "y")
  --timer.Simple(0.12, function() ZoneEdit:Show( 255, 0.5, 0, ScrH()*0.2181, "y") end)

  ------- Every thing between these lines is inside the Zone Editor

  -- Minimize Button for Zone Editor
  local ZoneEditMinimize = Editor.CreateButton("ZoneEditMinimize", (ZoneEdit.width*0.5) - 150/2, 0, 150, 25, "▲", "Editor Text", ZoneEdit.bg,
  function(self)
    if Minimized then
      Minimized = false
      self.Text = "▲"
      ZoneEdit:SizeTo( 0.1, ScrH()*0.434 )
    else
      Minimized = true
      self.Text = "▼"
      ZoneEdit:SizeTo( 0.1, 25 )
    end
  end)

  -- Back Button for Zone Editor
  local ZoneEditBack = Editor.CreateButton("ZoneEditBack", (ZoneEdit.width)-15, 0, 15, 15, "X", "HUD Name", ZoneEdit.bg,
  function(self)
    Editing = false
    Draw    = false
    ZoneEdit:SizeTo( 0.1, -50 )
    Editor.RenderBox()
    LocalPlayer():SetpanelEditing(nil)
    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = CurrentZone.Mode
    }
    Minimized = false
    ZoneEditMinimize.Text = "▲"
  end)

  -- Remove Button for Zone Editor
  local ZoneEditBack = Editor.CreateButton("ZoneEditRemove", (ZoneEdit.width)-15 -55.5, 0, 55, 15, "Remove", "HUD Name", ZoneEdit.bg,
  function(self)
    Editing = false
    Draw    = false
    ZoneEdit:SizeTo( 0.1, -50 )
    Editor.RenderBox()
    LocalPlayer():SetpanelEditing(nil)
    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = CurrentZone.Mode
    }
    Minimized = false
    ZoneEditMinimize.Text = "▲"
  end)

  -- Edit trick name
  local ZoneEditTitleEdit = vgui.Create( "EditText", nil, "Editor ZETE" )
  table.insert(Editor.Panels, ZoneEditTitleEdit)
  ZoneEditTitleEdit:SetSize(175,40)
  ZoneEditTitleEdit:SetCol(Color(0,0,0,200))
  ZoneEditTitleEdit:NewHighlight(Color(0,0,0,230))
  ZoneEditTitleEdit:NewFlash(Color(255,255,255,150))
  ZoneEditTitleEdit:SetParent(ZoneEdit.bg)
  ZoneEditTitleEdit:SetPos(0,70)
  ZoneEditTitleEdit:SetTxt(CurrentZone.Name)
  ZoneEditTitleEdit:Funct(function()
    ZoneEditTitleEdit.panel:SetColor(ZoneEditTitleEdit.flash)
    ZoneEditTitleEdit.panel:ColorTo(ZoneEditTitleEdit.highlight, 0.1, 0.1)
    CurrentZone.Name = ZoneEditTitleEdit.Text
  end)
  ZoneEditTitleEdit:Create()
  ZoneEditTitleEdit:Show( 255, 0.5, 0, ZoneEdit.width*0.5 - (175/2), "x")

  -- Displays what zone you're editing
  local ZoneEditTitle = Editor.CreateText("Edit Title", (ZoneEdit.width*0.5), 40, CurrentZone.Name, "Editor Text", ZoneEdit.bg)

  -- Set Point1
  Editor.CreateButton("ZoneEditP1", (ZoneEdit.width*0.5) - 150 - 3, 115, 150, 60, "Point 1", "Editor Text", ZoneEdit.bg,
  function()
    if posMode == "Feet" then
      CurrentZone.Point1 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point1 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  -- Set Point2
  Editor.CreateButton("ZoneEditP2", (ZoneEdit.width*0.5) + 3, 115, 150, 60, "Point 2", "Editor Text", ZoneEdit.bg,
  function()
    if posMode == "Feet" then
      CurrentZone.Point2 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point2 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  -- Where you position the points
  Editor.CreateButton("ZoneEditMode", (ZoneEdit.width*0.5) - 150 - 3, 180, 150, 60, "Pos: " .. posMode, "Editor Text", ZoneEdit.bg,
  function(self)
    if posMode == "Feet" then
      posMode = "Aim"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    else
      posMode = "Feet"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  -- Set if you'd like to surf, enter or walk in it
  local ZoneEditMode = Editor.CreateButton("ZoneEditMode", (ZoneEdit.width*0.5) + 3, 180, 150, 60, "Mode: " .. CurrentZone.Mode, "Editor Text", ZoneEdit.bg,
  function(self)
    if CurrentZone.Mode == "Enter" then
      CurrentZone.Mode = "Surfing"
      self:SetTxt("Mode: Surfing")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Surfing" then
      CurrentZone.Mode = "Ground"
      self:SetTxt("Mode: Ground")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Ground" then
      CurrentZone.Mode = "Enter"
      self:SetTxt("Mode: Enter")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  -- Save settings
  Editor.CreateButton("ZoneEditSave", (ZoneEdit.width*0.5) - 150/2, 245, 150, 60, "Save", "Editor Text", ZoneEdit.bg,
  function(self)
    Draw = false
    Editing = false
    Editor.RenderBox()
    table.SortByMember( Zones, "ID", true )
    for k ,v in pairs(Zones) do
      if v.Name == LocalPlayer():GetpanelEditing() then
        CurrentZone.ID = v.ID
        Zones[v.ID] = CurrentZone
        timer.Simple(0.1, function() PrintTable(Zones) end)
      end
    end
    net.Start("Update")
      net.WriteTable(CurrentZone)
    net.SendToServer()
    ZoneEdit:SizeTo( 0.1, -50 )
    LocalPlayer():SetpanelEditing(nil)
    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = CurrentZone.Mode
    }
  end)
  -------

  -- zonelist continuation
  table.insert(Editor.Panels, zoneList)
  zoneList:SetSize(ScrW()*0.17,-50)
  zoneList:NewColor(Color(0,0,0,200))
  zoneList:SetPos((ScrW()*0.3) - (zoneList.width - 150)*0.5,ScrH()*0.218 + 10)
  zoneList:SetTitle("Zones")
  zoneList:Funct(function() end)
  zoneList:Create()
  zoneList:Show( 255, 0.5, 0, (ScrW()*0.3) - (zoneList.width - 150)*0.5 ) -- Get button position then get the list and button difference/2
  zoneList.visible = false
  zoneList.Think = function()
    for _,zones in pairs(Zones) do
      if !UT:TblContain(Editor.Zones, zones.Name) then
        Editor.List(zoneList,
        function(self)
          -- GENERAL
          Draw = true
          Editing = true
          zoneList.visible = false
          zoneList:SizeTo( 0.1, -50 )
          trickList.visible = false
          trickList:SizeTo( 0.1, -51 )
          LocalPlayer():SetpanelEditing(self.Text)
          ZoneEdit:SizeTo( 0.1, ScrH()*0.434 )
          -- info
          if zones.Name == self.Text then
            CurrentZone = {
              Name = zones.Name,
              ID = zones.ID,
              Point1 = zones.Point1,
              Point2 = zones.Point1,
              Mode = zones.Mode
            }

            ZoneEditTitleEdit.Text = CurrentZone.Name
            ZoneEditTitle.Text     = CurrentZone.Name
            ZoneEditMode.Text      = "Mode: " .. CurrentZone.Mode
            timer.Simple(0.1, function() ZoneEditMode:UpdatePaint() end)
            Editor.RenderBox(zones.Point1, zones.Point2)
          end
        end, zones.Name, zones.Name, Editor.Zones, "Zone")
      end
    end
  end
  Editor.CreateText("Zones", mainPanel.width*0.3 + (150/2), mainPanel.height*-0.06, "C", "Zones", mainPanel.bg)

  -- Zone Create panel
  local ZoneCreate = vgui.Create( "vgui panel", nil, "Editor ZoneCreate" )
  table.insert(Editor.Panels, ZoneCreate)
  ZoneCreate:SetSize(ScrW()*0.3,0)
  ZoneCreate:SetCol(Color(0,0,0,200))
  ZoneCreate:SetPos(ScrW()/2 - (ScrW()*0.3/2),ScrH()*0.2181 + 10)
  ZoneCreate:Create()
  ZoneCreate:Show( 255, 0.5, 0, ScrH()*0.2181, "y")

  -- Create zones button
  Editor.CreateButton("Create Zone But", zoneList.width-15, 0, 15, 15, "+", "HUD Name", zoneList.mainpanel.bg,
  function(self)
    -- GENERAL
    Draw = true
    Editing = true
    zoneList.visible = false
    zoneList:SizeTo( 0.1, -50 )
    trickList.visible = false
    trickList:SizeTo( 0.1, -51 )

    ZoneCreate:SizeTo( 0.1, ScrH()*0.434 )
  end)

  ------- Every thing between these lines is inside the Zone Creator

  -- Minimize Button for Zone Creator
  local ZoneCreateMinimize = Editor.CreateButton("ZoneCreateMinimize", (ZoneCreate.width*0.5) - 150/2, 0, 150, 25, "▲", "Editor Text", ZoneCreate.bg,
  function(self)
    if Minimized then
      Minimized = false
      self.Text = "▲"
      ZoneCreate:SizeTo( 0.1, ScrH()*0.434 )
    else
      Minimized = true
      self.Text = "▼"
      ZoneCreate:SizeTo( 0.1, 25 )
    end
  end)

  -- Back Button for Zone Creator
  local ZoneCreateBack = Editor.CreateButton("ZoneCreateBack", (ZoneCreate.width)-15, 0, 15, 15, "X", "HUD Name", ZoneCreate.bg,
  function(self)
    Editing = false
    Draw    = false
    ZoneCreate:SizeTo( 0.1, -50 )
    Editor.RenderBox()
    LocalPlayer():SetpanelEditing(nil)
    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = CurrentZone.Mode
    }
    Minimized = false
    ZoneCreateMinimize.Text = "▲"
  end)

  -- Change zone name
  local ZoneCreateTitleEdit = vgui.Create( "EditText", nil, "Editor ZCTE" )
  table.insert(Editor.Panels, ZoneCreateTitleEdit)
  ZoneCreateTitleEdit:SetSize(175,40)
  ZoneCreateTitleEdit:SetCol(Color(0,0,0,200))
  ZoneCreateTitleEdit:NewHighlight(Color(0,0,0,230))
  ZoneCreateTitleEdit:NewFlash(Color(255,255,255,150))
  ZoneCreateTitleEdit:SetParent(ZoneCreate.bg)
  ZoneCreateTitleEdit:SetPos(0,70)
  ZoneCreateTitleEdit:SetTxt(CurrentZone.Name)
  ZoneCreateTitleEdit:Funct(function()
    ZoneCreateTitleEdit.panel:SetColor(ZoneCreateTitleEdit.flash)
    ZoneCreateTitleEdit.panel:ColorTo(ZoneCreateTitleEdit.highlight, 0.1, 0.1)
    CurrentZone.Name = ZoneCreateTitleEdit.Text
  end)
  ZoneCreateTitleEdit:Create()
  ZoneCreateTitleEdit:Show( 255, 0.5, 0, ZoneCreate.width*0.5 - (175/2), "x")

  -- Set Point1
  Editor.CreateButton("ZoneCreateP1", (ZoneCreate.width*0.5) - 150 - 3, 115, 150, 60, "Point 1", "Editor Text", ZoneCreate.bg,
  function()
    if posMode == "Feet" then
      CurrentZone.Point1 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point1 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  -- Set Point2
  Editor.CreateButton("ZoneCreateP2", (ZoneCreate.width*0.5) + 3, 115, 150, 60, "Point 2", "Editor Text", ZoneCreate.bg,
  function()
    if posMode == "Feet" then
      CurrentZone.Point2 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point2 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  -- Where you position the points
  Editor.CreateButton("ZoneCreateMode", (ZoneCreate.width*0.5) - 150 - 3, 180, 150, 60, "Pos: " .. posMode, "Editor Text", ZoneCreate.bg,
  function(self)
    if posMode == "Feet" then
      posMode = "Aim"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    else
      posMode = "Feet"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  -- Set if you'd like to surf, enter or walk in it
  local ZoneCreateMode = Editor.CreateButton("ZoneCreateMode", (ZoneCreate.width*0.5) + 3, 180, 150, 60, "Mode: " .. CurrentZone.Mode, "Editor Text", ZoneCreate.bg,
  function(self)
    if CurrentZone.Mode == "Enter" then
      CurrentZone.Mode = "Surfing"
      self:SetTxt("Mode: Surfing")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Surfing" then
      CurrentZone.Mode = "Ground"
      self:SetTxt("Mode: Ground")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Ground" then
      CurrentZone.Mode = "Enter"
      self:SetTxt("Mode: Enter")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  -- Save settomgs
  Editor.CreateButton("ZoneCreateSave", (ZoneCreate.width*0.5) - 150/2, 245, 150, 60, "Save", "Editor Text", ZoneCreate.bg,
  function(self)
    Draw = false
    Editing = false
    Editor.RenderBox()
    table.SortByMember( Zones, "ID", true )
    CurrentZone.ID = table.Count(Zones)+1
    net.Start("Create Zone")
      net.WriteTable(CurrentZone)
    net.SendToServer()
    ZoneCreate:SizeTo( 0.1, -50 )
    LocalPlayer():SetpanelEditing(nil)
    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = CurrentZone.Mode
    }
  end)
  -------

end
Editor.Draw()
hook.Add("UpdateDraw", "Draws the Editor UI", Editor.Draw)


function Editor.Remove()

  if LocalPlayer():GetEditing() then return end
  for k ,v in pairs(Editor.Panels) do

    v:Hide( 0.1, 0, -50, "x", 0.01)
    timer.Create("ERemove " .. tostring(v), 0.5, 1, function()
      v:Remove()
      table.RemoveByValue(Editor.Panels, v)

    end)
  end

end
hook.Add("UpdateDraw", "Remove the Editor UI", Editor.Remove)
--hook.Add("Think", "Removes the Editor UI", Editor.Remove)
