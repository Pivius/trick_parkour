PANEL = {} or PANEL

function PANEL:Create()
 self.panel = vgui.Create( "DModelPanel" )

 self.panel:SetAlpha(255)
 self.panel:SetColor(self.color)
 self.panel:SetSize( 0, 0 )
 self.panel:SetPos(0, 0)
 surface.SetFont(self.font)
 self.txtw, self.txth = surface.GetTextSize(self.Text)
 if !self.parent then
   self.p = vgui.Create( "DFrame" )
 else
   self.p = vgui.Create( "DFrame", self.parent )
 end
 self.p:SetSize( self.width, self.height )
 self.p:SetPos(self.X, self.Y)
 self.p:SetAlpha(255)
 self.p:SetDraggable(false)
 self.p:Dock( self.DOCK ) -- top
 self.p:DockMargin( self.mrg1, self.mrg2, self.mrg3, self.mrg4 )
 self.p:ShowCloseButton( false )
 self.p:SetTitle( "" )

 self.button = vgui.Create( "DButton", self.p )
 self.button:SetSize( self.width, self.height )
 self.button:SetPos(0, 0)
 self.button:DockPadding( self.mrg1, self.mrg2, self.mrg3, self.mrg4 )
 self.button:SetAlpha(255)
 self.button:SetText("")

 self.button.OnCursorEntered = function()
     self.panel:ColorTo(self.highlight, 0.15)
 end

 self.button.OnCursorExited = function()
     self.panel:ColorTo(self.color, 0.15)
     self.p.Paint = function( )
     end
 end

 self.p.Paint = function( )

 end

 self.button.Paint = function( )

  context.button( 0, 0,
    self.width,
    self.height,
    self.panel:GetColor(),
    self.Text,
    ((self.width - self.txtw)*0.5),
    ((self.height - self.txth)*0.5),
    Color(255,255,255,255),
    self.font)
 end
 self.button.DoClick = function()
  self.funct = self.func(self)

 end
end

function PANEL:Remove()
  if IsValid(self.panel) then
    self.panel:Remove()
  end
  --if self.parent then return end
  if IsValid(self.button) then
    self.button:Remove()
  end
  if IsValid(self.p) then
    self.p:Remove()
  end
end

function PANEL:UpdatePaint()
  surface.SetFont(self.font)
  self.txtw, self.txth = surface.GetTextSize(self.Text)
  self.button.Paint = function( )

   context.button( 0, 0,
   self.width,
      self.height,
      self.panel:GetColor(),
      self.Text,
      ((self.width - self.txtw)*0.5),
      ((self.height - self.txth)*0.5),
      Color(255,255,255,255),
      self.font)
  end
end

function PANEL:Funct(func)
 if !func then self.func = (function() end) return end
 self.func = func
end

function PANEL:SetDock(dock)
 if !dock then self.DOCK = NODOCK return end
 self.DOCK = dock
 --self.button:Dock( self.DOCK )
end

function PANEL:SetMargin(mrg1, mrg2, mrg3, mrg4)
 if !mrg1 then  return end
 self.mrg1 = mrg1
 self.mrg2 = mrg2
 self.mrg3 = mrg3
 self.mrg4 = mrg4


 --self.button:DockMargin( mrg1, mrg2, mrg3, mrg4 )
end

function PANEL:SetTxt(text)
 if !text then print("No text") self.Text = "" return end
 self.Text = text
end

function PANEL:SetFont(font)
 if !font then print("No font") self.font = "vgui Text" return end
 self.font = font
end

function PANEL:SetParent(pnl)
  self.parent = pnl
end

function PANEL:SetSize(w,h)
 if not w or not h then print("No scaling") return end
 self.width = w
 self.height = h
 if self.panel and self.button then
   self.button:SetSize(self.width, self.height)
 end
end

function PANEL:GetSize()
 return self.width, self.height
end

function PANEL:NewColor(vec)
 if not vec then print("No coloring") return end
 self.color = vec
end

function PANEL:NewHighlight(vec)
 if not vec then print("No highlight") return end
 self.highlight = vec
end

function PANEL:NewFlash(vec)
 if not vec then print("No flashing") return end
 self.flash = vec
end

function PANEL:SetPos(x,y)
 if not x or not y then return end
 self.X = x
 self.Y = y
 if self.button and self.p then
   --self.button:SetPos(x, y)
   self.p:SetPos(x, y)
 end
end

function PANEL:GetPos()
 return self.X, self.Y
end

function PANEL:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.button.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then

      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end

      self:SetPos( self.X, pos )
    end

  end
end

function PANEL:Show( alpha, dur, delay, slidepos, dir )
  if !IsValid(self.button) then return end
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.button:AlphaTo(alpha, dur, delay)
  else return end
end

function PANEL:Hide( dur, delay, slidepos, dir, frac )
  if !IsValid(self.button) then return end
  if !frac then frac = 0.1 end
  if self.button then
    self:Move( frac, slidepos, dir )
    self.button:AlphaTo(0, dur, delay)
  end
end


function PANEL:Init()

 -- self:SetPos(0,0)
 self:SetSize(0,0)
 self:SetFont("Context button")
 self:SetDock(NODOCK)
 self:SetMargin(0, 0, 0, 0.1)
 self:NewColor(Color(255,255,255,150))
 self:NewHighlight(Color(255,255,255,255))
 self:NewFlash(Color(255,255,255,0))
 self:Funct(function() end)
 self:SetTxt("Init")
end


vgui.Register( "Button", PANEL, "EditablePanel" )
