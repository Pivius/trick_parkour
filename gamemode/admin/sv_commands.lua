local meta = FindMetaTable( "Player" )

Admin.Commands = {} or Admin.Commands
Admin.Commands.cmds = {}

util.AddNetworkString("VGAM_cmd")
util.AddNetworkString("VGAM_sendCMD")

function Admin.Commands:LoadCMD()
  local files = file.Find( "parkour/gamemode/admin/cmds/*.lua", "LUA" )
  local tbl = {}
  for _,v in pairs( files ) do
    --Admin.Commands.cmds
    if string.match(v, "sv_") then
      include( "parkour/gamemode/admin/cmds/" .. v )
    elseif string.match(v, "cl_") then
      if CLIENT then
        include( "parkour/gamemode/admin/cmds/" .. v )
      else
        AddCSLuaFile( "parkour/gamemode/admin/cmds/" .. v )
      end
    end
  end

end

/*--------------------------------------------------------------------------
 NAME - Notify
 FUNCTION -
--------------------------------------------------------------------------*/
function meta:Notify(ply, cmd, target)
  local note = Notifications:Get( "Admin Success Command", {cmd} )
  if target and target != "" then
    chat.Text(ply, Color(0,75,255), "[Very Good Admin Mod]", Color(255,255,255, 255), note, " on ", target)
  elseif !target then
    chat.Text(ply, Color(0,75,255), "[Very Good Admin Mod]", Color(255,255,255, 255), note )
  end
end


/*--------------------------------------------------------------------------
 NAME - createCMD
 FUNCTION - Creates a command
--------------------------------------------------------------------------*/
function Admin.Commands:createCMD(cmd, ident, callback)

  Admin.Commands.cmds[cmd] = callback

  concommand.Add( "vgam_" .. cmd, callback, {"vgam"})
end

/*--------------------------------------------------------------------------
 NAME - autoComplete
 FUNCTION - AutoCompletes name
--------------------------------------------------------------------------*/
function Admin.Commands:autoComplete( varargs )

	local tbl = {}

	for k, v in pairs( player.GetAll() ) do
		local nick = v:Nick()
		if string.find( string.lower( nick ), varargs ) then
			nick = "\"" .. nick .. "\"" -- We put quotes around it incase players have spaces in their names.

			table.insert( tbl, nick )
		end
	end

	return tbl[1]
end

/*--------------------------------------------------------------------------
 NAME - autoCompleteConVar
 FUNCTION - AutoCompletes ConVar
--------------------------------------------------------------------------*/
function Admin.Commands:autoCompleteConVar( cmd, args )

	local tbl = {}

	for k, v in pairs( Admin.Commands.cmds ) do
		if string.find( string.lower( v ), args ) then

			table.insert( tbl, "vgam " .. v )
		end
	end

	return tbl
end

/*--------------------------------------------------------------------------
 NAME - runCMD
 FUNCTION - Calls a hook when using a command
--------------------------------------------------------------------------*/
function Admin.Commands.runCMD(cmd, vararg, ply)
  if !cmd then return end
  local cmd    = string.lower(cmd)
  if !vararg then
    vararg = {}
  end



  if Admin.Commands.cmds[cmd] and !ply then
    for ident,func in pairs(Admin.Commands.cmds) do
      if ident == cmd then
        return func("root",unpack(vararg))
      end
    end
  elseif ply then
    local rank = Admin.Ranks[ply:GetRank()]
    if Admin.Commands.cmds[cmd] and (UT:TblContain(rank["cmds"], "*") or UT:TblContain(rank["cmds"], cmd)) then
      for ident,func in pairs(Admin.Commands.cmds) do
        if ident == cmd then
          return func(ply,unpack(vararg))
        end
      end
    end
  end
end
net.Receive("VGAM_cmd", function(len, ply)
  local ply = net.ReadEntity()
  local cmd = net.ReadString()
  local args = net.ReadTable()

  Admin.Commands.runCMD(cmd, args, ply)
end)
concommand.Add("vgam", function(ply, cmd, args)
  --Admin.Commands.runCMD(cmd, )
  local varargs = args
  local cmd = table.remove(varargs, 1)
  Admin.Commands.runCMD(cmd, varargs)
end)

hook.Add("PlayerSay", "Command", function(ply, txt, team)
	if txt:sub(1, 1) == "!" or txt:sub(1, 1) == "/" then
		txt = txt:sub(2)
		local args = string.Split(txt, " ")
		local cmd = table.remove(args, 1)

		if cmd != "" then
      Admin.Commands.runCMD(cmd, args, ply)
    else
      chat.Text(ply, Admin.Colors["VGAM"], "[Very Good Admin Mod]", Admin.Colors["error"], " How about you enter a command?" )
    end
    if cmd == "saveloc" then
      ply:SetcurLoc(ply:GetPos())
      ply:SetcurAng(ply:EyeAngles())
      ply:SetcurSpeed(ply:GetVelocity())
      net.Start("SetTele")
        net.WriteEntity(ply)
        net.WriteVector(ply:GetcurLoc())
        net.WriteAngle(ply:GetcurAng())
        net.WriteVector(ply:GetcurSpeed())
      net.Send(ply)
      local plypos = (math.Round(ply:GetPos().x, 3) .. ", " .. math.Round(ply:GetPos().y, 3) .. ", " .. math.Round(ply:GetPos().z, 3))
      chat.Text(ply, Admin.Colors["normal"], "Location saved at ( ", Admin.Colors["VGAM"], plypos , Admin.Colors["normal"], " )" )
    elseif cmd == "gotoloc" then
      ply:Teleport()
    end
    /*
		table.remove(args, 1)
		cmd = string.lower(cmd)
		cmd = maestro.commandaliases[cmd] or cmd
		if maestro.commands[cmd] then
			if maestro.rankget(maestro.userrank(ply)).perms[cmd] then
				maestro.runCMD(team, cmd, args, ply)
			else
				maestro.chat(ply, maestro.orange, cmd, ": Insufficient permissions!")
			end
			return ""
		end
    */
      return ""
	end

end)
timer.Create("Vgam_SendCMD", 0.5, 1, function()
  net.Start("VGAM_sendCMD")
    net.WriteTable(table.GetKeys( Admin.Commands.cmds))
  net.Broadcast()

end)
