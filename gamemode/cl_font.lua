function InitalizeFonts()
  surface.CreateFont("HUD Health", {
    font = "Acens",
    size = 35})
  surface.CreateFont("HUD Info", {
    font = "MavenPro-Black",
    size = 35})
  surface.CreateFont("HUD Speed", {
    font = "Acens",
    size = 20})
  surface.CreateFont("HUD Name", {
    font = "MavenPro-Black",
    size = 15})
  surface.CreateFont("HUD Echo", {
    font = "Acens",
    size = 40})
  surface.CreateFont("Context button", {
    font = "MavenPro-Black",
    size = 20})
  surface.CreateFont("HUD Runner Icon", {
    font = "halflife2",
    size = 85})
  surface.CreateFont("Context Speed", {
    font = "MavenPro-Black",
    size = 23})

  surface.CreateFont("Editor Text", {
    font = "MavenPro-Black",
    size = 25})

  surface.CreateFont("Tricks", {
    font = "hl2mp",
    size = 100})

    surface.CreateFont("Zones", {
      font = "halflife2",
      size = 100})
end
