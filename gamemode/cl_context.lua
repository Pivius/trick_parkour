
context = context or {}
context.UI = context.UI or {}

context.tempopen = false
context.opened = false
context.holdkey = false
convars = convars or {}


function convars.GetBool(convar)
  if !convar then return end

  local con = GetConVar(convar):GetInt()
  if con > 0 then
    return true
  else
    return false
  end
end

function context.SetMeasure()
  local int = GetConVar("pkr_measurement"):GetInt()+1
  if int >= 3 then
    RunConsoleCommand(GetConVar("pkr_measurement"):GetName(), "0")
  else
    RunConsoleCommand(GetConVar("pkr_measurement"):GetName(), int)
  end
end

function context.GetMeasure()
  if GetConVar("pkr_measurement"):GetInt() == 0 then
    return "Units per second"
  elseif GetConVar("pkr_measurement"):GetInt() == 1 then
    return "Miles per hour"
  elseif GetConVar("pkr_measurement"):GetInt() == 2 then
    return "Kilometres per hour"
  end

end

function context.outlinedBox( x, y, w, h, thickness, color)
    surface.SetDrawColor( color )

    for i=0, thickness  do

        surface.DrawRect(x, y, w, h-h+thickness) --top
        surface.DrawRect(x, y+h-thickness-1, w, h-h+thickness) --bottom
        surface.DrawRect(x, y, w-w+thickness, h) --right
        surface.DrawRect(x+w-thickness-1, y, w+thickness, h)  --left
    end

end

function context.button( x, y, w, h, color, text, textx, texty, color2, font, outline, thickness, color3)
    surface.SetDrawColor(Color(color.r, color.g, color.b, color.a))
    surface.DrawRect( x, y,  w, h)
    if font then
      surface.SetFont( font )
    else
      surface.SetFont( "Context button" )
    end
    if outline == true then
        context.OutlinedBox(x, y, w, h, thickness, color3)
    end
    surface.SetTextColor(color2)
    surface.SetTextPos( textx, texty )
    surface.DrawText( text )
end

function context.chevron( vecx, vecy, size, color)
  local chevron = {
    { x = vecx+(100*size), y = vecy+(0*size) }, -- Top right
    { x = vecx+(150*size), y = vecy+(100*size) }, -- Point
    { x = vecx+(100*size), y = vecy+(200*size) }, -- Bottom right
    { x = vecx+(0*size), y = vecy+(200*size) }, -- Bottom left
    { x = vecx+(100*size), y =vecy+(200*size) }, -- Inner Point
    { x = vecx+(0*size), y = vecy+(0*size) } -- Upper left
  }
  surface.SetDrawColor(Color(color.r, color.g, color.b, color.a))
  draw.NoTexture()
  surface.DrawPoly(chevron)
end

context.Buttons = {}
context.ButtonsRight = {}

function context.CreateButton(name, convar, y, text, right)
  local PANEL = vgui.Create( "Button", nil, "context " .. name )
  if right then
    table.insert(context.ButtonsRight, PANEL)
  else
    table.insert(context.Buttons, PANEL)
  end
  PANEL:SetSize(175,36)
  if convars.GetBool(convar) then
    PANEL:NewColor(Color(0,0,0,230))
    PANEL:NewHighlight(Color(0,0,0,250))
  else
    PANEL:NewColor(Color(0,0,0,200))
    PANEL:NewHighlight(Color(0,0,0,230))
  end
  PANEL:NewFlash(Color(255,255,255,150))
  PANEL:SetPos(0,y)
  PANEL:SetTxt(text)
  PANEL:Funct(function()
    PANEL.panel:SetColor(PANEL.flash)
    RunConsoleCommand(GetConVar(convar):GetName(), GetConVar(convar):GetInt() == 1 and "0" or "1")
    if !convars.GetBool(convar) then
      PANEL:NewColor(Color(0,0,0,240))
      PANEL:NewHighlight(Color(0,0,0,250))
      PANEL:Show( 255, 0.3, 0, 20, "x")
      PANEL.panel:ColorTo(PANEL.highlight, 0.1, 0.1)
    else
      PANEL:NewColor(Color(0,0,0,200))
      PANEL:NewHighlight(Color(0,0,0,230))
      PANEL:Show( 255, 0.3, 0, 9, "x")
      PANEL.panel:ColorTo(PANEL.highlight, 0.1, 0.1)
    end
  end)
  PANEL:Create()
  PANEL:Hide( 0.1, 0, -200, "x")
  if !convars.GetBool(convar) then
    timer.Simple(0.1, function() PANEL:Show( 255, 0.3, 0, 10, "x") end)
  else
    timer.Simple(0.1, function() PANEL:Show( 255, 0.3, 0, 20, "x") end)
  end
end

function context.UI:Reset()

  for k ,v in pairs(vgui.GetWorldPanel():GetChildren()) do
    if string.match(tostring(v), "context") then
      v:Remove()
    end
  end
end
context.UI:Reset()

function context.Draw()
  local SSPanel = vgui.Create( "vgui panel", nil, "context SSPanel" )
  table.insert(context.ButtonsRight, SSPanel)
  SSPanel:SetSize(250,150)
  SSPanel:SetCol(Color(0,0,0,200))
  SSPanel:SetPos(ScrW()+200,0)
  SSPanel:Create()
  SSPanel:Hide( 0.1, 0, ScrW()+200, "x")
  timer.Simple(0.1, function() SSPanel:Show( 255, 0.3, 0, ScrW()*0.8, "x") end)

  local TSAllTime = vgui.Create( "Text", nil, "context TSAllTime" )
  table.insert(context.ButtonsRight, TSAllTime)
  TSAllTime:SetSize(250,150)
  TSAllTime:SetCol(Color(255,255,255))
  TSAllTime:SetPos(0,0)
  TSAllTime:SetFont("Context Speed")
  TSAllTime:SetParent(SSPanel.bg)

  TSAllTime:Create()
  TSAllTime:Hide( 0.1, 0, 20, "x")
  timer.Simple(0.1, function() TSAllTime:Show( 255, 0.3, 0, 20, "x") end)


  local TSSession = vgui.Create( "Text", nil, "context TSSession" )
  table.insert(context.ButtonsRight, TSSession)
  TSSession:SetSize(250,150)
  TSSession:SetCol(Color(255,255,255))
  TSSession:SetPos(0,30)
  TSSession:SetFont("Context Speed")
  TSSession:SetParent(SSPanel.bg)

  TSSession:Create()
  TSSession:Hide( 0.1, 0, 20, "x")
  timer.Simple(0.1, function() TSSession:Show( 255, 0.3, 0, 20, "x") end)

  local AvgSession = vgui.Create( "Text", nil, "context AvgSession" )
  table.insert(context.ButtonsRight, AvgSession)
  AvgSession:SetSize(250,150)
  AvgSession:SetCol(Color(255,255,255))
  AvgSession:SetPos(0,60)
  AvgSession:SetFont("Context Speed")
  AvgSession:SetParent(SSPanel.bg)

  AvgSession:Create()
  AvgSession:Hide( 0.1, 0, 20, "x")
  timer.Simple(0.1, function() AvgSession:Show( 255, 0.3, 0, 20, "x") end)

    -- Toggle HUD
    context.CreateButton("HUDToggle", "pkr_hud", 0, "Toggle HUD")

    -- Switch between light and dark theme
    context.CreateButton("ThemeToggle", "pkr_darktheme", 36, "Toggle Theme")

    -- Toggle Indicators
    local IndicatorsToggle = vgui.Create( "Button", nil, "context IndicatorsToggle" )
    table.insert(context.Buttons, IndicatorsToggle)
    IndicatorsToggle:SetSize(175,36)
    if convars.GetBool("pkr_indicator") or convars.GetBool("pkr_outline") then
      IndicatorsToggle:NewColor(Color(0,0,0,230))
      IndicatorsToggle:NewHighlight(Color(0,0,0,250))
    else
      IndicatorsToggle:NewColor(Color(0,0,0,200))
      IndicatorsToggle:NewHighlight(Color(0,0,0,230))
    end
    IndicatorsToggle:NewFlash(Color(255,255,255,150))
    IndicatorsToggle:SetPos(0,72)
    IndicatorsToggle:SetTxt("Indicators Toggle")
    IndicatorsToggle:Funct(function()
      IndicatorsToggle.panel:SetColor(IndicatorsToggle.flash)
      RunConsoleCommand(GetConVar("pkr_indicator"):GetName(), GetConVar("pkr_indicator"):GetInt() == 1 and "0" or "1")
      RunConsoleCommand(GetConVar("pkr_outline"):GetName(), GetConVar("pkr_outline"):GetInt() == 1 and "0" or "1")
      timer.Simple(0.01, function()
        if convars.GetBool("pkr_indicator") or convars.GetBool("pkr_outline") then
          IndicatorsToggle:NewColor(Color(0,0,0,240))
          IndicatorsToggle:NewHighlight(Color(0,0,0,250))
          IndicatorsToggle:Show( 255, 0.3, 0, 20, "x")
          IndicatorsToggle.panel:ColorTo(IndicatorsToggle.highlight, 0.1, 0.1)
        else
          IndicatorsToggle:NewColor(Color(0,0,0,200))
          IndicatorsToggle:NewHighlight(Color(0,0,0,230))
          IndicatorsToggle:Show( 255, 0.3, 0, 9, "x")
          IndicatorsToggle.panel:ColorTo(IndicatorsToggle.highlight, 0.1, 0.1)
        end
      end)
    end)
    IndicatorsToggle:Create()
    IndicatorsToggle:Hide( 0.1, 0, -200, "x")
    if !convars.GetBool("pkr_indicator") or !convars.GetBool("pkr_outline") then
      timer.Simple(0.1, function() IndicatorsToggle:Show( 255, 0.3, 0, 10, "x") end)
    else
      timer.Simple(0.1, function() IndicatorsToggle:Show( 255, 0.3, 0, 20, "x") end)
    end

    -- Toggle Key Echoes
    context.CreateButton("KeyEchoes", "pkr_keys", 108, "Key Echoes")

    -- Toggle Trails
    context.CreateButton("Trail", "pkr_hidetrail", 144, "Hide trails")

    -- Switch between Up/s, km/h and mp/h
    local MeasureToggle = vgui.Create( "Button", nil, "context MeasureToggle" )
    table.insert(context.Buttons, MeasureToggle)
    MeasureToggle:SetSize(175,36)
    MeasureToggle:NewColor(Color(0,0,0,200))
    MeasureToggle:NewHighlight(Color(0,0,0,230))
    MeasureToggle:NewFlash(Color(255,255,255,150))
    MeasureToggle:SetPos(0,180)
    MeasureToggle.msrt = context.GetMeasure()
    MeasureToggle:SetTxt(MeasureToggle.msrt)
    MeasureToggle:Funct(function()
      MeasureToggle.panel:SetColor(MeasureToggle.flash)
      MeasureToggle.panel:ColorTo(MeasureToggle.color, 0.1, 0.1)
      context.SetMeasure()
      timer.Simple(0.1, function()
        MeasureToggle.msrt = context.GetMeasure()
        MeasureToggle:SetTxt(MeasureToggle.msrt)
        MeasureToggle:UpdatePaint() end)
    end)
    MeasureToggle:Create()
    MeasureToggle:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() MeasureToggle:Show( 255, 0.3, 0, 10, "x") end)
    hook.Add("Think", "Update speeds", UpdateSpeedMenu)
end

function UpdateSpeedMenu()
  for k ,v in pairs(context.ButtonsRight) do
    if string.match(tostring(v), "TSAllTime") then
      v:SetTxt("Alltime Topspeed: " .. LocalPlayer():getTopAllTime())
    end
    if string.match(tostring(v), "TSSession") then
      v:SetTxt("Session Topspeed: " .. LocalPlayer():getTopSession())
    end
    if string.match(tostring(v), "AvgSession") then

      v:SetTxt("Session Average: " .. LocalPlayer():GetAvg())
    end
  end
end


function context.open()
  if not context.opened  then
    context.opened = true
    context.Draw()
   end
end

function context.close()
  if context.opened then
    context.opened = false
    for k ,v in pairs(context.Buttons) do
      if IsValid(v) then
        v:Hide( 0.3, 0, -200, "x")
        timer.Create("ERemove " .. tostring(v), 0.5, 1, function() v:Remove() table.RemoveByValue(context.Buttons, v) end)
      end
    end
    for k ,v in pairs(context.ButtonsRight) do
      if IsValid(v) then
        v:Hide( 0.3, 0, ScrW()+200, "x")
        timer.Create("ERemoveR " .. tostring(v), 0.5, 1, function() v:Remove() table.RemoveByValue(context.ButtonsRight, v) end)
      end
    end
  end
end


function GM:OnContextMenuOpen()

	gui.EnableScreenClicker(true)
  if context.tempopen then
    context.tempopen = false
  end
  context.holdkey = true
  if !LocalPlayer():GetEditing() then
    if #context.Buttons == 0 and #context.ButtonsRight == 0 then
	    context.open()
    else
      timer.Create("Retry menu", 0.2, 1, function() if context.holdkey then context.open() end end)
    end
  end
end

function GM:OnContextMenuClose()
  if !context.tempopen then
    context.holdkey = false
    if !LocalPlayer():GetEditing() then
      context.close()
    end
    gui.EnableScreenClicker(false)
  end
end
