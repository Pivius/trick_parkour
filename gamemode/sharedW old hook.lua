include( "player_class/player_sandbox.lua" )
include( "drive/drive_sandbox.lua" )
include( "cl_hud.lua" )



DEFINE_BASECLASS( "gamemode_base" )

GM.Name 	= "Parkour"
GM.Author 	= "Pivius"
GM.Email 	= "n/a"
GM.Website 	= "n/a"







function GM:PlayerNoClip( ply )
end

hook.Add( "ShouldCollide", "ShouldCollideTestHook", function(ent1, ent2)
	if ( ent1:IsPlayer() and ent2:IsPlayer() ) then
		 return false

	end
end)
hook.Add( "Think", "ShouldCollideTestHook2", function(ent1, ent2)



	for _,pl in pairs (player.GetAll()) do
		pl:SetCustomCollisionCheck( true )
		pl:SetAvoidPlayers( false )
	end
end)



--Player Regeneration
function GM:Think()

        if CurTime() > (Lastthink or 0) + 0.6 then
                Lastthink = CurTime()
                for _,pl in pairs (player.GetAll()) do
                        local hp = pl:Health()
                        if hp/100 != math.floor (hp/100) then
                                if (pl.LastHit or 0) + 5 < CurTime() then
                                        pl:SetHealth (hp+1)
                                end
                        end

                end
        end
end
hook.Add("Think", "Regen", Think)
--[[
hook.Add("KeyPress", "boost", function(ply, key)
        if (key == IN_SPEED) then
                ply:SetVelocity(ply:GetAimVector() * 1500)
        end
end)]]


hook.Add( 'SetupMove', 'AutoHop', function( ply, move )

        local bhop = CreateClientConVar( "bhop", "0", true, false )

                if( bhop:GetInt() == 0 ) then
                end
                if( bhop:GetInt() == 1 ) then

                        if( !ply:IsOnGround() ) then
                                move:SetButtons( bit.band( move:GetButtons(), bit.bnot( IN_JUMP ) )  )

                        end
--[[
                        if( ply:IsOnGround() && bit.bnot( IN_JUMP ) ) then

                        	ply:SetVelocity(ply:GetVelocity()* 0.05)
                        end]]
                end

end )

--[[
function Test(ply)
	if ply:KeyDown( IN_SPEED ) then
		for k, v in pairs(ents.FindInSphere(ply:GetPos(), 100)) do
			if v:IsPlayer() and !(LocalPlayer() == v)	 then
				print("We got a Player")
			end
		end
	end
end
hook.Add("SetupMove", "test", Test)





]]


function abc( ply )

	local tracedata={ }

	tracedata.start = ply:GetShootPos()
	tracedata.endpos = ply:GetShootPos() - Vector( 0, 0, 40 )
	tracedata.filter = ply
	tr=util.TraceLine( tracedata )
	if ( tr.Fraction == 1 ) then

		ply:SetUnDuckSpeed( 0 )
	elseif ( tr.Fraction < 1 ) then

		ply:SetUnDuckSpeed( .5 )
	end




end
hook.Add("Move", "hopduckthing", abc)

local AIR_SPEEDCAP	= 100
local AIR_ACCEL		= 100

hook.Add("SetupMove", "air strafing", function(ply, cmd, cud)
	if (ply:Alive()) then
		local aim 			= cmd:GetMoveAngles():Forward()
		local angle = ( cmd:GetAngles() - cmd:GetVelocity():Angle()  )
		angle:Normalize()

		local velocity		= cmd:GetVelocity()
		local forward		= Vector(aim.x, aim.y, 0):GetNormal()
		local right			= Vector(aim.y, -aim.x, 0):GetNormal()
		local wishDir 		= Vector(0,0,0)
		local wishSpeed		= 0
		local addSpeed		= 0
		local currentSpeed	= 0
		local cooldown 		= 100


		wishDir 			= wishDir + forward * cmd:GetForwardSpeed()
		wishDir 			= wishDir + right * cmd:GetSideSpeed()

		wishSpeed			= wishDir:Length()
		wishDir				= wishDir:GetNormal()

		if (wishSpeed > AIR_SPEEDCAP) then
			wishSpeed = AIR_SPEEDCAP
		end
		--ply:SetJumpPower(200)
		currentSpeed = velocity:Dot(wishDir)

		addSpeed = wishSpeed - currentSpeed




		if cooldown == 100 then

		if ply:KeyDown( IN_SPEED ) and not ply:IsOnGround() then
			if not Holding then
				if ( ( ply:KeyDown( IN_MOVELEFT ) and not ply:KeyDown( IN_MOVERIGHT ) or ply:KeyDown( IN_MOVERIGHT ) and  not ply:KeyDown( IN_MOVELEFT ) ) ) then

					if (addSpeed > 200) then
    	            	local accelSpeed = AIR_ACCEL * wishSpeed
    	            	local addSpeed = wishSpeed - currentSpeed 	-- * 1.8
    	              	if (accelSpeed > addSpeed) then
    	               		accelSpeed = addSpeed
    	                end
    	                cmd:SetVelocity(velocity + (accelSpeed * wishDir))
    	            end
    	            Holding = true
    	        else
					Holding = false
				end

			else
				if ( ( ply:KeyDown( IN_MOVELEFT ) and not ply:KeyDown( IN_MOVERIGHT ) )  or ( ply:KeyDown( IN_MOVERIGHT ) and  not ply:KeyDown( IN_MOVELEFT ) ) ) then
					if (addSpeed > 200) then
    	            	local accelSpeed = AIR_ACCEL * wishSpeed
   	 	            	local addSpeed = wishSpeed - currentSpeed --* 1.8
    	              	if (accelSpeed > addSpeed) then
    	                   	accelSpeed = addSpeed
    	               	end
    	               	cmd:SetVelocity(velocity + (accelSpeed * wishDir))
    	        	end
				end
			end
    	elseif not ply:KeyDown( IN_SPEED ) then
			Holding = false
		end
		elseif cooldown == 0 then
			Holding = false
		end
    end
end)


-- Accelerated movement
hook.Add("SetupMove", "Accelerate", function(ply, cmd, cud)

	if ply:Alive() and ply:IsOnGround() then
			local aim = cmd:GetMoveAngles()
			local forward, right = aim:Forward(), aim:Right()
			local fmove = cmd:GetForwardSpeed()
			local smove = cmd:GetSideSpeed()
			local aspeed = 400
			local maxspeed	= 9999
			local acceleration = 1
			local plyspeed = 400
			forward.z, right.z = 0,0
			forward:Normalize()
			right:Normalize()

			local wishvel = forward * fmove + right * smove
			wishvel.z = 0

			local wishspeed = wishvel:Length()

			if(wishspeed > maxspeed) then
				wishvel = wishvel * (maxspeed/wishspeed)
				wishspeed = maxspeed
			end



			local wishspd = wishspeed
			wishspd = math.Clamp(wishspd, 0, plyspeed)

			local wishdir = wishvel:GetNormal()
			local current = cmd:GetVelocity():Dot(wishdir)

			local addspeed = wishspd - current
			if(addspeed <= 0) then return end

			local accelspeed = (acceleration) * wishspeed * FrameTime() *ply:GetFriction()

			if(accelspeed > addspeed) then
				accelspeed = addspeed
			end

			local vel = cmd:GetVelocity()
			vel = vel + (wishdir * accelspeed)
			cmd:SetVelocity(vel)

			return false
		end

end)

function distanceFrom(A,B)
   return math.sqrt((B.x - A.x) ^ 2 + (B.y - A.y) ^ 2 + (B.z - A.z) ^ 2)
end

function mul_euclidian(p1, p2)
	return math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z))
end
--Angle between two vectors
function ABTV(A, B)
    local fCrossX = A.y * B.z - A.z * B.y
		local fCrossY = A.z * B.x - A.x * B.z
		local fCrossZ = A.x * B.y - A.y * B.x
    local fCross = math.sqrt(fCrossX * fCrossX + fCrossY * fCrossY + fCrossZ * fCrossZ)
		local fDot = A.x * B.x + A.y * B.y + A.z * B.z
    return math.atan2(fCross, fDot);
end

function DrawRope(origin, hit)
	local Laser = Material( "cable/cable" )
	local red = Color(255, 0, 0, 255)
	hook.Add("PreDrawTranslucentRenderables", "DrawLines", function()
		render.SetMaterial(Laser)

				render.DrawBeam(origin - Vector(0,0, 5), hit, 3, 0, 0, red)
	end)
end


hook.Add("SetupMove", "Hooks", function(ply, cmd, cud)
	--Trace
	local trace = ply:GetEyeTrace()
	local origin = trace.StartPos


	if !ply:KeyDown(IN_ATTACK) then  hit = trace.HitPos return end
	local distY, distX, distZ = (origin.Y - hit.Y), (origin.X - hit.X), (origin.Z - hit.Z)
	local defswingvel = 2 * 400
	local maxswingvel = 4 * 400
	local velocity = Vector(0, 0, 0)
	local Speed = ply:GetVelocity()
	local speedang = ABTV(Speed:GetNormalized(), Speed)
	local speedmagn = mul_euclidian(Vector(0, 0, 0), Speed)
	local ropeang = ABTV(origin, hit)
	local relspeedang = speedang - ropeang
	local relSpeed = Vector(
	speedmagn*math.cos(relspeedang-math.pi/2)*math.sin(relspeedang-math.pi/2),
	speedmagn*math.sin(relspeedang-math.pi/2)*math.sin(relspeedang-math.pi/2),
	speedmagn*math.cos(relspeedang-math.pi/2))

	DrawRope(origin, hit)
	--local acceleration = Vector(0.4 * (math.sin(angle) * math.cos(angle)), 0.4 * (math.sin(angle) * math.sin(angle)), (0.4 * math.cos(angle)))
	if ( distX < 0 ) then
			velocity.x = math.max(defswingvel, velocity.x);
		else
			velocity.x = math.min(-defswingvel, velocity.x);
	end



	relSpeedAngle = ABTV(relSpeed, relSpeed)
	speedMagnitude = mul_euclidian(Vector(0, 0, 0), relSpeed)
	speedAngle = relSpeedAngle + ropeang
	velocity = Vector(
	speedmagn*math.cos(speedang+math.pi/2)*math.sin(speedang+math.pi/2),
	speedmagn*math.sin(speedang+math.pi/2)*math.sin(speedang+math.pi/2),
	speedmagn*math.cos(speedang+math.pi/2))
	print(relSpeed)
	ply:SetVelocity(relSpeed)
end)


--[[
Hook = Hook or {}

function Hook:Init()
	 min_mass = 10
	 counter_mass = 20
	 counter_mass_low = 20
	 counter_mass_high = 80
	 hooked1 = false
	 hook_ent1 = nil
	 hook_ent1_offset = Vector( 0, 0, 0 )
	 hook_pos1 = Vector( 0, 0, 0 )
	 ropelength1 = 0
	 sound1 = util.PrecacheSound( "weapons/crossbow/fire1.wav" )
	 sound2 = util.PrecacheSound( "ambient/machines/slicer3.wav" )

end
	Hook:Init()
function Hook:Reset()
	Hook:RopeOff()
end

function Hook:RopeOn(ply)
if hooked1 then return end


	--local ply = Entity( ent )									-- Gets owner reference
	local ply_aim = ply:GetAimVector()
	local trace = {}														-- Creates a trace table
	trace.start = ply:EyePos()										-- Sets trace start position
	trace.endpos = ply:EyePos() + ( ply_aim * 2500 )				-- Sets trace end position, range = constant number
	trace.filter = ply													-- Sets the trace to ignore the user of the SWEP
	local tr = util.TraceLine( trace )										-- Initiates the actual trace with the parameters stored in the trace table
	if tr.HitSky then return end
	--if not tr.HitWorld then return end										-- Past this point, the trace should have hit something solid.
	if not SERVER then return end									-- The client's doesn't have to think about this
	if tr.HitWorld then
		hook_ent1 = nil
		enthook1 = false
		ply:SetAnimation( PLAYER_ATTACK1 )
		local ply_pos = ply:GetPos()
		hooked1 = true														-- It's hooked all right
		hook_pos1 = tr.HitPos												-- Set hit position
		local tension = hook_pos1 - ply_pos
		ropelength1 = math.sqrt(tension.x*tension.x + tension.y*tension.y + tension.z*tension.z)
		ropelength1_original = ropelength1

		piton1 = ents.Create( "prop_physics" )
		piton1:SetModel( "models/weapons/w_missile_closed.mdl" ) --"models/props_junk/GlassBottle01a.mdl" "models/kunai.mdl"
		piton1:SetPos( hook_pos1 + ( ply_aim * -2.0 ) )
		piton1:SetAngles( ( -1 * ply_aim ):Angle() )
		piton1:Spawn()
		piton1:EmitSound( "weapons/crossbow/hit1.wav" )
		piton1:SetKeyValue( "targetname", "vip_piton1" .. ply:UniqueID())
		piton1:SetMoveType(MOVETYPE_NONE)
		local physent = piton1:GetPhysicsObject()
		physent:EnableMotion(false)


		ropekeyframe1 = ents.Create( "keyframe_rope" )
		ropekeyframe1:SetKeyValue( "MoveSpeed", "64" )
		ropekeyframe1:SetKeyValue( "Slack", "-2500" )
		ropekeyframe1:SetKeyValue( "Subdiv", "2" )
		ropekeyframe1:SetKeyValue( "Width", "0.5" )
		ropekeyframe1:SetKeyValue( "TextureScale", "1" )
		ropekeyframe1:SetKeyValue( "Collide", "1" )
		ropekeyframe1:SetKeyValue( "RopeMaterial", "cable/cable.vmt" )
		ropekeyframe1:SetKeyValue( "targetname", "ropekeyframe1" .. ply:UniqueID())
		ropekeyframe1:SetPos( hook_pos1 + ( ply_aim * -8.0 ) )
		ropekeyframe1:Spawn()
		ropekeyframe1:Activate()

		rope1 = ents.Create( "move_rope" )
		rope1:SetKeyValue( "MoveSpeed", "64" )
		rope1:SetKeyValue( "Slack", "-2500" )
		rope1:SetKeyValue( "Subdiv", "2" )
		rope1:SetKeyValue( "Width", "0.5" )
		rope1:SetKeyValue( "TextureScale", "1" )
		rope1:SetKeyValue( "Collide", "1" )
		rope1:SetKeyValue( "PositionInterpolator", "2" )
		rope1:SetKeyValue( "NextKey", "ropekeyframe1" .. ply:UniqueID())
		rope1:SetKeyValue( "RopeMaterial", "cable/cable.vmt" )
		rope1:SetKeyValue( "targetname", "rope1" .. ply:UniqueID())
		rope1:SetPos( ply:GetPos() )
		rope1:Spawn()
		rope1:Activate()
		rope1:SetParent( ply )
	elseif tr.HitNonWorld and tr.Entity:IsValid() and ( not tr.Entity:IsPlayer() ) and ( not tr.Entity:IsWeapon() ) then -- Must have hit a non-world entity. Not a player either. NPCs are allright, but remember PHW24: Riding scanners is dangerous.
		local ply_pos = ply:GetPos()
		hooked1 = true														-- It's hooked all right
		enthook1 = true
		hook_ent1 = tr.Entity
		hook_ent_phys1 = hook_ent1:GetPhysicsObject()
		if hook_ent_phys1:GetMass() < min_mass then Hook:RopeOff() return end -- Physics objects lighter than 10 tend to spazz
		hook_pos1 = tr.HitPos												-- Set hit position
		local tension = hook_pos1 - ply_pos
		ropelength1 = math.sqrt(tension.x*tension.x + tension.y*tension.y + tension.z*tension.z)
		ropelength1_original = ropelength1
		local hook_ent1_offset = hook_pos1 + ( ply_aim * -8.0 ) - hook_ent1:GetPos()
		hook_ent1_offset_forward = (hook_ent1:GetForward()):Dot(hook_ent1_offset)
		hook_ent1_offset_right = (hook_ent1:GetRight()):Dot(hook_ent1_offset)
		hook_ent1_offset_up = (hook_ent1:GetUp()):Dot(hook_ent1_offset)

		--Msg("DEBUG3\n" .. tostring(self.hook_ent1_offset_forward) .. "\n" .. tostring(self.hook_ent1_offset_right).."\n"..tostring(self.hook_ent1_offset_up) .. "\n")
		--Msg("DEBUG4\n" .. tostring(hook_ent1_offset) .. "\n")

		ply:SetAnimation( PLAYER_ATTACK1 )

		piton1 = ents.Create( "prop_physics" )
		piton1:SetModel( "models/weapons/w_missile_closed.mdl" ) --"models/props_junk/GlassBottle01a.mdl" "models/kunai.mdl"
		piton1:SetPos( hook_pos1 + ( ply_aim * -2.0 ) )
		piton1:SetAngles( ( -1.0 * ply_aim ):Angle() )
		piton1:Spawn()
		piton1:SetParent(hook_ent1)
		piton1:EmitSound( "weapons/crossbow/hit1.wav" )
		piton1:SetKeyValue( "targetname", "vip_piton1" .. ply:UniqueID())
		piton1:SetMoveType(MOVETYPE_NONE)
		local physent = piton1:GetPhysicsObject()
		physent:EnableMotion(false)
		physent:EnableCollisions(false)

		ropekeyframe1 = ents.Create( "keyframe_rope" )
		ropekeyframe1:SetKeyValue( "MoveSpeed", "64" )
		ropekeyframe1:SetKeyValue( "Slack", "-2500" )
		ropekeyframe1:SetKeyValue( "Subdiv", "2" )
		ropekeyframe1:SetKeyValue( "Width", "0.5" )
		ropekeyframe1:SetKeyValue( "TextureScale", "1" )
		ropekeyframe1:SetKeyValue( "Collide", "1" )
		ropekeyframe1:SetKeyValue( "RopeMaterial", "cable/cable.vmt" )
		ropekeyframe1:SetKeyValue( "targetname", "ropekeyframe1" .. ply:UniqueID())
		ropekeyframe1:SetPos( hook_pos1 + ( ply_aim * -8.0 ) )
		ropekeyframe1:Spawn()
		ropekeyframe1:SetParent(hook_ent1)
		ropekeyframe1:Activate()

		rope1 = ents.Create( "move_rope" )
		rope1:SetKeyValue( "MoveSpeed", "64" )
		rope1:SetKeyValue( "Slack", "-2500" )
		rope1:SetKeyValue( "Subdiv", "2" )
		rope1:SetKeyValue( "Width", "0.5" )
		rope1:SetKeyValue( "TextureScale", "1" )
		rope1:SetKeyValue( "Collide", "1" )
		rope1:SetKeyValue( "PositionInterpolator", "2" )
		rope1:SetKeyValue( "NextKey", "ropekeyframe1" .. ply:UniqueID())
		rope1:SetKeyValue( "RopeMaterial", "cable/cable.vmt" )
		rope1:SetKeyValue( "targetname", "rope1" .. ply:UniqueID())
		rope1:SetPos( ply:GetPos() ) --ply:EyePos() + Vector( 0, 0, -5 )
		rope1:Spawn()
		rope1:Activate()
		rope1:SetParent( ply )
	else
		return
	end
end

function Hook:RopeOff()
	if hooked1 then
		hooked1 = false
		if ropekeyframe1:IsValid() then ropekeyframe1:Remove() end
		if rope1:IsValid() then rope1:Remove() end
		if ( piton1:IsValid() ) then
			piton1:EmitSound( "ambient/machines/slicer3.wav" )
			piton1:Remove()
		end
		enthook1 = false
		hook_ent1 = nil
		hook_ent_phys1 = nil
		hook_ent1_offset = Vector( 0, 0, 0 )
		hook_pos1 = Vector( 0, 0, 0 )
		ropelength1 = 0
		hook_ent1_offset_forward = 0
		hook_ent1_offset_right = 0
		hook_ent1_offset_up = 0
	end
end


function Hook:RopeThink(ply)

--	if ply and ply:IsValid() then

		if hooked1 then -- Level 3: Hooked, general

			--local ply = Entity( ent )
			local ply_pos = ply:GetPos()
			local ropeforce1 = Vector(0,0,0)


			if hooked1 then

				if not ( ropekeyframe1:IsValid() and rope1:IsValid() and piton1:IsValid() ) then

					Hook:RopeOff()
				end
			end


			if hooked1 then -- Level 4, hooked #1
				-- New swep.hook_pos1 has to be defined is the rope is attached to a possibly-moving entity

				local tension = hook_pos1 - ply_pos
				local stretch = tension:Length()
				/*
				if ( ropelength1 < 2 ) then ropelength1 = 0 else ropelength1 = ropelength1 - 2 end

				ropekeyframe1:SetKeyValue( "Slack", tostring( ropelength1 - ropelength1_original ) )
				rope1:SetKeyValue( "Slack", tostring( ropelength1 - ropelength1_original ) )*/

				if ( ropelength1 < stretch ) then
					local tension_norm = tension:GetNormal()
					local ply_vel = Vector(0,0,0)

						ply_vel = ply:GetVelocity()

					local tension_vel_mag = tension_norm:DotProduct(ply_vel)

					if ( tension_vel_mag < 0 ) then
						ropeforce1 = tension_norm * ( ( tension_vel_mag * -1.25 ) + ( stretch - ropelength1 ) ) -- Stretched and moving the wrong way REMOVE FOR FUN TIME
print(tension_norm * ( ( tension_vel_mag * -1.25 ) + ( stretch - ropelength1 ) ))
					else
						ropeforce1 = tension_norm * ( stretch - ropelength1 ) -- Stretched and moving the right way

					end



				end
				/*
				local sin = math.sin(CurTime() * 5  ) * 150
				local cos = math.cos(CurTime() * 5) * 150
				ropeforce1 = (ply:GetForward() * math.cos(150)) + (ply:GetUp() * math.sin(150)) -- Stretched and moving the right way
				ply:SetVelocity( ( ply:GetForward() * sin ) + ( ply:GetUp() * cos ) )
*/

				/*
				if ( ropelength1 < stretch ) then
					local tension_norm = tension:GetNormal()
					local ply_vel = Vector(0,0,0)
					if enthook1 then
						ply_vel = ply:GetVelocity() - hook_ent_phys1:GetVelocity() -- This compensates when the player is hanging on a moving object
					else
						ply_vel = ply:GetVelocity()
					end
					local tension_vel_mag = tension_norm:DotProduct(ply_vel)

					if ( tension_vel_mag < 0 ) then
						ropeforce1 = tension_norm * ( ( tension_vel_mag * -1.25 ) + ( stretch - ropelength1 ) ) -- Stretched and moving the wrong way REMOVE FOR FUN TIME

					else
						ropeforce1 = tension_norm * ( stretch - ropelength1 ) -- Stretched and moving the right way

					end

					-- And for every action, there is an equal and opposite reaction...
					if enthook1 then

						local reactionforce1 = -1.0 * counter_mass * ropeforce1
						hook_ent_phys1:ApplyForceOffset( reactionforce1, hook_pos1 )
					end
				end*/
			end

			ply:SetVelocity( ropeforce1 )
		end
	--end
end



hook.Add("SetupMove", "Hooks", function(ply, cmd, cud)



Hook:RopeThink(ply)



end)

hook.Add( "KeyPress", "keypress", function( ply, key )

	if ( key == IN_ATTACK ) then

		Hook:RopeOn(ply)

	end
end)

hook.Add( "KeyRelease", "keypress", function( ply, key )

	if ( key == IN_ATTACK ) then

		Hook:Reset(ply)

	end
end)
]]
