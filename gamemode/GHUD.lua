-- http://facepunch.com/showthread.php?t=1209676
GHUD = {}

GHUD.SIZE = 0.05/2
GHUD.DISTANCE = 25/2
GHUD.SUPER_ANGLE = 20

GHUD.Color = Color( 255, 255, 255, 255 )

function GM:HUDPaint()

    cam.Start3D( EyePos(), EyeAngles() )

        GHUD.SexyHud()

    cam.End3D()
end

function GM:HUDShouldDraw(name)
    local draw = true

    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo") then
    draw = false;
    end
        
return draw;
end

function GHUD.Health(ply, pos, ang)
    local ply = LocalPlayer()
    local HP = ply:Health()

    cam.Start3D2D( pos, ang, GHUD.SIZE/2 )

        draw.DrawText( HP, "CloseCaption_Normal", ScrW() * -0.004, ScrH() * 0.384, GHUD.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
        draw.RoundedBox(2, 636, ScrH() - 568, 211, 60, Color( 255, 255, 255, 25));
        draw.RoundedBox(2, 632, ScrH() - 550, 39, 20, Color( 30, 30, 30, 255));

        draw.RoundedBox(2, 632, ScrH() - 528, 207, 5, Color( 50, 50, 50, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( HP, 0, 200)*2.065, 5, Color( 220, 108, 108, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( HP, 0, 200)*2.065, 1, Color( 255, 255, 255, 40));

    cam.End3D2D()
end

--[[
function GHUD.Health3D(ply, pos, ang)
        local pl = LocalPlayer();
        local HP = pl:Health();



        cam.Start3D2D( pos, ang, GHUD.SUPER_SIZE/2 );

        draw.DrawText( HP, "CloseCaption_Normal", ScrW() * -0.004, ScrH() * 0.384, GHUD.Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);

        draw.RoundedBox(2, 636, ScrH() - 568, 211, 60, Color( 255, 255, 255, 25));
        draw.RoundedBox(2, 632, ScrH() - 550, 39, 20, Color( 30, 30, 30, 255));


        draw.RoundedBox(2, 632, ScrH() - 528, 207, 5, Color( 50, 50, 50, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( HP, 0, 200)*2.065, 5, Color( 220, 108, 108, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( HP, 0, 200)*2.065, 1, Color( 255, 255, 255, 40));
        cam.End3D2D();

        return;
end

function GHUD.Speed3D(ply, pos, ang)
        local pl = LocalPlayer();
        local Speed = pl:GetVelocity():Length();
        local fast = math.Round(pl:GetVelocity():Length() / 10 );

        cam.Start3D2D( pos, ang, GHUD.SUPER_SIZE/2 );
        draw.RoundedBox(2, 628, ScrH() - 568, 215, 60, Color( 255, 255, 255, 25));
        draw.RoundedBox(2, 800, ScrH() - 550, 39, 20, Color( 30, 30, 30, 255));
        draw.DrawText( fast, "CloseCaption_Normal", ScrW() * 0.5007, ScrH() * 0.384, GHUD.SUPER_COLOR, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);

        draw.RoundedBox(2, 632, ScrH() - 528, 207, 5, Color( 50, 50, 50, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( Speed, 0, 5000)/22.5, 5, Color( 105, 175, 175, 255));
        draw.RoundedBox(2, 632, ScrH() - 528, math.Clamp( Speed, 0, 5000)/22.5-2, 1, Color( 255, 255, 255, 40));
        cam.End3D2D();


        return;
end
]]--
function GHUD.SexyHud()
    local ply = LocalPlayer()
    local plyAng = EyeAngles():Up()*1.001
    local plyPos = EyePos()
    local pos = plyPos
    local centerAng = Angle( plyAng.p, plyAng.y, 0 )

    pos = pos + ( centerAng:Forward() * GHUD.DISTANCE )

    centerAng:RotateAroundAxis( centerAng:Right(), 90 )
    centerAng:RotateAroundAxis( centerAng:Up(), -90 )

    local leftAng = Angle( centerAng.p, centerAng.y, centerAng.r )
    local rightAng = Angle( centerAng.p, centerAng.y, centerAng.r )

    leftAng:RotateAroundAxis( leftAng:Right(), GHUD.SUPER_ANGLE*-1 )
    rightAng:RotateAroundAxis( rightAng:Right(), GHUD.SUPER_ANGLE )

    local centerAngUp = Angle( plyAng.p, plyAng.y, 0 )

    centerAngUp:RotateAroundAxis( centerAngUp:Right(), 98 )
    centerAngUp:RotateAroundAxis( centerAngUp:Up(), -90 )

    local leftAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r )
    local rightAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r )

    leftAngUp:RotateAroundAxis( leftAngUp:Right(), GHUD.SUPER_ANGLE*-1)
    rightAngUp:RotateAroundAxis( rightAngUp:Right(), GHUD.SUPER_ANGLE )

        cam.Start3D( plyPos, plyAng )
            cam.IgnoreZ(true)

            GHUD.Health(pl, pos, leftAng)

            cam.IgnoreZ(false)
        cam.End()
end

hook.Add( "RenderScreenspaceEffects", "GHUDM8", GHUD.SexyHud )