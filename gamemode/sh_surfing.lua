local meta = FindMetaTable( "Player" )
function meta:IsSurfing()
  if !IsFirstTimePredicted() then return end
  local pos = self:GetPos()
  local mins = self:OBBMins()
  local maxs = self:OBBMaxs()
  local endPos = pos * 1
  endPos.z = endPos.z -1

  local tr = util.TraceHull{
    start = pos,
    endpos = endPos,
    mins = mins,
    maxs = maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  if(tr.Fraction ~= 1) then
  -- Gets the normal vector of the surface under the player
    local Plane = tr.HitNormal
  -- Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
    if(0.2 <= Plane.z and Plane.z < 1) then
			return true
		else
		  return false
    end
	else
		return false
  end
end

function Surf(ply, mv)
  local	wishdir   = Vector()
  local aim       = mv:GetMoveAngles():Forward()
  local forward   = Vector(aim.x,  aim.y, 0):GetNormal()
  local right	    = Vector(aim.y, -aim.x, 0):GetNormal()
  local vel       = mv:GetVelocity()
  local Accelerate = 0
  local speedCap = 0
  wishdir         = wishdir + forward * mv:GetForwardSpeed()
  wishdir         = wishdir + right * mv:GetSideSpeed()
	wishdir.z       = 0
  local wishspeed	= wishdir:Length()
  wishdir         = wishdir:GetNormal()
  if ply:IsSurfing() then
    Accelerate = 500
    speedCap = 1
  end

  if (wishspeed > speedCap) then
    wishspeed = speedCap
  end
  local currentspeed = vel:Dot(wishdir)
  local addspeed     = wishspeed - currentspeed
  if (addspeed <= 0) then return end
  local accelspeed    = Accelerate*wishspeed
  if (accelspeed > addspeed) then
    accelspeed = addspeed
  end
  mv:SetVelocity(mv:GetVelocity() + (accelspeed*wishdir))
end
hook.Add("SetupMove", "Allow surfing with 10aa", Surf)
