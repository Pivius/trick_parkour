local meta = FindMetaTable( "Player" )
Teleport = {}
  if SERVER then
    util.AddNetworkString("SetTele")
    util.AddNetworkString("getLocNet")
  end

hook.Add("OnEntityCreated", "Initalize tele", function(ent)
  if ent:IsPlayer() then
    GetSet("curLoc", "Player")
    GetSet("curAng", "Player")
    GetSet("curSpeed", "Player")
    ent:SetcurLoc(nil)
    ent:SetcurAng(nil)
    ent:SetcurSpeed(nil)
  end
end)

net.Receive("SetTele", function()
  local ply = net.ReadEntity()
  local pos = net.ReadVector()
  local ang = net.ReadAngle()
  local vel = net.ReadVector()

  ply:SetcurLoc(pos)
  ply:SetcurAng(ang)
  ply:SetcurSpeed(vel)
end)

function meta:Teleport(vec, ang, speed)
  if !vec then
    if !self:GetcurLoc() then
      if CLIENT then
        chat.AddText( Color(255,75,0, 255), "You must set a location with ", Color(255,255,255, 255), "!saveloc")
      else
        chat.Text(self, Color(255,75,0, 255), "You must set a location with ", Color(255,255,255, 255), "!saveloc")
      end
    end
    if self:GetcurLoc() and self:GetcurAng() and self:GetcurSpeed() then
      if CLIENT then
        net.Start("getLocNet")
          net.WriteEntity(self)
        net.SendToServer()
      else
        net.Receive("getLocNet",function(len, ply)
          ply:SetPos( ply:GetcurLoc() )
          ply:SetEyeAngles( ply:GetcurAng() )
          ply:SetLocalVelocity( ply:GetcurSpeed() )
        end)
        self:SetPos( self:GetcurLoc() )
        self:SetEyeAngles( self:GetcurAng() )
        self:SetLocalVelocity( self:GetcurSpeed() )
        self:ResetOrder()
      end

      self:SetHealth(100)
    end
    return
  end
  if !ang or !speed then return end

  self:SetPos(vec)
  self:SetEyeAngles( ang )
  self:SetLocalVelocity( speed )
  self:SetHealth(100)
  if SERVER then
    self:ResetOrder()
  end
end

function autoCompTele( cmd, args )
	local tbl = {}
  local commands = {"gotoloc", "saveloc"}
	for k, v in pairs( commands ) do
    if string.find( string.lower( " "..v ), args ) then
  	  table.insert( tbl, v )
    end
	end
	return tbl
end

concommand.Add("gotoloc", function(ply)
  ply:Teleport()
end, autoCompTele, nil, FCVAR_USERINFO)

concommand.Add("saveloc", function(ply)
  ply:SetcurLoc(ply:GetPos())
  ply:SetcurAng(ply:EyeAngles())
  ply:SetcurSpeed(ply:GetVelocity())
  net.Start("SetTele")
    net.WriteEntity(ply)
    net.WriteVector(ply:GetcurLoc())
    net.WriteAngle(ply:GetcurAng())
    net.WriteVector(ply:GetcurSpeed())
  net.SendToServer()
  local plypos = (math.Round(ply:GetPos().x, 3) .. ", " .. math.Round(ply:GetPos().y, 3) .. ", " .. math.Round(ply:GetPos().z, 3))
  chat.AddText(Color(255,255,255, 255), "Location saved at ( ", Color(0,75,255,255), plypos , Color(255,255,255, 255), " )" )
end, autoCompTele, nil, FCVAR_USERINFO)
