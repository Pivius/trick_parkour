
lobby = lobby or {}

lobby.minigames = { Miscellaneous  = 1}

function lobby.minigames:LobbyName( ID )
	return LobbyName[ ID ] or "Unknown"
end

function lobby.minigames:IsValidLobby( Lobby )
	return not not StyleNames[ Lobby ]
end

function lobby.minigames:GetLobbyID( szLobby )
	for s,id in pairs( _C.Lobby ) do
		if sl( s ) == sl( szLobby ) then
			return id
		end
	end
	
	return 0
end