function Parkour(ply, key)
ply.canWallKick = false
	Now		= CurTime()
	if ply:KeyDown(IN_JUMP) then
		ply.canWallKick = true
		if !ply.coolDown then
			ply.coolDown = Now
		end
	end
	if ply.canWallKick and (Now > ply.coolDown) then

		ReboundPower = 300 --300
		KickPower = 355 --355
		VerticalPower = 200 --200
		local coolDown = Now + 0.2

		local tr = 	util.TraceLine(util.GetPlayerTrace(ply, Vector(0, 0, -1)))
		if !tr.Hit then end

		local HitDistance = tr.HitPos:Distance(tr.StartPos)
		if (HitDistance > 10) then
			local vForward = ply:GetForward():GetNormalized()
			local vRight = ply:GetRight():GetNormalized()
			local vVelocity = Vector(0,0,0)
			local pVelocity = ply:GetVelocity():Length()
	-----------------------------------------------------
						-- WALLJUMP COOLDOWN
			--WALLJUMPING
			if ply:KeyDown(IN_MOVERIGHT) then
				-- Trace Right
				local tracedata = {}
				tracedata.start = ply:GetPos()+Vector(0,0,50)
				tracedata.endpos = ply:GetPos()+(vRight*-40)
				tracedata.filter = ply
				local tr = util.TraceLine(tracedata)

				-- Detect Wall
				if (tr.Fraction < 1) and not tr.HitSky then
					vVelocity = vVelocity + (vRight * ReboundPower)
					vVelocity = vVelocity * ReboundPower / KickPower
					vVelocity.z = vVelocity.z + VerticalPower
					if SERVER then
						ply:EmitSound("npc/footsteps/hardboot_generic1.wav")
					end
					ply.coolDown = coolDown
					ply:SetVelocity(vVelocity)
				end
			end

			if ply:KeyDown(IN_MOVELEFT) then
				-- Trace Left
				local tracedata = {}
				tracedata.start = ply:GetPos()+Vector(0,0,50)
				tracedata.endpos = ply:GetPos()+(vRight*40)
				tracedata.filter = ply
				local tr = util.TraceLine(tracedata)

				-- Detect Wall
				if (tr.Fraction < 1) and not tr.HitSky then
					vVelocity = vVelocity + (vRight * -ReboundPower)
					vVelocity = vVelocity * ReboundPower / KickPower
					vVelocity.z = vVelocity.z + VerticalPower
					if SERVER then
						ply:EmitSound("npc/footsteps/hardboot_generic1.wav")
					end
					ply.coolDown = coolDown
					ply:SetVelocity(vVelocity)
				end
			end

			if ply:KeyDown(IN_BACK) then
				-- Trace Forward
				local tracedata = {}
				tracedata.start = ply:GetPos()+Vector(0,0,50)
				tracedata.endpos = ply:GetPos()+(vForward*40)
				tracedata.filter = ply
				local tr = util.TraceLine(tracedata)

				-- Detect Wall
				if (tr.Fraction < 1) and not tr.HitSky then
					vVelocity = vVelocity + (vForward * -ReboundPower)
					vVelocity = vVelocity * ReboundPower / KickPower
					vVelocity.z = vVelocity.z + VerticalPower
					if SERVER then
						ply:EmitSound("npc/footsteps/hardboot_generic1.wav")
					end
					ply.coolDown = coolDown
					ply:SetVelocity(vVelocity)
				end
			end

			if ply:KeyDown(IN_FORWARD) then
				-- Trace Backwards
				local tracedata = {}
				tracedata.start = ply:GetPos()+Vector(0,0,50)
				tracedata.endpos = ply:GetPos()+(vForward*-40)
				tracedata.filter = ply
				local tr = util.TraceLine(tracedata)

				-- Detect Wall
				if (tr.Fraction < 1) and not tr.HitSky then
					vVelocity = vVelocity + (vForward * ReboundPower)
					vVelocity = vVelocity * ReboundPower / KickPower
					vVelocity.z = vVelocity.z + VerticalPower
					if SERVER then
						ply:EmitSound("npc/footsteps/hardboot_generic1.wav")
					end
					ply.coolDown = coolDown
					ply:SetVelocity(vVelocity)
				end
			end
		end
	end
end


hook.Add("KeyPress", "Parkour", Parkour)
