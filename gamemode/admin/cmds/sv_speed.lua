
Admin.Commands:createCMD("resettop", "caller ; user", function(caller, user)
  if !user or user == "" then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "Please enter a steamID or nick.")
    return
  end
  local nick = Admin.Commands:autoComplete( user )
  if nick == nil then
    if string.Split( string.lower(user), ":" )[1] == user then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "This is not a target!")
      return
    end
    if !caller:IsHigherThan( Admin.Player:GetRank(user) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
      return
    end
    local sid = string.Split( string.lower(user), ":" )
    if string.match(sid[1], "steam_") and isnumber(util.StringToType( sid[2], "Float" )) and isnumber(util.StringToType( sid[2], "Float" )) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You reset ', Admin.Colors["VGAM"], user, Admin.Colors["normal"], "'s topspeed.")
      stats:Remove(user)
    else
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You entered an invalid steamID or Nick.")
    end
  else
    nick = string.gsub(nick, '"', "")
    if !caller:IsHigherThan( plyFromNick(nick):GetRank() ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
      return
    end
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You reset ', Admin.Colors["VGAM"], nick, Admin.Colors["normal"], "'s topspeed.")
    plyFromNick(nick):resetATTop()
  end

end)

Admin.Commands:createCMD("topspeed", "caller ; user", function(caller)
  if stats.session.topAllTime == {} then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "There is no top speeds!")
    return
  end
  chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Top Speeds:")
  --table.sort( stats.session.topAllTime, function( a, b ) return a[2] > b[2] end )
  local lb = {}
  for sID, stats in pairs(stats.session.topAllTime) do
    local count = #lb+1
    if count <= 10 then
      lb[count] = stats
    end

  end
  table.sort( lb, function( a, b ) return a["speed"] > b["speed"] end )

  for k, v in pairs(lb) do
    chat.Text(caller, Admin.Colors["VGAM"], "[", Admin.Colors["normal"], k, Admin.Colors["VGAM"], "] Name", Admin.Colors["normal"], ": " .. v["name"], Admin.Colors["VGAM"], " Speed", Admin.Colors["normal"], ": " .. v["speed"], Admin.Colors["VGAM"], " Date", Admin.Colors["normal"], ": " .. v["date"] )
  end
end)
