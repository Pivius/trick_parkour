
--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

include( 'shared.lua' )
include( 'getset.lua')
include( 'UtiliT.lua' )
include( 'cl_font.lua' )
include( 'cl_context.lua' )
include( 'cl_hud.lua' )
include( 'cl_lobby.lua' )
include( 'sh_wallslide.lua' )
include( 'cl_trails.lua' )
include( 'sh_surfing.lua' )
include( 'sh_stats.lua' )
include( 'cl_stats.lua' )
include("sh_editor.lua")
include("cl_editor.lua")
include('admin/cl_commands.lua')
include('sh_teleport.lua')
include("sh_walljump.lua")

--
-- Make BaseClass available
--
DEFINE_BASECLASS( "gamemode_base" )


function GM:Initialize()

	BaseClass.Initialize( self )

end
InitalizeFonts()
