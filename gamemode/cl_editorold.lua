Editor.UI = {} or Editor.UI

hook.Add("OnEntityCreated", "Initialize editor", function(ent)
  if ent == LocalPlayer() then
    GetSet("Editing", "Player")
    GetSet("panelEditing", "Player")
    LocalPlayer():SetEditing(false)
    LocalPlayer():SetpanelEditing(nil)
  end
end)

Zones = {}
Tricks = {}
SequenceOpen  = false
IgnoreOpen    = false
Draw          = false
posMode       = "Feet"

CurrentZone = {
  Name = "Untitled",
  ID = 1,
  Point1 = Vector(0, 0, 0),
  Point2 = Vector(0, 0, 0),
  Mode = "Enter"
}

CurrentTrick = {
  Name = "Untitled",
  ID = 1,
  Sequence = {},
  Limit = 0
}

net.Receive("Update Zone", function(len, ply)
  local cache = net.ReadTable()
  if UT:TblCompare(Zones,cache) then return end
  UT:MergeTableTo(Zones,cache)

end)

Editor.Panels = {}

function Editor.RenderBox(pos1, pos2)
  if !pos1 then pos1= Vector(0,0,0) end
  if !pos2 then pos2= Vector(0,0,0) end
	local Col, Width = Color( 0, 255, 255, 255 ), 1
  local DrawMaterial = Material( "materials/zone/zone.png" )
  if !Draw then
    pos1 = Vector(0,0,0)
    pos2 = Vector(0,0,0)
  end
  hook.Add("PreDrawTranslucentRenderables", "draws a box", function()
    render.SetMaterial( DrawMaterial )
  	render.DrawBeam( Vector(pos1.x, pos1.y, pos1.z), Vector(pos1.x, pos2.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos1.z), Vector(pos2.x, pos2.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos1.z), Vector(pos2.x, pos1.y, pos1.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos1.z), Vector(pos1.x, pos1.y, pos1.z), Width, 0, 1, Col )

  	render.DrawBeam( Vector(pos1.x, pos1.y, pos2.z), Vector(pos1.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos2.z), Vector(pos2.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos2.z), Vector(pos2.x, pos1.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos2.z), Vector(pos1.x, pos1.y, pos2.z), Width, 0, 1, Col )

  	render.DrawBeam( Vector(pos1.x, pos1.y, pos1.z), Vector(pos1.x, pos1.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos1.x, pos2.y, pos1.z), Vector(pos1.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos2.y, pos1.z), Vector(pos2.x, pos2.y, pos2.z), Width, 0, 1, Col )
  	render.DrawBeam( Vector(pos2.x, pos1.y, pos1.z), Vector(pos2.x, pos1.y, pos2.z), Width, 0, 1, Col )
  end)
end

function Editor.UI:Reset()
  for k ,v in pairs(vgui.GetWorldPanel():GetChildren()) do
    if string.match(tostring(v), "Editor") then
      v:Remove()
    end
  end
end
Editor.UI:Reset()

function Editor.CreateButton(name, x, y, text, parent, func)
  local PANEL = vgui.Create( "Button", nil, "Editor " .. name )
  table.insert(Editor.Panels, PANEL)
  PANEL:SetSize(175,36)
  PANEL:NewColor(Color(0,0,0,200))
  PANEL:NewHighlight(Color(0,0,0,230))
  PANEL:NewFlash(Color(255,255,255,150))
  PANEL:SetParent(parent)
  PANEL:SetPos(0,y)
  PANEL:SetTxt(text)
  PANEL:Funct(function()
    PANEL.panel:SetColor(PANEL.flash)
    PANEL.panel:ColorTo(PANEL.highlight, 0.1, 0.1)
    local funct = func(PANEL)
  end)
  PANEL:Create()
  PANEL:Hide( 0.1, 0, -200, "x")
  timer.Simple(0.1, function() PANEL:Show( 255, 0.5, 0, x, "x") end)
  return PANEL
end

function Editor.CreateText(name, x, y, text, font, parent)
  local TEXT = vgui.Create( "Text", nil, "Editor " .. name )
  table.insert(Editor.Panels, TEXT)
  TEXT:SetSize(ScrW()*0.17,ScrH()*0.55)
  TEXT:SetCol(Color(0,0,0))
  TEXT:SetPos(-200,y)
  TEXT:SetFont(font)
  TEXT:SetTxt(text)
  TEXT:SetParent(parent)
  TEXT:Create()
  TEXT:Hide( 0.1, 0, -200, "x")
  timer.Simple(0.1, function() TEXT:Show( 255, 0.5, 0, x, "x") end)
end

-- CREATE ZONE BUTTONS
function Editor.DrawCreateZone(CreateZonePanel)
  Editor.CreateButton("Editor CZPoint 1", ((ScrW()*0.17)-175)-20, 75, "Point 1", CreateZonePanel.bg, function(self)
    Draw = true
    if posMode == "Feet" then
      CurrentZone.Point1 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point1 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  Editor.CreateButton("Editor CZPoint 2", ((ScrW()*0.17)-175)-20, 115, "Point 2", CreateZonePanel.bg, function(self)
    Draw = true
    if posMode == "Feet" then
      CurrentZone.Point2 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point1 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point2 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point1 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  Editor.CreateButton("Editor CZPos", ((ScrW()*0.17)-175)-20, 155, "Pos: ".. posMode, CreateZonePanel.bg, function(self)
    if posMode == "Feet" then
      posMode = "Aim"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() print(posMode) end)
    else
      posMode = "Feet"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  Editor.CreateButton("Editor CZMode", ((ScrW()*0.17)-175)-20, 195, "Mode: " .. CurrentZone.Mode, CreateZonePanel.bg, function(self)
    if CurrentZone.Mode == "Enter" then
      CurrentZone.Mode = "Surfing"
      self:SetTxt("Mode: Surfing")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Surfing" then
      CurrentZone.Mode = "Ground"
      self:SetTxt("Mode: Ground")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Ground" then
      CurrentZone.Mode = "Enter"
      self:SetTxt("Mode: Enter")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  Editor.CreateButton("Editor CZSave", ((ScrW()*0.17)-175)-20, 235, "Save", CreateZonePanel.bg, function(self)
    Draw = false
    Editor.RenderBox()
    table.SortByMember( Zones, "ID" )
    CurrentZone.ID = table.Count(Zones)+1
    print(CurrentZone.ID)
    net.Start("Create Zone")
      net.WriteTable(CurrentZone)
    net.SendToServer()
  end)
end
-- EDIT ZONE BUTTONS
function Editor.DrawEditZone(parent, EditZonePanel)
  Editor.CreateButton("Editor EZPoint 1", ((ScrW()*0.17)-175)-20, 75, "Point 1", parent.bg, function(self)
    if posMode == "Feet" then
      CurrentZone.Point1 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point1 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  Editor.CreateButton("Editor EZPoint 2", ((ScrW()*0.17)-175)-20, 115, "Point 2", parent.bg, function(self)
    if posMode == "Feet" then
      CurrentZone.Point2 = UT:TraceVec(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    else
      CurrentZone.Point2 = UT:TraceAim(LocalPlayer())
      if CurrentZone.Point2 != Vector(0,0,0) then
        Editor.RenderBox(CurrentZone.Point1, CurrentZone.Point2)
      end
    end
  end)

  Editor.CreateButton("Editor EZPos", ((ScrW()*0.17)-175)-20, 155, "Pos: " .. posMode, parent.bg, function(self)
    if posMode == "Feet" then
      posMode = "Aim"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    else
      posMode = "Feet"
      self:SetTxt("Pos: "..posMode)
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  Editor.CreateButton("Editor EZMode", ((ScrW()*0.17)-175)-20, 195, "Mode: " .. CurrentZone.Mode, parent.bg, function(self)
    if CurrentZone.Mode == "Enter" then
      CurrentZone.Mode = "Surfing"
      self:SetTxt("Mode: Surfing")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Surfing" then
      CurrentZone.Mode = "Ground"
      self:SetTxt("Mode: Ground")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    elseif CurrentZone.Mode == "Ground" then
      CurrentZone.Mode = "Enter"
      self:SetTxt("Mode: Enter")
      timer.Simple(0.1, function() self:UpdatePaint() end)
    end
  end)

  Editor.CreateButton("Editor EZSave", ((ScrW()*0.17)-175)-20, 235, "Save", parent.bg, function(self)
    Draw = false
    Editor.RenderBox()
    table.SortByMember( Zones, "ID", true )
    for k ,v in pairs(Zones) do
      if v.Name == LocalPlayer():GetpanelEditing() then
        print("hotdamn")
        CurrentZone.ID = v.ID
        Zones[v.ID] = CurrentZone
        timer.Simple(0.1, function() PrintTable(Zones) addItems() end)
      end
      for k ,v in pairs(Editor.Panels) do
        if string.match(tostring(v), "Zone Editor ") then

          EditZonePanel:RemoveItem(v)
          v:Remove()
          table.RemoveByValue(Editor.Panels, v )
        end
      end
    end



    net.Start("Update")
      net.WriteTable(CurrentZone)
    net.SendToServer()

    parent:Hide( 0.1, 0, (ScrW()*0.17)+10, "x")
    LocalPlayer():SetpanelEditing(nil)
    LocalPlayer():SetEditing(false)

    CurrentZone = {
      Name = "Untitled",
      ID = 1,
      Point1 = Vector(0,0,0),
      Point2 = Vector(0,0,0),
      Mode = "Ground"
    }
  end)
end

-- CREATE Trick BUTTONS
function Editor.DrawCreateTrick(CreateTrickPanel, GetZonesPanel, SequenceList)
  --CREATE SEQUENCE
  Editor.CreateButton("Editor CTSequence", ((ScrW()*0.17)-175)-20, 75, "Sequence", CreateTrickPanel.bg, function(self)
    if IgnoreOpen then
      Editor.ZonelistRm(GetZonesPanel)
      IgnoreOpen = false
    end

    if SequenceOpen then
      Editor.ZonelistRm(GetZonesPanel)
      GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
      SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
      timer.Simple(0.21, function()
        GetZonesPanel:Hide( 0, 0, 5, "x", 0)
        SequenceList:Hide( 0, 0, 5, "x", 0)
      end)
      timer.Simple(0.25, function()

        SequenceOpen = false
      end)
    else
      SequenceOpen = true
      GetZonesPanel:Show( 255, 0.5, 0, (ScrW()*0.352), "x")
      SequenceList:Show( 255, 0.5, 0, (ScrW()*0.526), "x")
      Editor.Zonelist(GetZonesPanel, function(panel)
        local zL = panel
        panel.panel:SetColor(panel.flash)
        panel.panel:ColorTo(panel.highlight, 0.1, 0.1)
        table.insert(CurrentTrick.Sequence, panel.Text)
        Editor.sequenceList(zL.Text, SequenceList, function()
          print(zL.Text)
          for k ,v in pairs(Editor.Panels) do
            if string.match(tostring(v), "Editor SequenceBut " .. zL.Text) then
              SequenceList:RemoveItem(v)
              v:Remove()
              table.RemoveByValue(Editor.Panels, v)
            end
          end
        end)
      end)
    end
  end)
  --IGNORE LIST
  Editor.CreateButton("Editor CTIgnore", ((ScrW()*0.17)-175)-20, 195, "Ignore Zones", CreateTrickPanel.bg, function(self)
    if SequenceOpen then
      Editor.ZonelistRm(GetZonesPanel)
      SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
      timer.Simple(0.21, function()
        SequenceList:Hide( 0, 0, 5, "x", 0)
      end)
      SequenceOpen = false
    end

    if IgnoreOpen then
      Editor.ZonelistRm(GetZonesPanel)
      GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
      timer.Simple(0.21, function()
        GetZonesPanel:Hide( 0, 0, 5, "x", 0)

      end)
      timer.Simple(0.25, function()

        IgnoreOpen = false
      end)
    else
      timer.Simple(0.1, function()
        IgnoreOpen = true
        GetZonesPanel:Show( 255, 0.5, 0, (ScrW()*0.34)+15, "x")
        Editor.Zonelist(GetZonesPanel, function(panel)
          panel.panel:SetColor(panel.flash)
          panel.panel:ColorTo(panel.highlight, 0.1, 0.1)
          print("Jeff")
        end)
      end)
    end
  end)

  Editor.CreateButton("Editor CTSave", ((ScrW()*0.17)-175)-20, 235, "Save", CreateTrickPanel.bg, function(self)
    /*table.SortByMember( Tricks, "ID" )
    CurrentTrick.ID = table.Count(Tricks)+1
    print(CurrentTrick.ID)
    net.Start("Create Trick")
      net.WriteTable(CurrentTrick)
    net.SendToServer()
    Editor.sequenceListRm(SequenceList)
    */
  end)
end

function Editor.updateCurZone(name, id, point1, point2, mode)
  CurrentZone = {
    Name = name,
    ID = id,
    Point1 = point1,
    Point2 = point2,
    Mode = mode
  }
end

function Editor.sequenceList(name, panel, func)
  local ADD = vgui.Create( "Button", nil, "Editor SequenceBut " .. name .. #CurrentTrick.Sequence )
  table.insert(Editor.Panels, ADD)

  ADD:SetSize(175,36)
  ADD:NewColor(Color(0,0,0,200))
  ADD:NewHighlight(Color(0,0,0,230))
  ADD:NewFlash(Color(255,255,255,150))
  ADD:SetPos(0,0)
  ADD:SetParent(panel.list)
  ADD:SetDock(TOP)
  ADD:SetMargin(20, 0, 0, 1)
  ADD:SetTxt(name)
  ADD:Funct(function()
    ADD.panel:SetColor(ADD.flash)
    ADD.panel:ColorTo(ADD.highlight, 0.1, 0.1)
    local funct = func(ADD)
  end)
  --ADD:Show( 255, 0.5, 0, 0)
  ADD:Create()

  panel:Item(ADD)
end

function Editor.sequenceListRm(parent)
  for k ,v in pairs(Editor.Panels) do
    if v then
      if string.match(tostring(v), "Editor SequenceBut") then
        timer.Create("SeqLRemove " .. tostring(v), 0.5, 1, function() parent:RemoveItem(v) v:Remove() table.RemoveByValue(Editor.Panels, v)  end)
      end
    end
  end
end


function Editor.Zonelist(panel, func)
  for k, v in pairs(Zones) do

    local ADD = vgui.Create( "Button", nil, "Editor Zone List " .. v.Name )
    table.insert(Editor.Panels, ADD)

    ADD:SetSize(175,36)
    ADD:NewColor(Color(0,0,0,200))
    ADD:NewHighlight(Color(0,0,0,230))
    ADD:NewFlash(Color(255,255,255,150))
    ADD:SetPos(0,0)
    ADD:SetParent(panel.list)
    ADD:SetDock(TOP)
    ADD:SetMargin(20, 0, 0, 1)
    ADD:SetTxt(v.Name)
    ADD:Funct(function()
      ADD.panel:SetColor(ADD.flash)
      ADD.panel:ColorTo(ADD.highlight, 0.1, 0.1)
      local funct = func(ADD)
    end)
    ADD:Create()

    panel:Item(ADD)
  end
end

function Editor.ZonelistRm(parent)
  for k ,v in pairs(Editor.Panels) do
    if v then
      if string.match(tostring(v), "Editor Zone List") then
        timer.Create("ZLRemove " .. tostring(v), 0.5, 1, function() parent:RemoveItem(v) v:Remove() table.RemoveByValue(Editor.Panels, v) end)
      end
    end
  end
end

function Editor.Draw()
  if LocalPlayer():GetNoclip() then
    Selection = 0
    /*
 ██████ ██████  ███████  █████  ████████ ███████     ███████  ██████  ███    ██ ███████
██      ██   ██ ██      ██   ██    ██    ██             ███  ██    ██ ████   ██ ██
██      ██████  █████   ███████    ██    █████         ███   ██    ██ ██ ██  ██ █████
██      ██   ██ ██      ██   ██    ██    ██           ███    ██    ██ ██  ██ ██ ██
 ██████ ██   ██ ███████ ██   ██    ██    ███████     ███████  ██████  ██   ████ ███████
*/


    -- CREATE ZONE
    local CreateZonePanel = vgui.Create( "vgui panel", nil, "Editor CreateZonePanel" )
    table.insert(Editor.Panels, CreateZonePanel)
    CreateZonePanel:SetSize(ScrW()*0.17,ScrH()*0.4)
    CreateZonePanel:SetCol(Color(0,0,0,200))
    CreateZonePanel:SetPos(5,5)
    CreateZonePanel:Create()
    CreateZonePanel:Hide( 0.1, 0, 5, "x")

    -- CREATE ZONE TITLE TEXT
    Editor.CreateText("CREATE ZONE", (ScrW()*0.17)/5.5, 10, "CREATE ZONE", "Editor Text", CreateZonePanel.bg)

    -- ZONE TITLE EDITOR
    local CZText = vgui.Create( "EditText", nil, "Editor CZText" )
    table.insert(Editor.Panels, CZText)
    CZText:SetSize(175,36)
    CZText:SetCol(Color(0,0,0,200))
    CZText:NewHighlight(Color(0,0,0,230))
    CZText:NewFlash(Color(255,255,255,150))
    CZText:SetParent(CreateZonePanel.bg)
    CZText:SetPos(0,35)
    CZText:SetTxt("Untitled")
    CZText:Funct(function()
      CZText.panel:SetColor(CZText.flash)
      CZText.panel:ColorTo(CZText.highlight, 0.1, 0.1)
      CurrentZone.Name = CZText.Text

    end)
    CZText:Create()
    timer.Simple(0.1, function() CZText:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x") end)

    -- DRAW PANELS
    Editor.DrawCreateZone(CreateZonePanel)

    /*
    ███████ ██████  ██ ████████     ███████  ██████  ███    ██ ███████
    ██      ██   ██ ██    ██           ███  ██    ██ ████   ██ ██
    █████   ██   ██ ██    ██          ███   ██    ██ ██ ██  ██ █████
    ██      ██   ██ ██    ██         ███    ██    ██ ██  ██ ██ ██
    ███████ ██████  ██    ██        ███████  ██████  ██   ████ ███████
    */

    -- EDIT ZONE BG
    local EditZoneP = vgui.Create( "vgui panel", nil, "Editor EditZoneP" )
    table.insert(Editor.Panels, EditZoneP)
    EditZoneP:SetSize(ScrW()*0.17,ScrH()*0.4)
    EditZoneP:SetCol(Color(0,0,0,200))
    EditZoneP:SetPos(5,5)
    EditZoneP:Create()
    EditZoneP:Hide( 0.1, 0, 5, "x")

    -- EDIT ZONE TEXT
    Editor.CreateText("EDIT ZONE2", (ScrW()*0.17)/4, 10, "EDIT ZONE", "Editor Text", EditZoneP.bg)

    -- EDIT ZONE TEXT EDITOR
    local EZText = vgui.Create( "EditText", nil, "Editor EZText" )
    table.insert(Editor.Panels, EZText)
    EZText:SetSize(175,36)
    EZText:SetCol(Color(0,0,0,200))
    EZText:NewHighlight(Color(0,0,0,230))
    EZText:NewFlash(Color(255,255,255,150))
    EZText:SetParent(EditZoneP.bg)
    EZText:SetPos(0, 35)
    EZText:SetTxt("Nothing")
    EZText:Funct(function()
      EZText.panel:SetColor(EZText.flash)
      EZText.panel:ColorTo(EZText.highlight, 0.1, 0.1)
      CurrentZone.Name = EZText.Text

    end)
    EZText:Create()
    timer.Simple(0.1, function() EZText:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x") end)

    //ZONE LIST
    local EditZonePanel = vgui.Create( "Scroll", nil, "Editor EditZonePanel" )
    table.insert(Editor.Panels, EditZonePanel)
    EditZonePanel:SetSize(ScrW()*0.17,ScrH()*0.55)
    EditZonePanel:NewColor(Color(0,0,0,200))
    EditZonePanel:SetPos(5,5)
    EditZonePanel:SetTitle("")
    EditZonePanel:Funct(function() end)
    EditZonePanel:Create()
    EditZonePanel:Hide( 0.1, 0, 5, "x")

    Editor.DrawEditZone(EditZoneP, EditZonePanel)

    -- EDIT ZONE TITLE
    Editor.CreateText("EDIT ZONE", (ScrW()*0.17)/4, 10, "EDIT ZONE", "Editor Text", EditZonePanel.mainpanel.bg)
    -- ADDS ITEMS TO LIST
    function addItems()
      for k, v in pairs(Zones) do

        local ADD = vgui.Create( "Button", nil, "Zone Editor " .. v.Name )
        table.insert(Editor.Panels, ADD)

        ADD:SetSize(175,36)
        ADD:NewColor(Color(0,0,0,200))
        ADD:NewHighlight(Color(0,0,0,230))
        ADD:NewFlash(Color(255,255,255,150))
        ADD:SetPos(0,0)
        ADD:SetParent(EditZonePanel.list)
        ADD:SetDock(TOP)
        ADD:SetMargin(20, 0, 0, 1)
        ADD:SetTxt(v.Name)
        ADD:Funct(function()
        ADD.panel:SetColor(ADD.flash)
        ADD.panel:ColorTo(ADD.highlight, 0.1, 0.1)
        if !LocalPlayer():GetEditing() then
          EditZoneP:Show(255, 0.1, 0, (ScrW()*0.351), "x")
          LocalPlayer():SetEditing(true)
          LocalPlayer():SetpanelEditing(v.Name)
          EZText:SetTxt(LocalPlayer():GetpanelEditing())
          Draw = true
          Editor.updateCurZone(v.Name, v.ID, v.Point1, v.Point2, v.Mode)
          CurrentZone = {
            Name = v.Name,
            ID = v.ID,
            Point1 = v.Point1,
            Point2 = v.Point2,
            Mode = v.Mode
          }
          Editor.RenderBox(v.Point1, v.Point2)
        elseif LocalPlayer():GetEditing() and v.Name == LocalPlayer():GetpanelEditing() then
          EditZoneP:Hide( 0.1, 0, 5, "x")
          LocalPlayer():SetpanelEditing(nil)
          LocalPlayer():SetEditing(false)
          CurrentZone = {
            Name = "Untitled",
            ID = 1,
            Point1 = Vector(0,0,0),
            Point2 = Vector(0,0,0),
            Mode = "Ground"
          }
          Editor.RenderBox()
          Draw = false
        end
        if LocalPlayer():GetEditing() and v.Name != LocalPlayer():GetpanelEditing() then
          EditZoneP:Hide( 0.1, 0, 5, "x")
          timer.Simple(0.1, function() EditZoneP:Show(255, 0.1, 0, (ScrW()*0.351), "x") end)
          LocalPlayer():SetpanelEditing(v.Name)
          EZText:SetTxt(LocalPlayer():GetpanelEditing())
          CurrentZone = {
            Name = v.Name,
            ID = v.ID,
            Point1 = v.Point1,
            Point2 = v.Point2,
            Mode = v.Mode
          }
          Editor.RenderBox(v.Point1, v.Point2)
        end
      end)
      ADD:Create()

      EditZonePanel:Item(ADD)
    end
  end
  addItems()

  /*
 ██████ ██████  ███████  █████  ████████ ███████     ████████ ██████  ██  ██████ ██   ██
██      ██   ██ ██      ██   ██    ██    ██             ██    ██   ██ ██ ██      ██  ██
██      ██████  █████   ███████    ██    █████          ██    ██████  ██ ██      █████
██      ██   ██ ██      ██   ██    ██    ██             ██    ██   ██ ██ ██      ██  ██
 ██████ ██   ██ ███████ ██   ██    ██    ███████        ██    ██   ██ ██  ██████ ██   ██
*/


  -- CREATE TRICK PANEL
  local CreateTrickPanel = vgui.Create( "vgui panel", nil, "Editor CreateTrickPanel" )
  table.insert(Editor.Panels, CreateTrickPanel)
  CreateTrickPanel:SetSize(ScrW()*0.17,ScrH()*0.55)
  CreateTrickPanel:SetCol(Color(0,0,0,200))
  CreateTrickPanel:SetPos(5,5)
  CreateTrickPanel:Create()
  CreateTrickPanel:Hide( 0.1, 0, 5, "x")

  -- CREATE TRICK TITLE TEXT
  Editor.CreateText("CREATE TRICK", (ScrW()*0.17)/5.5, 10, "CREATE TRICK", "Editor Text", CreateTrickPanel.bg)

  -- CREATE TRICK TITLE EDITOR
  local CTText = vgui.Create( "EditText", nil, "Editor CTText" )
  table.insert(Editor.Panels, CTText)
  CTText:SetSize(175,36)
  CTText:SetCol(Color(0,0,0,200))
  CTText:NewHighlight(Color(0,0,0,230))
  CTText:NewFlash(Color(255,255,255,150))
  CTText:SetParent(CreateTrickPanel.bg)
  CTText:SetPos(0,35)
  CTText:SetTxt("Untitled")
  CTText:Funct(function()
    CTText.panel:SetColor(CTText.flash)
    CTText.panel:ColorTo(CTText.highlight, 0.1, 0.1)
    CurrentTrick.Name = CTText.Text

  end)
  CTText:Create()
  timer.Simple(0.1, function() CTText:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x") end)

  -- SPEED LIMITER
  local CTLimit = vgui.Create( "EditText", nil, "Editor CTLimit" )
  table.insert(Editor.Panels, CTLimit)
  CTLimit:SetSize(175,36)
  CTLimit:SetCol(Color(0,0,0,200))
  CTLimit:NewHighlight(Color(0,0,0,230))
  CTLimit:NewFlash(Color(255,255,255,150))
  CTLimit:SetParent(CreateTrickPanel.bg)
  CTLimit:SetPos(0,115)
  CTLimit:SetTxt("Speed Limit")
  CTLimit:Funct(function()
    CTLimit.panel:SetColor(CTLimit.flash)
    CTLimit.panel:ColorTo(CTLimit.highlight, 0.1, 0.1)
    CurrentTrick.Limit = CTLimit.Text

  end)
  CTLimit:Create()
  timer.Simple(0.1, function() CTLimit:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x") end)

  -- Points
  local CTPts = vgui.Create( "EditText", nil, "Editor CTPts" )
  table.insert(Editor.Panels, CTPts)
  CTPts:SetSize(175,36)
  CTPts:SetCol(Color(0,0,0,200))
  CTPts:NewHighlight(Color(0,0,0,230))
  CTPts:NewFlash(Color(255,255,255,150))
  CTPts:SetParent(CreateTrickPanel.bg)
  CTPts:SetPos(0,155)
  CTPts:SetTxt("Points")
  CTPts:Funct(function()
    CTPts.panel:SetColor(CTPts.flash)
    CTPts.panel:ColorTo(CTPts.highlight, 0.1, 0.1)
    CurrentTrick.Limit = CTPts.Text

  end)
  CTPts:Create()
  timer.Simple(0.1, function() CTPts:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x") end)

  //ZONE LIST
  local GetZonesPanel = vgui.Create( "Scroll", nil, "Editor GetZonesPanel" )
  table.insert(Editor.Panels, GetZonesPanel)
  GetZonesPanel:SetSize(ScrW()*0.17,ScrH()*0.55)
  GetZonesPanel:NewColor(Color(0,0,0,200))
  GetZonesPanel:SetPos(5,5)
  GetZonesPanel:SetTitle("")
  GetZonesPanel:Funct(function() end)
  GetZonesPanel:Create()
  GetZonesPanel:Hide( 0.1, 0, 5, "x")

  -- ZONE LIST TITLE
  Editor.CreateText("ZONE LIST", (ScrW()*0.17)/4, 10, "ZONE LIST", "Editor Text", GetZonesPanel.mainpanel.bg)

  //SEQUENCE LIST
  local SequenceList = vgui.Create( "Scroll", nil, "Editor Sequence" )
  table.insert(Editor.Panels, SequenceList)
  SequenceList:SetSize(ScrW()*0.17,ScrH()*0.55)
  SequenceList:NewColor(Color(0,0,0,200))
  SequenceList:SetPos(5,5)
  SequenceList:SetTitle("")
  SequenceList:Funct(function() end)
  SequenceList:Create()
  SequenceList:Hide( 0.1, 0, 5, "x")


  -- ZONE LIST TITLE
  Editor.CreateText("SEQUENCE LIST", (ScrW()*0.17)/4, 10, "SEQUENCE", "Editor Text", SequenceList.mainpanel.bg)

  Editor.DrawCreateTrick(CreateTrickPanel, GetZonesPanel, SequenceList)

  /*
███████ ██████  ██ ████████     ████████ ██████  ██  ██████ ██   ██
██      ██   ██ ██    ██           ██    ██   ██ ██ ██      ██  ██
█████   ██   ██ ██    ██           ██    ██████  ██ ██      █████
██      ██   ██ ██    ██           ██    ██   ██ ██ ██      ██  ██
███████ ██████  ██    ██           ██    ██   ██ ██  ██████ ██   ██
*/


  local EditTrickPanel = vgui.Create( "vgui panel", nil, "Editor EditTrickPanel" )
  table.insert(Editor.Panels, EditTrickPanel)
  EditTrickPanel:SetSize(ScrW()*0.17,ScrH()*0.55)
  EditTrickPanel:SetCol(Color(0,0,0,200))
  EditTrickPanel:SetPos(5,5)
  EditTrickPanel:Create()
  EditTrickPanel:Hide( 0.1, 0, 5, "x")

  local EditorPanel = vgui.Create( "vgui panel", nil, "Editor EditorPanel" )
  table.insert(Editor.Panels, EditorPanel)
  EditorPanel:SetSize(ScrW()*0.17,ScrH()*0.29)
  EditorPanel:SetCol(Color(0,0,0,200))
  EditorPanel:SetPos(0,5)
  EditorPanel:Create()
  EditorPanel:Hide( 0.1, 0, -200, "x")
  timer.Simple(0.1, function() EditorPanel:Show( 255, 0.5, 0, 5, "x") end)

  /*
  ███    ███  █████  ██ ███    ██     ██████  ██    ██ ████████ ████████  ██████  ███    ██ ███████
  ████  ████ ██   ██ ██ ████   ██     ██   ██ ██    ██    ██       ██    ██    ██ ████   ██ ██
  ██ ████ ██ ███████ ██ ██ ██  ██     ██████  ██    ██    ██       ██    ██    ██ ██ ██  ██ ███████
  ██  ██  ██ ██   ██ ██ ██  ██ ██     ██   ██ ██    ██    ██       ██    ██    ██ ██  ██ ██      ██
  ██      ██ ██   ██ ██ ██   ████     ██████   ██████     ██       ██     ██████  ██   ████ ███████
  */
  -- Title
  Editor.CreateText("TRICK EDITOR", (ScrW()*0.17)/5.5, 10, "TRICK EDITOR", "Editor Text", EditorPanel.bg)
  -- Create Zone
  CreateZone = nil
  Editor.CreateButton("Create Zone", ((ScrW()*0.17)-175)-20, 40, "Create Zone", "", function(self)
    LocalPlayer():SetpanelEditing(nil)
    LocalPlayer():SetEditing(false)
    CreateZone = self
    if Selection ~= 1 then
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175), "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      if EditZone then
        EditZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if EditTrick then
        EditTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      if CreateTrick then
        CreateTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      if EditZoneP then
        EditZoneP:Hide( 0.1, 0, 5, "x")
      end
      Editor.sequenceListRm(SequenceList)
      if SequenceOpen or IgnoreOpen then
        Editor.ZonelistRm(GetZonesPanel)
        GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
        SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
        timer.Simple(0.21, function()
          GetZonesPanel:Hide( 0, 0, 5, "x", 0)
          SequenceList:Hide( 0, 0, 5, "x", 0)
        end)
        timer.Simple(0.25, function()
          IgnoreOpen   = false
          SequenceOpen = false
        end)
      end
      CreateZonePanel:Show( 255, 0.3, 0, (ScrW()*0.17)+10, "x")
      CZText:SetTxt("Untitled")
      CurrentZone = {
        Name = "Untitled",
        Point1 = Vector(0, 0, 0),
        Point2 = Vector(0, 0, 0),
        Mode = "Enter"
      }
      CurrentTrick = {
        Name = "Untitled",
        ID = 1,
        Sequence = {},
        Limit = 0
      }
      Selection = 1
    else
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      CreateZonePanel:Hide( 0.3, 0, 5, "x")
      Selection = 0
    end
  end)
  EditZone = nil
  -- Edit Zone
  Editor.CreateButton("Edit Zone", ((ScrW()*0.17)-175)-20, 80, "Edit Zone", "", function(self)
    LocalPlayer():SetpanelEditing(nil)
    LocalPlayer():SetEditing(false)
    EditZone = self
    if Selection ~= 2 then
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175), "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      if CreateZone then
        CreateZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if EditTrick then
        EditTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      if CreateTrick then
        CreateTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      Editor.sequenceListRm(SequenceList)
      if SequenceOpen or IgnoreOpen then
        Editor.ZonelistRm(GetZonesPanel)
        GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
        SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
        timer.Simple(0.21, function()
          GetZonesPanel:Hide( 0, 0, 5, "x", 0)
          SequenceList:Hide( 0, 0, 5, "x", 0)
        end)
        timer.Simple(0.25, function()
          IgnoreOpen   = false
          SequenceOpen = false
        end)
      end
      EditZonePanel:Show( 255, 0.3, 0, (ScrW()*0.17)+10, "x")
      Selection = 2
    else
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      EditZonePanel:Hide( 0.3, 0, 5, "x")
      if EditZoneP then
        EditZoneP:Hide( 0.1, 0, 5, "x")
      end
      CurrentTrick = {
        Name = "Untitled",
        ID = 1,
        Sequence = {},
        Limit = 0
      }
      Selection = 0
    end
  end)

  -- Create Trick
  CreateTrick = nil
  Editor.CreateButton("Create Trick", ((ScrW()*0.17)-175)-20, 120, "Create Trick", "", function(self)
    LocalPlayer():SetpanelEditing(nil)
    LocalPlayer():SetEditing(false)
    CreateTrick = self
    if Selection ~= 3 then
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175), "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      if EditZone then
        EditZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if CreateZone then
        CreateZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if EditTrick then
        EditTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      if EditZoneP then
        EditZoneP:Hide( 0.1, 0, 5, "x")
      end
      CreateTrickPanel:Show( 255, 0.3, 0, (ScrW()*0.17)+10, "x")
      Selection = 3
      CTText:SetTxt("Untitled")
      CTLimit:SetTxt("Speed Limit")
      CTPts:SetTxt("Points")
      CurrentTrick = {
        Name = "Untitled",
        ID = 1,
        Sequence = {},
        Limit = 0
      }
      CurrentTrick = {
        Name = "Untitled",
        ID = 1,
        Sequence = {},
        Limit = 0
      }
    else
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      CreateTrickPanel:Hide( 0.3, 0, 5, "x")
      Selection = 0
      Editor.sequenceListRm(SequenceList)
      if SequenceOpen or IgnoreOpen then
        Editor.ZonelistRm(GetZonesPanel)
        GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
        SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
        timer.Simple(0.21, function()
          GetZonesPanel:Hide( 0, 0, 5, "x", 0)
          SequenceList:Hide( 0, 0, 5, "x", 0)
        end)
        timer.Simple(0.25, function()
          IgnoreOpen   = false
          SequenceOpen = false
        end)
      end
    end
  end)
  EditTrick = nil
  -- Edit Trick
  Editor.CreateButton("Edit Trick", ((ScrW()*0.17)-175)-20, 160, "Edit Trick", "", function(self)
    LocalPlayer():SetpanelEditing(nil)
    LocalPlayer():SetEditing(false)
    EditTrick = self
    if Selection ~= 4 then
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175), "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      if CreateZone then
        CreateZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if EditZone then
        EditZone:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        EditZonePanel:Hide( 0.3, 0, 5, "x")
      end
      if CreateTrick then
        CreateTrick:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
        CreateTrickPanel:Hide( 0.3, 0, 5, "x")
      end
      if EditZoneP then
        EditZoneP:Hide( 0.1, 0, 5, "x")
      end
      Editor.sequenceListRm(SequenceList)
      if SequenceOpen or IgnoreOpen then
        Editor.ZonelistRm(GetZonesPanel)
        GetZonesPanel:Hide( 0.2, 0, GetZonesPanel.X, "x")
        SequenceList:Hide( 0.2, 0, SequenceList.X, "x")
        timer.Simple(0.21, function()
          GetZonesPanel:Hide( 0, 0, 5, "x", 0)
          SequenceList:Hide( 0, 0, 5, "x", 0)
        end)
        timer.Simple(0.25, function()
          IgnoreOpen   = false
          SequenceOpen = false
        end)
      end
      CurrentTrick = {
        Name = "Untitled",
        ID = 1,
        Sequence = {},
        Limit = 0
      }
      EditTrickPanel:Show( 255, 0.3, 0, (ScrW()*0.17)+10, "x")
      Selection = 4
    else
      self:Show( 255, 0.5, 0, ((ScrW()*0.17)-175)-20, "x")
      self.panel:ColorTo(self.highlight, 0.1, 0.1)
      EditTrickPanel:Hide( 0.3, 0, 5, "x")
      Selection = 0
    end
  end)
end
end
hook.Add("UpdateDraw", "Draws the Editor UI", Editor.Draw)


function Editor.Remove()
  if LocalPlayer():GetNoclip() then return end
  for k ,v in pairs(Editor.Panels) do
    if v then
      print(v)
      v:Hide( 0.1, 0, -200, "x")
      timer.Create("ERemove " .. tostring(v), 1, 1, function() v:Remove() table.RemoveByValue(Editor.Panels, v) end)

    end
  end
end
hook.Add("Think", "Removes the Editor UI", Editor.Remove)
