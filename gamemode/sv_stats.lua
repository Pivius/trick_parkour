stats = stats or {}
stats.session = stats.session or {}
stats.session.topAllTime = stats.session.topAllTime or {}


local meta = FindMetaTable( "Player" )

util.AddNetworkString( "UpdateSpeedTopSess" )
util.AddNetworkString( "UpdateTopAll" )
util.AddNetworkString( "ResetTopAll" )


/*
  NAME - Insert
  FUNCTION - Inserts values to a mysql table
*/
function stats:Insert(sID, name, speed, date)
  local query = mysql:Insert(SQLDetails.tables.topspeed)
  query:Insert("sID", sID)
  query:Insert("name", name)
  query:Insert("speed", speed)
  query:Insert("date", date)
  query:Execute()
end

/*
  NAME - Update
  FUNCTION - Updates a mysql table
*/
function stats:Update(sID, name, speed, date)
  local query = mysql:Update(SQLDetails.tables.topspeed)

  query:Where("sID", sID)
  query:Update("name", name)
  query:Update("speed", speed)
  query:Update("date", date)
  query:Execute()
end

/*
  NAME - Remove
  FUNCTION - Removes a mysql table
*/
function stats:Remove(sID)
  local query = mysql:Delete(SQLDetails.tables.topspeed)
    query:Where("sID", sID)
  query:Execute()
  stats.session.topAllTime = {}
  stats:loadStats()
end

/*
  NAME - loadStats
  FUNCTION - Loads stats from a mysql table
*/
function stats:loadStats()
  local query = mysql:Select(SQLDetails.tables.topspeed)
    query:Callback(function(result, status, lastID)
      if (type(result) == "table" and #result > 0) then
        for k, v in pairs(result) do
          local sID = v["sID"]
          local name = v["name"]
          local speed = v["speed"]
          local date = v["date"]

          local statstbl = {
            [sID] = {
              ["name"] = name,
              ["speed"] = speed,
              ["date"] = date
            }
          }
          UT:MergeTableTo(stats.session.topAllTime, statstbl)

        end

      end
    end)
  query:Execute()
end

function AllTimeSpeed(ply)


  if ply:getTopSession() > ply:getTopAllTime() then
    ply:SettopSpeedAll(ply:getTopSession())
    UpdateTop( ply )

  end
end
hook.Add( "CheckAllTime", "Checks AllTime Top", AllTimeSpeed )
/*
  NAME - UpdateTop
  FUNCTION - Updates when player gets new alltime top
*/
function UpdateTop( ply )
  if !ply:IsPlayer() or ply:IsBot() then return end
  local name = ply:Nick()
  local sID  = ply:SteamID()

  local query = mysql:Select(SQLDetails.tables.topspeed)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)
    if type(res) == "table" and #res > 0 then
      stats:Update(sID, name, ply:getTopAllTime(), GetDate())
    else

      stats:Insert(sID, name, ply:getTopAllTime(), GetDate())
    end
    local users = {
      [sID] = {
        ["name"] = name,
        ["speed"] = ply:getTopAllTime(),
        ["date"] = GetDate()
      }
    }

    UT:MergeTableTo(stats.session.topAllTime, users)

    net.Start("UpdateTopAll")
      net.WriteEntity(ply)
      net.WriteTable(stats.session.topAllTime)
    net.Send(ply)
  end)
  query:Execute()
end

net.Receive("ResetTopSess", function(len)
  local ply = net.ReadEntity()
  ply:SettopSpeedSess(0)
end)

hook.Add("PlayerSpawn", "Set Top", function(ply)
  if !ply:GettopSpeedSess() then
    ply:SettopSpeedSess(0)
  end
  if stats.session.topAllTime[ply:SteamID()] then
    ply:SettopSpeedAll(stats.session.topAllTime[ply:SteamID()]["speed"])
  else
    ply:SettopSpeedAll(0)
  end
  timer.Create("Updt Delay " .. ply:Nick(), 0.5, 1, function()
    net.Start("UpdateTopAll")
      net.WriteEntity(ply)
      net.WriteTable(stats.session.topAllTime)
    net.Send(ply)
  end)
end)
