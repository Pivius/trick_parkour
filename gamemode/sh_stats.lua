stats = stats or {}
local meta = FindMetaTable( "Player" )

function stats:Init()
  if SERVER then
    stats:loadStats()
  end
  GetSet("topSpeedSess", "Player")
  GetSet("topSpeedAll", "Player")
end


function meta:GetSpeed()
  local vel    = self:GetVelocity()
  local speed2 = Vector()
  local speed3 = Vector(vel.x, vel.y, 0)
  -------------------------------------
  -- Detects vertical momentum.
  -------------------------------------
  speed2.y = vel.y
  speed2.x = vel.x
  speed2.z = 0


  -- If vertical speed is higher than horizontal speed, or negative vertical speed is lower than horizontal speed then
  -- don't count vertical speed.
  if vel.z > speed3:Length()*1.72 or -vel.z > speed3:Length()*1.72 then
      speed2.z = speed3:Length()*1.72

  -- Else if vertical speed is lower than horizontal speed or negative vertical speed is higher than horizontal speed then
  -- count vertical speed.
  elseif vel.z < speed3:Length()*1.72 or -vel.z < vel:Length()*1.72 then
      speed2.z = vel.z
  end

  local speed = math.Round( speed2:Length()/10 )

  return speed
end

function GetDate()
  local Timestamp = os.time()
  local TimeString = os.date( "%d/%m/%Y" , Timestamp )
  return TimeString
end

function meta:getTopSession()
  if self:GettopSpeedSess() == nil then return 0 end
  return self:GettopSpeedSess()
end

function meta:getTopAllTime()
  if self:GettopSpeedAll() == nil then return 0 end
  return self:GettopSpeedAll()
end

function meta:resetATTop()
  if CLIENT then
    topVel = 0
    self:resetSessTop()
    self:SettopSpeedAll(0)
  else
    self:SettopSpeedSess(0)
    self:SettopSpeedAll(0)
    UpdateTop( self )
    net.Start("ResetTopAll")
      net.WriteEntity(self)
    net.Send(self)
  end
end

function meta:resetSessTop()
  if CLIENT then
    topVel = 0
    self:SettopSpeedSess(0)
    net.Start("ResetTopSess")
      net.WriteEntity(self)
    net.SendToServer()
  else
    self:SettopSpeedSess(0)
  end
end

net.Receive("UpdateSpeedTopSess", function(len, ply)
  local topVel = net.ReadDouble()
  ply:SettopSpeedSess(topVel)
  hook.Call("CheckAllTime", nil, ply)
end)

stats:Init()
