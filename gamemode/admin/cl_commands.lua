Admin = {} or Admin
Admin.Commands = {} or Admin.Commands


/*--------------------------------------------------------------------------
 NAME - autoCompleteConVar
 FUNCTION - AutoCompletes ConVar
--------------------------------------------------------------------------*/
function autoCompleteConVar( cmd, args )
	local tbl = {}
  
	for k, v in pairs( Admin.Commands.cmds ) do
    if string.find( string.lower( " "..v ), args ) then
  	  table.insert( tbl, "vgam " .. v )
    end
	end

	return tbl
end

function Admin.Commands.command(ply, cmd, args)
  local varargs = args
  local cmd = table.remove(varargs, 1)
  print(cmd)
	net.Start("VGAM_cmd")
		net.WriteEntity(ply)
    net.WriteString(cmd)
    net.WriteTable(varargs)
	net.SendToServer()
end
net.Receive("VGAM_sendCMD", function(len, ply)
  Admin.Commands.cmds = net.ReadTable()

  concommand.Add("vgam", Admin.Commands.command, autoCompleteConVar, nil, FCVAR_USERINFO)

end)
