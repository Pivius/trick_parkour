local meta = FindMetaTable( "Player" )
Editor = {} or Editor

function Editor:Init()
  GetSet("Noclip", "Player")
  GetSet("Editing", "Player")
end
Editor:Init()

hook.Add("PlayerInitialSpawn", function(ply)
  timer.Simple(2, function()
    ply:SetNoclip(false)
    ply:SetEditing(false)
  end)
end)

function meta:toggleEditor(bool)
  local edit = self:GetEditing()
    if !bool then
      if edit then
        self:SetNWBool( "Editing", false )
        self:SetEditing(false)
      else
        self:SetNWBool( "Editing", true )
        self:SetEditing(true)
      end
    else
      self:SetEditing(bool)
    end
end

function meta:toggleNoclip(bool)
  local noclip = self:GetNoclip()
    if !bool then
      if noclip then
        self:SetNWBool( "Noclip", false )
        self:SetNoclip(false)
      else
        self:SetNWBool( "Noclip", true )
        self:SetNoclip(true)
      end
    else
      self:SetNoclip(bool)
    end
end

if CLIENT then
  function UpdateUI()
    -- Sync Client and Server
    for _, ply in pairs(player.GetAll()) do
      if ply:GetNWBool("Noclip") ~= ply:GetNoclip() then
        ply:SetNoclip(ply:GetNWBool("Noclip"))
      end
      if ply:GetNWBool("Editing") ~= ply:GetEditing() then
        ply:SetEditing(ply:GetNWBool("Editing"))
        hook.Call("UpdateDraw")
      end
    end
  end
  hook.Add("Think", "Update Editor UI", UpdateUI)
end

function Noclip(ply, mv)

  -- Disable noclip
  if !ply:GetNoclip() then return false end

  local forward = mv:GetForwardSpeed()
  local side = mv:GetSideSpeed()
  local up = mv:GetUpSpeed()
  local ang = mv:GetMoveAngles()
  local speed = 5
  local pos = mv:GetOrigin()
  if ( mv:KeyDown( IN_SPEED ) ) then speed = 7 end
  if ( mv:KeyDown( IN_DUCK ) ) then speed = 1 end

  local acceleration = ( ang:Forward() * mv:GetForwardSpeed() ) + ( ang:Right() * mv:GetSideSpeed() ) + ( Vector( 0, 0, 1 ) * mv:GetUpSpeed() )
  local accelSpeed = math.min( acceleration:Length(), ply:GetMaxSpeed() )
  local accelDir = acceleration:GetNormal()
  acceleration = accelDir * accelSpeed * speed

  local newVelocity = mv:GetVelocity() + acceleration * FrameTime() * 5
  newVelocity = newVelocity * ( 0.95 - FrameTime() * 4 );

  // set velocity
  mv:SetVelocity( newVelocity );

  // move the player
  local newOrigin = mv:GetOrigin() + newVelocity * FrameTime()
  mv:SetOrigin( newOrigin )

	return true

end
hook.Add( "Move", "Noclip move", Noclip )
