local trail = CreateClientConVar("pkr_hidetrail", "0", true, true, "Hides/shows trails")

function Hidetrail()
  for k, ent in pairs( ents.FindByClass( "env_spritetrail" ) ) do -- loop through all spritetrails
    if IsValid(ent) and ent:GetParent() ~= LocalPlayer() then
      ent:SetNoDraw( true )
    end
  end
end

function Showtrail()
  for k, ent in pairs( ents.FindByClass( "env_spritetrail" ) ) do -- loop through all spritetrails
    if IsValid(ent) then
      ent:SetNoDraw( false )
    end
  end
end


cvars.AddChangeCallback( "pkr_hidetrail", function( name, old, new )

	if trail:GetBool() then
    Hidetrail()
  else
    Showtrail()
  end
end )
