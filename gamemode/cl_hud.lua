
-------------------------------------
-- Idea SECTION
-------------------------------------
-- SPEED UI
-------------------------------------
-- IDEA The limit should be all time top -- Idea BY THEO

-------------------------------------
-- LOBBIES
-------------------------------------
-- IDEA Hard mode, worse acceleration (higher grav?), no shift

-------------------------------------
-- HEALTH UI
-------------------------------------
--IDEA Light/White UI style
--IDEA Fill up health heart and speed guy idea by moose5XDXDXD
-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}


hud.size = 0.05/2
hud.distance = 25/2

-------------------------------------
-- Topspeed
-- Saves the topspeed of the player for the current session.
-------------------------------------
Topspeed     = 0
Topspeed2     = 0
Topspeed3     = 0

newtop       = false
newtop2       = false
newtop3       = false
topstartlimit= 5
toplimit     = topstartlimit
hudlimits    = 1

start = -1
-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local keyechoes   = CreateClientConVar("pkr_keys", "0", true, true, "Hides/shows key echoes")
local hudtoggle   = CreateClientConVar("pkr_hud", "1", true, true, "Hides/shows hud")
local darktheme   = CreateClientConVar("pkr_darktheme", "0", true, true, "Toggles between light and dark HUD theme")
local plyoutline  = CreateClientConVar( "pkr_outline", "1", true, true, "See players behind walls")
local outlineecho = CreateClientConVar( "pkr_keyoutline", "1", true, true, "Outlines the key echoes when you press it")
local indicators = CreateClientConVar( "pkr_indicator", "1", true, true, "Hides/shows indicators")
local crosshairtoggle = CreateClientConVar( "pkr_crosshair", "1", true, true, "Hides/shows crosshair")
local msrt = CreateClientConVar( "pkr_measurement", "0", true, true, "0 - Measures speed in Units Per Second, 1 - Measures speed in Miles Per Hour, 2 - Measures speed in Kilometers Per Hour")
local OffsUP = 0
local OffsSIDE = 0
local LastViewAngle = Angle(0, 0, 0)
-------------------------------------
-- The main UI colors.
-------------------------------------
hud.color = Color(155, 155, 155, 20)
hud.colorw = Color(155, 155, 155, 10)
hud.colord = Color(0, 0, 0, 150)

-------------------------------------
-- INIT UI.
-------------------------------------
function hud.init()
-------------------------------------
-- The main UI color.
-------------------------------------
        if !darktheme:GetBool() then
            hud.color = hud.colorw
        else
            hud.color = hud.colord
        end
end
hook.Add("Think","different things to initialize",hud.init)
-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
    local draw = true
    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" or name == "CHudCrosshair") then
    draw = false;
    end
return draw;
end




-------------------------------------
-- Draw Player behind walls.
-------------------------------------
function drawPlayer(ent)
    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():EyePos()+ang:Forward()*10
            render.SetShadowColor( 155, 155, 255 )
            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilWriteMask(255)
            render.SetStencilTestMask(255)
            render.SetStencilReferenceValue( 01 )
            render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
            render.SetStencilFailOperation(STENCILOPERATION_KEEP)
            --[[
            local scale = Vector(1.1,1.03,1.01)
            local mat = Matrix()
            mat:Scale(Vector(scale))
            mat:Translate( Vector(-0.36,0,-0.7) )
            ent:EnableMatrix("RenderMultiply", mat) ]]

            render.SetBlend( 0 )
              ent:DrawModel()
            render.SetBlend( 1 )

            render.SetStencilReferenceValue( 01 )
            cam.Start3D2D(Vector(pos.x, pos.y, pos.z),Angle(ang.p+90,ang.y,0),1)
                render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
                surface.SetDrawColor(155,255,255,100)
                surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
            cam.End3D2D()




                    ent:DrawModel()

            render.SetStencilEnable(false)

end

-------------------------------------
-- Render player.
-------------------------------------

hook.Add( "PostPlayerDraw", "RenderEntityGlow", function( ply )

    if !plyoutline:GetBool() then return end

    if( ScrW() == ScrH() ) then return end

    if( OUTLINING_ENTITY ) then return end
    OUTLINING_ENTITY = true

    drawPlayer( ply )

    if ply:GetRagdollEntity() then
        drawPlayer(ply:GetRagdollEntity())
        drawPlayer2(ply:GetRagdollEntity())
    end

    if IsValid( ply:GetActiveWeapon() ) then
        drawPlayer( ply:GetActiveWeapon() )
    end


    OUTLINING_ENTITY = false

end )

-------------------------------------
-- Calculate the distance between two elements. http://www.purplemath.com/modules/distform.htm
-------------------------------------
function GetDistanceSqr(pp ,cx, cy)

    return (pp.x - cx) ^ 2 + (pp.y - cy) ^ 2
end

-------------------------------------
-- Find the  root.
-------------------------------------
function GetDistance(pp, cx, cy)
    return math.sqrt(GetDistanceSqr(pp, cx ,cy))
end

-------------------------------------
-- Indicator UI.
-------------------------------------
function hud.indicator()
    for k, v in pairs (player.GetAll()) do


        local       plypos = (v:LocalToWorld(Vector(0,0,30))):ToScreen()
        local      speed   = v:GetSpeed()
        local   playername = ""
        local        alpha = 255
        local       cx, cy = input.GetCursorPos()
        if v == LocalPlayer() then
            playername = ""
        else
            playername = v:Name()
        end


        if v == LocalPlayer() then
            speed = ""
        else
            speed = v:GetSpeed()
        end



        alpha = Lerp(GetDistance(plypos, cx, cy)/(math.Clamp(alpha, 0, 255)/4) , 255, 0)


        if hudtoggle:GetBool() and indicators:GetBool() and v:Alive() then

        draw.DrawText(playername, "HUD Name", plypos.x, plypos.y-10 , Color(255,255,255, alpha), TEXT_ALIGN_CENTER)
        draw.DrawText(speed, "HUD Speed", plypos.x+20, plypos.y, Color(255,255,255, alpha), TEXT_ALIGN_CENTER)
        end

    end
end

hook.Add("HUDPaint", "Indicator", hud.indicator)

-------------------------------------
-- Crosshair UI.
-------------------------------------
function hud.crosshair()
    local SCREEN_W       = ScrW();
    local SCREEN_H       = ScrH();
    local X_MULTIPLIER   = ScrW() / SCREEN_W;
    local Y_MULTIPLIER   = ScrH() / SCREEN_H;
    local ply      = LocalPlayer()
    local plyspeed = ply:GetVelocity()
    local playerspeed = ply:GetVelocity()
    local speed   = ply:GetSpeed()

    if hudtoggle:GetBool() and crosshairtoggle:GetBool() then

        if not darktheme:GetBool() then
          surface.SetTextColor( 0, 0, 0, 225 )
          surface.SetTextPos( ScrW( )/2.01, ScrH()/2.14 )
          surface.DrawText( "." )
        else
            surface.SetTextColor( 255, 255, 255, 255 )
          	surface.SetTextPos( ScrW( )/2.01, ScrH()/2.14 )
          	surface.DrawText( "." )
        end


        --draw.DrawText(speed, "HUD Speed", X_MULTIPLIER+(SCREEN_W/2)*1.005, Y_MULTIPLIER+(SCREEN_H/2), Color(255,255,255, alpha), TEXT_ALIGN_CENTER)


    end


end

hook.Add("HUDPaint", "Crosshair", hud.crosshair)

-------------------------------------
-- UI Health.
-------------------------------------
function hud.Info( ply, pos, ang )

    local SCREEN_W       = ScrW();
    local SCREEN_H       = ScrH();
    local X_MULTIPLIER   = ScrW() / SCREEN_W;
    local Y_MULTIPLIER   = ScrH() / SCREEN_H;
    local ply            = LocalPlayer()
    local HP             = ply:Health()
    local endpos         = 0
    local serbernaym     = "Retards"
    -------------------------------------
    -- Icon.
    -------------------------------------


    cam.Start3D2D( pos, ang, hud.size/1.6 )
        -------------------------------------
        -- UI Background.
        -------------------------------------

        surface.SetDrawColor(hud.color)
        surface.SetFont( "HUD Info" )
        local w, h = surface.GetTextSize(serbernaym)
        surface.SetDrawColor(Color(hud.color.r, hud.color.g, hud.color.b, hud.color.a+5))
        -- surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER-370,  w*1.05, h*1.2)
        surface.DrawRect( X_MULTIPLIER-w*1.05/2, Y_MULTIPLIER-450,  w*1.05, h*1.2)

        -------------------------------------
        -- serbernaym.
        -------------------------------------

        surface.SetTextColor(Color(255, 255, 255, 255))


        -- surface.SetTextPos( X_MULTIPLIER-670, Y_MULTIPLIER-370 )
        surface.SetTextPos( X_MULTIPLIER-w/2, Y_MULTIPLIER-450 )
        surface.DrawText( serbernaym )
    cam.End3D2D()
end

-------------------------------------
-- UI Health.
-------------------------------------
function hud.Health( ply, pos, ang )

    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local            ply = LocalPlayer()
    local             HP = ply:Health()
    local         endpos = 0
    -------------------------------------
    -- Icon.
    -------------------------------------


    healthicon = Material("materials/trick/hud/FullHealth.png", "noclamp")

    healthicon2 = Material("materials/trick/hud/LowHealth.png", "noclamp")


    cam.Start3D2D( pos, ang, hud.size/1.6 )
        -------------------------------------
        -- UI Background.
        -------------------------------------

            surface.SetDrawColor(hud.color)

        -------------------------------------
        -- UI fit numbers
        -- 100-90
        -------------------------------------
        if HP >= 100 then
            endpos = 80
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP < 100 and HP > 94 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 95 and HP > 93 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 93 and HP > 91 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 91 and HP > 90 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 90-80
        -------------------------------------
        elseif HP <= 90 and HP > 84 then
            endpos = 73
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 85 and HP > 83 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 83 and HP > 81 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 81 and HP > 80 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 80-70
        -------------------------------------
        elseif HP <= 80 and HP > 74 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 75 and HP > 73 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 73 and HP > 71 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 71 and HP > 70 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 70-60
        -------------------------------------
        elseif HP <= 70 and HP > 64 then
            endpos = 73
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 65 and HP > 63 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 63 and HP > 61 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 61 and HP > 60 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 60-50
        -------------------------------------
        elseif HP <= 60 and HP > 54 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 55 and HP > 53 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 53 and HP > 51 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 51 and HP > 50 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 50-40
        -------------------------------------
        elseif HP <= 50 and HP > 44 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 45 and HP > 43 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 43 and HP > 41 then
            endpos = 71
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 41 and HP > 40 then
            endpos = 61
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 40-30
        -------------------------------------
        elseif HP <= 40 and HP > 34 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 35 and HP > 33 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 33 and HP > 31 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 31 and HP > 30 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 30-20
        -------------------------------------
        elseif HP <= 30 and HP > 24 then
            endpos = 72
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 25 and HP > 23 then
            endpos = 69
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 23 and HP > 21 then
            endpos = 71.2
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 21 and HP > 20 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 20-10
        -------------------------------------
        elseif HP <= 20 and HP > 14 then
            endpos = 62
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 15 and HP > 13 then
            endpos = 59
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 13 and HP > 11 then
            endpos = 61
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 11 and HP > 10 then
            endpos = 52
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos

        -------------------------------------
        -- 10-0
        -------------------------------------
        elseif HP <= 10 and HP > 4 then
            endpos = 56
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 5 and HP > 3 then
            endpos = 54
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 3 and HP > 1 then
            endpos = 54
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 1 and HP > 0 then
            endpos = 46
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        elseif HP <= 0 then
            endpos = 55
            lerpB = lerpB and Lerp(0.25, lerpB, endpos) or endpos
        end

        surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375,  lerpB, 44)
        -------------------------------------
        -- Icon UI.
        -------------------------------------
        surface.SetDrawColor(Color(255, 255, 255, 255))
        if HP >= 45 then
        surface.SetMaterial(healthicon)
        positiony = Y_MULTIPLIER+384
        else
        surface.SetMaterial(healthicon2)
        positiony = Y_MULTIPLIER+382
        end
        surface.DrawTexturedRect(X_MULTIPLIER-673.3, positiony, 30, 30)

        -------------------------------------
        -- Health bar background.
        -------------------------------------
        surface.SetDrawColor(Color(hud.color.r, hud.color.g, hud.color.b, 225))
        surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375,  200-1.2, 3)

        -------------------------------------
        -- Health bar delay
        -------------------------------------
        lerp = lerp and Lerp(0.05, lerp, HP) or HP

        surface.SetDrawColor(Color(255, 75, 75, 255))
        surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(lerp, 0, 100), 4)


        -------------------------------------
        -- Health bar.
        -------------------------------------
        surface.SetDrawColor(Color(255, 75, 75, 255))

        //surface.DrawRect( X_MULTIPLIER-675, Y_MULTIPLIER+375, math.Clamp(HP*2, 0, 200) , 3)
 --[[--Used if only when health lowered delayed
            surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(HP, 0, 100), 4)
]]
        if HP > HP then
                lerp = lerp and Lerp(0.05, HP, lerp) or HP
            surface.DrawRect(X_MULTIPLIER-675, Y_MULTIPLIER+375, 2* math.Clamp(lerp, 0, 100), 4)
        end
        -------------------------------------
        -- Health counter.
        -------------------------------------

            surface.SetTextColor(Color(255, 255, 255, 255))
            surface.SetFont( "HUD Health" )
            surface.SetTextPos( X_MULTIPLIER-640, Y_MULTIPLIER+382 )
            surface.DrawText( HP )
    cam.End3D2D()
end

-------------------------------------
-- UI Speed.
-------------------------------------
function hud.Speed( ply, pos, ang )

    local SCREEN_W     = ScrW();
    local SCREEN_H     = ScrH();
    local X_MULTIPLIER = ScrW() / SCREEN_W;
    local Y_MULTIPLIER = ScrH() / SCREEN_H;
    local ply          = LocalPlayer()
    local speed        = ply:GetSpeed()
    -------------------------------------
    -- Calculate the limit of the bar.
    -- The limit is your topspeed
    -------------------------------------
    if (speed*10)/100/2 > toplimit then
        if toplimit < 15 then
            toplimit = toplimit+5
            hudlimits = hudlimits-0.1
        else
            toplimit = toplimit+2.5
            hudlimits = hudlimits-0.02
        end

    end
    -------------------------------------
    -- Icon.
    -------------------------------------



    cam.Start3D2D( pos, ang, hud.size/1.6 )
        -------------------------------------
        -- UI Background.
        -------------------------------------
         surface.SetDrawColor(hud.color)
         surface.DrawRect( X_MULTIPLIER+595, Y_MULTIPLIER+375,  85, 44)

        -------------------------------------
        -- Speed bar background.
        -------------------------------------
        surface.SetDrawColor(hud.color.r, hud.color.g, hud.color.b, 225)
        surface.DrawRect( X_MULTIPLIER+482, Y_MULTIPLIER+375,  200-1.2, 3)


        -------------------------------------
        -- Icon UI.
        -------------------------------------
        --[[OLD
        surface.SetDrawColor(Color(255, 255, 255, 255))
        surface.SetMaterial(speedicon)
        positiony = Y_MULTIPLIER+375

        surface.DrawTexturedRect(X_MULTIPLIER+641, positiony, 40, 45)]]


        surface.SetTextColor(Color(255, 255, 255, 255))
        surface.SetFont( "HUD Runner Icon" )
        surface.SetTextPos( X_MULTIPLIER+650, Y_MULTIPLIER+343 )
        if speed <= 10 then
            surface.DrawText( "C" )
        else
            surface.DrawText( "D" )
        end

        -------------------------------------
        -- Speed bar delay
        -------------------------------------
            surface.SetDrawColor(Color(155, 255, 255, 15))
            lerps2 = lerps2 and Lerp(0.05, lerps2, (speed*10)/(toplimit/hudlimits)) or (speed*10)/(toplimit/hudlimits)
            surface.DrawRect(X_MULTIPLIER+680, Y_MULTIPLIER+375, -math.Clamp(lerps2/hudlimits, 0, 200), 4)
        -------------------------------------
        -- Speed bar.
        -------------------------------------
        surface.SetDrawColor(Color(155, 255, 255, 255))

            lerps = lerps and Lerp(0.25, lerps, (speed*10)/(toplimit/hudlimits)) or (speed*10)/(toplimit/hudlimits)
            surface.DrawRect(X_MULTIPLIER+680, Y_MULTIPLIER+375, -math.Clamp(lerps/hudlimits, 0, 200), 4)

        -------------------------------------
        -- Speed Text.
        -------------------------------------
            surface.SetTextColor(Color(255, 255, 255, 255))
            surface.SetFont( "HUD Health" )
            surface.SetTextPos( X_MULTIPLIER+597, Y_MULTIPLIER+382.5 )
            surface.DrawText( speed )



    cam.End3D2D()
end

-------------------------------------
-- Draws a outlined box.
-------------------------------------
function hud.OutlinedBox( x, y, w, h, thickness, color)
    surface.SetDrawColor( color )

    for i=0, thickness  do

        surface.DrawRect(x, y, w, h-h+thickness)
        surface.DrawRect(x, y+h-thickness, w, h-h+thickness)
        surface.DrawRect(x, y, w-w+thickness, h)
        surface.DrawRect(x+w-thickness+.5, y, w-w+thickness, h)  end

end

-------------------------------------
-- Draws a box.
-------------------------------------
function hud.drawBox(corner, x, y, w, h, color, key, font, txtcolor, ply, enum, keoutline, thickness, outlinecolor)
    xt,yt = x + w/2, y + h/2*0.6
    draw.RoundedBox(corner, x, y, w, h, color)



        if  ( ply:KeyDown( enum ) and keoutline == true and  outlineecho:GetBool()) then

            --surface.DrawRect(x, y, w, h) maybe
            hud.OutlinedBox(x, y, w, h, thickness, outlinecolor)

        end


draw.DrawText( key, font, xt, yt, txtcolor, TEXT_ALIGN_CENTER )
end

-------------------------------------
-- UI key echoes.
-------------------------------------
function hud.keyecho( ply, pos, ang )

    local       SCREEN_W = ScrW();
    local       SCREEN_H = ScrH();
    local   X_MULTIPLIER = ScrW() / SCREEN_W;
    local   Y_MULTIPLIER = ScrH() / SCREEN_H;
    local        cradius = 2

    hud.keys = hud.keys or {}

    hud.keys.backgroundcolor = hud.color
    hud.keys.outlinecolor = Color( 155, 255, 255, 255)
    hud.keys.alpha2 = 0

    hud.keys.alphaw = 0

    hud.keys.alphaa = 0

    hud.keys.alphas = 0

    hud.keys.alphad = 0

    hud.keys.alphajump = 0

    hud.keys.alphaduck = 0

    hud.keys.alphashift = 0

    hud.keys.alpham2 = 0


        if darktheme:GetBool() then
            hud.keys.alphaclamp = 200
            hud.keys.color = Color(hud.colorw.r, hud.colorw.g, hud.colorw.b, 255)
        else
            hud.keys.alphaclamp = 25
            hud.keys.color = Color(hud.colord.r, hud.colord.g, hud.colord.b, 255)
        end

    cam.Start3D2D( pos, ang, hud.size/1.6 )



    -------------------------------------
    -- Fading effect.
    -------------------------------------
        -------------------------------------
        -- W.
        -------------------------------------
        if ply:KeyDown( IN_FORWARD )  then
            hud.keys.alphaw = hud.keys.alphaclamp
        end

        lbaw = lbaw and Lerp(0.05, lbaw, hud.keys.alphaw) or hud.keys.alphaw
        draw.RoundedBox(cradius, -47*X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbaw, 0, hud.keys.alphaw)))
        hud.keys.alphaw = hud.keys.alpha2
        lbaw = lbaw and Lerp(0.01, lbaw, hud.keys.alphaw) or hud.keys.alphaw

        -------------------------------------
        -- A.
        -------------------------------------
        if ply:KeyDown( IN_MOVELEFT )  then
            hud.keys.alphaa = hud.keys.alphaclamp
        end

        lbaa = lbaa and Lerp(0.05, lbaa, hud.keys.alphaa) or hud.keys.alphaa
        draw.RoundedBox(cradius, -146*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbaa, 0, hud.keys.alphaa)))
        hud.keys.alphaa = hud.keys.alpha2
        lbaa = lbaa and Lerp(0.01, lbaa, hud.keys.alphaa) or hud.keys.alphaa

        -------------------------------------
        -- S.
        -------------------------------------
        if ply:KeyDown( IN_BACK )  then
            hud.keys.alphas = hud.keys.alphaclamp
        end

        lbas = lbas and Lerp(0.05, lbas, hud.keys.alphas) or hud.keys.alphas
        draw.RoundedBox(cradius, -47*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbas, 0, hud.keys.alphas)))
        hud.keys.alphas = hud.keys.alpha2
        lbas = lbas and Lerp(0.01, lbas, hud.keys.alphas) or hud.keys.alphas

        -------------------------------------
        -- D.
        -------------------------------------
        if ply:KeyDown( IN_MOVERIGHT )  then
            hud.keys.alphad = hud.keys.alphaclamp
        end
        lbad = lbad and Lerp(0.05, lbad, hud.keys.alphad) or hud.keys.alphad
        draw.RoundedBox(cradius, 53*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbad, 0, hud.keys.alphad)))
        hud.keys.alphad = hud.keys.alpha2
        lbad = lbad and Lerp(0.01, lbad, hud.keys.alphad) or hud.keys.alphad

        -------------------------------------
        -- JUMP.
        -------------------------------------
        if ply:KeyDown( IN_JUMP )  then
            hud.keys.alphajump = hud.keys.alphaclamp
        end

        lbajump = lbajump and Lerp(0.05, lbajump, hud.keys.alphajump) or hud.keys.alphajump
        draw.RoundedBox(cradius, -146*X_MULTIPLIER, 350*Y_MULTIPLIER, 294, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbajump, 0, hud.keys.alphajump)))
        hud.keys.alphajump = hud.keys.alpha2
        lbajump = lbajump and Lerp(0.01, lbajump, hud.keys.alphajump) or hud.keys.alphajump

        -------------------------------------
        -- DUCK.
        -------------------------------------
        if ply:KeyDown( IN_DUCK )  then
            hud.keys.alphaduck = hud.keys.alphaclamp
        end

        lbaduck = lbaduck and Lerp(0.05, lbaduck, hud.keys.alphaduck) or hud.keys.alphaduck
        draw.RoundedBox(cradius, -146*X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbaduck, 0, hud.keys.alphaduck)))
        hud.keys.alphaduck = hud.keys.alpha2
        lbaduck = lbaduck and Lerp(0.01, lbaduck, hud.keys.alphaduck) or hud.keys.alphaduck

        -------------------------------------
        -- SHIFT.
        -------------------------------------
        /*
        if ply:KeyDown( IN_SPEED )  then
            hud.keys.alphashift = hud.keys.alphaclamp
        end

        lbashift = lbashift and Lerp(0.05, lbashift, hud.keys.alphashift) or hud.keys.alphashift
        draw.RoundedBox(cradius, 53*X_MULTIPLIER, 350*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbashift, 0, hud.keys.alphashift)))
        hud.keys.alphashift = hud.keys.alpha2
        lbashift = lbashift and Lerp(0.01, lbashift, hud.keys.alphashift) or hud.keys.alphashift
*/
        -------------------------------------
        -- M2.
        -------------------------------------
        if ply:KeyDown( IN_ATTACK2 )  then
            hud.keys.alpham2 = hud.keys.alphaclamp
        end

        lbam2 = lbam2 and Lerp(0.05, lbam2, hud.keys.alpham2) or hud.keys.alpham2
        draw.RoundedBox(cradius, 53 *X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, Color(hud.keys.backgroundcolor.r, hud.keys.backgroundcolor.g, hud.keys.backgroundcolor.b, math.Clamp(lbam2, 0, hud.keys.alpham2)))
        hud.keys.alpham2 = hud.keys.alpha2
        lbam2 = lbam2 and Lerp(0.01, lbam2, hud.keys.alpham2) or hud.keys.alpham2

    -------------------------------------
    -- Draw key echoes.
    -------------------------------------
        -------------------------------------
        -- W.
        -------------------------------------
        hud.drawBox(cradius, -47*X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "W", "HUD Echo", hud.keys.color, ply, IN_FORWARD, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- A.
        -------------------------------------
        hud.drawBox(cradius, -146*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "A", "HUD Echo", hud.keys.color, ply, IN_MOVELEFT, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- S.
        -------------------------------------
        hud.drawBox(cradius, -47*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "S", "HUD Echo", hud.keys.color, ply, IN_BACK, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- D.
        -------------------------------------
        hud.drawBox(cradius, 53*X_MULTIPLIER, 250*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "D", "HUD Echo", hud.keys.color, ply, IN_MOVERIGHT, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- JUMP.
        -------------------------------------
        hud.drawBox(cradius, -146*X_MULTIPLIER, 350*Y_MULTIPLIER, 294, 95, hud.keys.backgroundcolor, "JUMP", "HUD Echo", hud.keys.color, ply, IN_JUMP, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- DUCK.
        -------------------------------------
        hud.drawBox(cradius, -146*X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "DUCK", "HUD Echo", hud.keys.color, ply, IN_DUCK, true, 5, hud.keys.outlinecolor)

        -------------------------------------
        -- SHIFT.
        -------------------------------------
        --hud.drawBox(cradius, 53*X_MULTIPLIER, 350*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "SHIFT", "HUD Echo", hud.keys.color, ply, IN_SPEED, true, 5, hud.keys.outlinecolor,50, 0)
         -------------------------------------
        -- M2.
        -------------------------------------
        hud.drawBox(cradius, 53*X_MULTIPLIER, 150*Y_MULTIPLIER, 95, 95, hud.keys.backgroundcolor, "M2", "HUD Echo", hud.keys.color, ply, IN_ATTACK2, true, 5, hud.keys.outlinecolor,50, 0)

    cam.End3D2D()
end
hook.Add( "CalcView", "MyCalcView", function(ply, pos, ang)
ViewAngle = ang

end)

function hud.DrawHUD()
    local        ply = LocalPlayer()
    local     plyAng = EyeAngles()
    local     plyPos = EyePos()
    local        pos = plyPos
/*
	  Delta = ViewAngle - ViewAngleLast

    if not ((Delta.y > 180) or (Delta.y < -180)) then
      OffsUP = OffsUP + ((Delta.p - OffsUP) / 20)
      OffsSIDE = OffsSIDE + ((Delta.y - OffsSIDE) / 20)
    end*/
    local  centerAng = Angle( plyAng.p, plyAng.y, 0 )
    pos = pos + ( centerAng:Forward() * hud.distance )
    ViewAngleLast = EyeAngles()
    centerAng:RotateAroundAxis( centerAng:Right(), 90 )
    centerAng:RotateAroundAxis( centerAng:Up(), -90  )

    //centerAng:RotateAroundAxis( centerAng:Right(), hudangle )

    local  leftAng = Angle( centerAng.p, centerAng.y, centerAng.r )
    local rightAng = Angle( centerAng.p, centerAng.y, centerAng.r )

    leftAng:RotateAroundAxis( leftAng:Right(), -15 )
    leftAng:RotateAroundAxis( leftAng:Up(), -7 )
    rightAng:RotateAroundAxis( rightAng:Right(), 15 )
    rightAng:RotateAroundAxis( leftAng:Up(), 8 )

    local speedAng = Angle( centerAng.p, centerAng.y, centerAng.r )

    speedAng:RotateAroundAxis( speedAng:Right(), 20*10)

    local centerAngUp = Angle( plyAng.p, plyAng.y, 0 )

    centerAngUp:RotateAroundAxis( centerAngUp:Right(), 98 )
    centerAngUp:RotateAroundAxis( centerAngUp:Up(), -90 )

    local centerAngDown = Angle( plyAng.p, plyAng.y, 0 )


    centerAngDown:RotateAroundAxis( centerAngDown:Right(), 110 )
    centerAngDown:RotateAroundAxis( centerAngDown:Up(), -90 )

    local  leftAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r )
    local rightAngUp = Angle(centerAngUp.p, centerAngUp.y, centerAngUp.r )

    leftAngUp:RotateAroundAxis( leftAngUp:Right(), 20*-1)
    rightAngUp:RotateAroundAxis( rightAngUp:Right(), 20 )


        cam.Start3D( plyPos, ViewAngle --[[- Angle( OffsUP, OffsSIDE, 0 )]], 105)
            cam.IgnoreZ(true)
                if hudtoggle:GetBool() then
                    --hud.Info(ply, pos, centerAngDown)
                    hud.Health(ply, pos, leftAng)
                    hud.Speed(ply, pos, rightAng)

                    if keyechoes:GetBool() then
                       hud.keyecho(ply, pos, centerAng)
                    end
                end
                --hud.topSpeed(ply, pos, rightAng)
            cam.IgnoreZ(false)
        cam.End3D()

end

hook.Add( "RenderScreenspaceEffects", "hud", hud.DrawHUD )
