local meta = FindMetaTable( "Player" )

topVel = 0

net.Receive("UpdateTopAll", function(len, ply)

  local ply = net.ReadEntity()
  local tbl = net.ReadTable()
  if !IsValid(ply) then return end
  if !ply:GettopSpeedSess() then
    ply:SettopSpeedSess(0)
  end
  if tbl[ply:SteamID()] then
    ply:SettopSpeedAll(tbl[ply:SteamID()]["speed"])
  else
    ply:SettopSpeedAll(0)
  end

end)

net.Receive("ResetTopAll", function(len)
  local ply = net.ReadEntity()
  ply:SettopSpeedSess(0)
  ply:SettopSpeedAll(0)
end)

function topSpeed()
  local ply = LocalPlayer()
  if !ply:Alive() then return end
  local Vel = ply:GetSpeed()
  if Vel > topVel and Vel > 50 then
    topVel = Vel
    ply:SettopSpeedSess(topVel)
    net.Start("UpdateSpeedTopSess")
      net.WriteDouble(topVel)
    net.SendToServer()
  end
end
hook.Add( "Think", "Topspeed", topSpeed )


totalVel = 0
Samples = 0

function meta:resetAvg()
  totalVel = 0
  Samples = 0
  averageVel = 0
end

function averageSpeed()
  local ply = LocalPlayer()
  if !ply:Alive() then return end

  local Vel = ply:GetSpeed()
  if Vel > 50 then
    totalVel = totalVel + Vel
    Samples = Samples + 1
    averageVel = math.Round(totalVel / Samples)
  end
end
hook.Add( "Think", "Averagespeed", averageSpeed )

function meta:GetAvg()
  if !averageVel then return 0 end
  return averageVel
end
